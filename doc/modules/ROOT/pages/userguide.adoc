= Datagate

The intention is to have a configurable system to receive data from different resources
and store or cache this data in a local storage independent from the source.

It offers a simple web interface to show and modify the data. And if the source
allows to create or update data this is also done by changing the local data.

== Overview

[ditaa]
----
   +--------+            +----------+              /-----\
   |   UI   |-list-call->| /data-   |<--query/auth-| App |
   | Browser|<-----------| endpoint |-formatter--->|     |
   +--------+            +----------+              \-----/
        ^  |                  |
        |  |                  |
        |  |           +-------------+
        |  +---------->| Server      |         +----------+
   +-----------+       | o Sparkjava |         |          |
   | Publisher |<------| o Velocity  |         |{s}Json   |
   |           |       | o Jetty     |         |  Storage |
   +-----------+       +-------------+         +----------+
     |                        |                      ^  |
     |                        |                      |  |
     |                 +-------------+               |  |
     |   +-------------|  Data       |--[merge]------+  |
     |   |             | Manager     |                  |
     |   |             |             |<-----------------+
     |   |             +-------------+
     |   |                  ^   |unmap
     |   |                  |   |
     |   |               map|   v
   +----------+        +-------------+
   | Resource |--------| Connector   |
   | Metadata |        |             |
   +----------+        +-------------+
                              |
                              |
                       +-------------+             +-------------+
                       |  Fixture    | -request--->|    Remote   |
                       |             | <--response-|  Datasource |
                       +-------------+             +-------------+
----

include::resource.adoc[]

== Data Manager
This is the central point to manage incoming requests and produce the output. Then overall
configuration for supported endpoints are defined in `SparkConfiguration`.

include::storage.adoc[]

== Connector
A Connector can fetch and send data to an external resource using an optional `Fixture`.
The connection is readonly if the `remoteWritable` flag is `false`. In this case the local storage is
changed only.

== Fixture
A Fixture can be used to separate the request/response activities from the connector,
in the base version we support `FixturePublicHttp` for all public interfaces and
`FixtureAccountManager` which takes the authorization for the connection (don't confound this
with `Authentication` which is used to identify the user)

Every fixture has a name which can be used to modify the local storage name with the intention
that we using the same application to connect different server and store the reuslt in
different locations. To update all loaded resources call the method.

----
Metadata.updateAllFixtureStorageNames( JsonStorage jdb )
----

include::paging.adoc[]

== Merge Data
This is controlled by the resource property `mergeBeforeUpdate` and is used to merge incoming data
with existing data. The resource *needs* a defined primary key.

If the property is `true` the `MergerJsonDefault` is used, this merger analyzes the new and
the old json data and create a change list, this will not merge arrays.

Additional to the data merge a change list is created and send to the notification client if defined,
in this case the update notifiaction isn't send twice.

By implementing the `Merger` interface an own merger can be registered to a resource.
----
resource.setContentMerger(<Merger>)
----

== Conversion
A conversion converts data from one structure to another structure. The overall
structure syntax is *JSON*. It is possible to build conversion chains by using
the class `ConversionChain`.

The interface defines two methods `map` and `unmap`. The `map` method is called
to convert incoming data from the viewpoint of this application and the `unmap`
method is called to send data to the connector and later to the connected remote
system.

To manage different conversions depending at the resource meta-data and the
connection we use the ConversionDictionary class and add a combination between resource
and connector.

----
ConversionDictionary.add(ResourceMetadate.getResourceName(), Connector.getName(), Conversion);
----

There is an special endpoint to get data from storage and
add data to the storage if the came from the `datamanager` web application. This is named
`ConversionDictionary.Node.DATA` and is used by the `details` incoming (i.e. post) endpoints and
the `data` endpoint to deliver *REST* data.

include::formatter.adoc[]

include::publisher.adoc[]

include::mqtt.adoc[]

include::notification.adoc[]

include::parser.adoc[]

include::authentication.adoc[]

