package me.mosling.datagate.parser;

import org.json.JSONObject;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class QueryFilterParserTest
{
    private QueryFilterParser p = new QueryFilterParser();

    private static void checkError( List<ErrorEntry> l, List<String> m )
    {
        int cnte = m.size() / 3;

        assertThat( l.size() ).isEqualTo( cnte );
        for ( int i = 0; i < cnte; ++i )
        {
            int        ii = 3 * i;
            ErrorEntry ee = l.get( i );
            assertThat( ee.getLine() ).isEqualTo( m.get( ii ) );
            assertThat( ee.getMsg() ).isEqualTo( m.get( ii + 1 ) );
            assertThat( ee.errorMarker() ).isEqualTo( m.get( ii + 2 ) );

        }
    }

    @Test
    void missingDot()
    {
        p.createFilterListFromHttpQuery( "id.eq1234" );

        checkError( p.getErrorListener().getErrors(), Arrays.asList( "id.eq1234",
                "line 1:3 mismatched input 'eq1234' expecting {IN, ANY, ALL, LIKE, ILIKE, MATCH, BTW, NOT, EQ, GT, "
                        + "GTE, LT, LTE, IS, HAS}", "   ^^^^^^" ) );
    }

    @Test
    void openString()
    {
        p.createFilterListFromHttpQuery( "id.eq.'1234" );

        checkError( p.getErrorListener().getErrors(),
                Arrays.asList( "id.eq.'1234", "line 1:6 token recognition error at: ''1234'", "", "id.eq.'1234",
                        "line 1:11 mismatched input '<EOF>' expecting {QSTRING, ISODATETIME, NUMBER, FIELDNAME, "
                                + "ADDCHAR}",
                        "           ^" ) );
    }

    @Test
    void openStringCheckJson()
    {
        p.createFilterListFromHttpQuery( "id.eq.'1234" );

        String s = QueryFilterParser.prepareJsonErrorList( p.getErrorListener().getErrors() );
        System.out.println(s);
        JSONObject o = new JSONObject(s);

    }

    @Test
    void checkUsingNewestVersion()
    {
        // for versions before 4.7.2 this test throws a StringIndexOutOfBoundsException
        String inp = "or(id.like.'*’*',id.like.'*''*)";
        p.createFilterListFromHttpQuery( inp );

        checkError( p.getErrorListener().getErrors(),
                Arrays.asList( inp, "line 1:28 token recognition error at: ''*)'", "", inp,
                        "line 1:31 mismatched input '<EOF>' expecting {')', ','}",
                        "                               ^" ) );
    }

    @Test
    void openLikeAttribute()
    {
        p.createFilterListFromHttpQuery( "id.like.'*''*" );

        checkError( p.getErrorListener().getErrors(),
                Arrays.asList( "id.like.'*''*", "line 1:11 token recognition error at: ''*'", "" ) );
    }
}
