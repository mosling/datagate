package me.mosling.datagate.mock;

import me.mosling.datagate.fixture.Fixture;
import me.mosling.httpfixture.fixture.ResponseData;

import java.util.Map;
import java.util.TreeMap;

public class FixtureTestHelper implements Fixture {

    private Map<String, String> addressResponseMap = new TreeMap<>();

    public void addResponseForAddress(String address, String response) {
        addressResponseMap.put(address, response);
    }

    public void cleanAllResponseEntries() {
        addressResponseMap.clear();
    }

    @Override
    public String getName() {
        return "TestHelper";
    }

    @Override
    public ResponseData doGetRequest(String resource, String address) {
        ResponseData rd = new ResponseData();
        rd.setStatus(200);
        rd.setContenttype("application/json");
        rd.setResponseContent(addressResponseMap.getOrDefault(address, ""));

        return rd;
    }

    @Override
    public ResponseData doWriteRequest(String resource, String method, String address, String data) {
        return null;
    }

    @Override
    public boolean isConnected() {
        return true;
    }
}
