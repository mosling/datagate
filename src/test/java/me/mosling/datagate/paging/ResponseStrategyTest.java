package me.mosling.datagate.paging;

import me.mosling.datagate.connector.Connector;
import me.mosling.datagate.connector.ConnectorRest;
import me.mosling.datagate.core.DataManager;
import me.mosling.datagate.core.Metadata;
import me.mosling.datagate.mock.FixtureTestHelper;
import me.mosling.datagate.storage.JsonStorage;
import me.mosling.datagate.storage.JsonStorageMemory;
import me.mosling.httpfixture.common.JsonHelper;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class ResponseStrategyTest {

    private static DataManager dataManager;
    private static JsonStorage storage;
    private static Connector connector;
    private static FixtureTestHelper fixture;

    @BeforeAll
    public static void setup() {
        fixture = new FixtureTestHelper();
        storage = new JsonStorageMemory();
        dataManager = new DataManager(storage);
        connector = new ConnectorRest(dataManager, fixture);

        Metadata.loadConfiguration("testdata", connector);
    }

    @BeforeEach
    public void before() {
        fixture.cleanAllResponseEntries();
    }

    @Test
    public void checkEntryResponse() {
        fixture.addResponseForAddress("/test/app", JsonHelper.createJsonFromString("name,testapp,description,small testapp", ","));

        dataManager.refreshData(Metadata.get("testdata-app"), false);
        dataManager.waitForFetchThreads();

        List<String> l = storage.select("md_testdata_app", null);

        assertThat(l.size()).isEqualTo(1);
    }


}
