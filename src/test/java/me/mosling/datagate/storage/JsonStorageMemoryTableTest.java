package me.mosling.datagate.storage;

import me.mosling.datagate.parser.QueryParameter;
import org.json.JSONObject;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class JsonStorageMemoryTableTest
{

    private final JsonStorageMemoryTable t = new JsonStorageMemoryTable();

    @BeforeEach
    void eachTestSetup()
    {
        t.insert( g( "one", "dog", "house" ) );
        t.insert( g( "two", "cat", "house" ) );
        t.insert( g( "three", "dog", "house" ) );
        t.insert( g( "four", "mouse", "house" ) );
        t.insert( g( "four", "dog", "village" ) );

        t.addIndex( "id" );
    }

    @Test
    void size()
    {
        assertThat( t.size( null ) ).isEqualTo( 5 );
        assertThat( t.size( QueryParameter.newEqual( "id", "two" ) ) ).isEqualTo( 1 );
        assertThat( t.size( QueryParameter.newEqual( "id", "thousand" ) ) ).isEqualTo( 0 );
        assertThat( t.size( QueryParameter.newEqual( "arg1", "dog" ) ) ).isEqualTo( 3 );
        assertThat( t.size( QueryParameter.newEqual( "arg5", "cat" ) ) ).isEqualTo( 0 );

        assertThat( t.size( new QueryParameter( "arg1.eq.dog,arg2.eq.house", "", "", "" ) ) ).isEqualTo( 2 );
    }

    @Test
    void select()
    {
        assertThat( t.select( new QueryParameter( "id.eq.one", "", "", "" ) ).size() ).isEqualTo( 1 );
    }

    @Test
    void delete()
    {
        t.delete( new QueryParameter( "arg1.eq.dog,arg2.eq.house", "", "", "" ) );

        assertThat( t.size( null ) ).isEqualTo( 3 );
        assertThat( t.size( QueryParameter.newEqual( "arg1", "dog" ) ) ).isEqualTo( 1 );

        t.delete( null );
        assertThat( t.size( null ) ).isEqualTo( 0 );
    }

    @Test
    void insert()
    {
        int s = t.size( null );

        t.insert( g( "insert-1", "bird", "house", "new" ) );
        t.insert( g( "insert-1", "dog", "city", "new" ) );

        assertThat( t.size( null ) ).isEqualTo( s + 2 );
        assertThat( t.size( QueryParameter.newEqual( "id", "insert-1" ) ) ).isEqualTo( 2 );

        t.delete( QueryParameter.newEqual( "id", "insert-1" ) );
        assertThat( t.size( null ) ).isEqualTo( s );
    }

    private JSONObject g( String pk, String... args )
    {
        JSONObject o = new JSONObject();
        o.put( "id", pk );
        int i = 1;
        for ( String s : args )
        {
            o.put( String.format( "arg%d", i++ ), s );
        }

        return o;
    }
}