package me.mosling.datagate.storage;

import me.mosling.datagate.parser.QueryParameter;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

class QueryPostgresSqlTest
{

    private String where( String filter )
    {
        String wv = " WHERE ";
        String ws = QueryPostgresSql.createWhereClause( new QueryParameter( filter, "", "", "" ) );
        assertThat( ws ).startsWith( wv );

        return ws.substring( wv.length() );
    }

    @Test
    void createWhereClause_empty()
    {
        assertThat( QueryPostgresSql.createWhereClause( new QueryParameter( "", "", "", "" ) ) ).isEqualTo( "" );
    }

    @Test
    void createWhereClause_junction()
    {
        assertThat( where( "i.like.aaab_s__,i.not.eq.aaab_stg" ) ).isEqualTo(
                "data ->> 'i' like 'aaab_s__' and data ->> 'i' <> 'aaab_stg'" );

        assertThat( where( "or(a.eq.a,b.eq.b)" ) ).isEqualTo( " ( data ->> 'a' = 'a' or data ->> 'b' = 'b' ) " );

        assertThat( where( "and(or(a.eq.a,b.eq.b),c.eq.3)" ) ).isEqualTo(
                " (  ( data ->> 'a' = 'a' or data ->> 'b' = 'b' )  and (data ->> 'c')::numeric = 3 ) " );
    }

    @Test
    void createWhereClause_comparsion()
    {
        assertThat( where( "col.eq.me" ) ).isEqualTo( "data ->> 'col' = 'me'" );
        assertThat( where( "col.not.eq.me" ) ).isEqualTo( "data ->> 'col' <> 'me'" );
        assertThat( where( "col.lt.me" ) ).isEqualTo( "data ->> 'col' < 'me'" );
        assertThat( where( "col.lte.me" ) ).isEqualTo( "data ->> 'col' <= 'me'" );
        assertThat( where( "col.gt.me" ) ).isEqualTo( "data ->> 'col' > 'me'" );
        assertThat( where( "col.gte.me" ) ).isEqualTo( "data ->> 'col' >= 'me'" );
        assertThat( where( "col.is.null" ) ).isEqualTo( "data ->> 'col' is null" );
    }

    @Test
    void createWhereClause_comparsion_rhs_string()
    {
        assertThat( where( "id.eq.'23'" ) ).as( "quote string" ).isEqualTo( "data ->> 'id' = '23'" );
    }

    @Test
    void createWhereClause_comparsion_rhs_number()
    {
        assertThat( where( "id.eq.23" ) ).as( "number" ).isEqualTo( "(data ->> 'id')::numeric = 23" );
    }

    @Test
    void createWhereClause_comparsion_rhs_datetime()
    {
        assertThat( where( "t.eq.2018-10-10" ) ).as( "date" ).isEqualTo( "data ->> 't' = '2018-10-10'" );
        assertThat( where( "t.eq.2018-10-10T12:45:00" ) )
                .as( "datetime" )
                .isEqualTo( "data ->> 't' = '2018-10-10T12:45:00'" );
    }

    @Test
    void createWhereClause_comparsion_rhs_column()
    {
        assertThat( where( "c1.eq.data>c2" ) ).as( "column" ).isEqualTo( "data ->> 'c1' = data ->> 'c2'" );
        assertThat( where( "c1.eq._c3" ) ).as( "column" ).isEqualTo( "data ->> 'c1' = \"c3\"" );
    }

    @Test
    void createWhereClause_between()
    {
        assertThat( where( "age.BTW.(23,45)" ) ).isEqualTo( "(data ->> 'age')::numeric between 23 and 45" );
        assertThat( where( "age.BETWEEN.(45,65)" ) ).isEqualTo( "(data ->> 'age')::numeric between 45 and 65" );
        assertThat( where( "age.not.between.(65,100)" ) ).isEqualTo(
                "(data ->> 'age')::numeric not between 65 and 100" );

        assertThat( where( "birthday.BTW.(2000-01-01,2020-12-31)" ) ).isEqualTo(
                "data ->> 'birthday' between '2000-01-01' and '2020-12-31'" );
    }

    @Test
    void createWhereClause_list()
    {
        assertThat( where( "id.in.(3,4,5)" ) ).isEqualTo( "data ->> 'id' in (3,4,5)" );
        assertThat( where( "id.in.(abc,def,ghi)" ) ).isEqualTo( "data ->> 'id' in ('abc','def','ghi')" );
        assertThat( where( "id.in.('a*bc','4','5')" ) ).isEqualTo( "data ->> 'id' in ('a*bc','4','5')" );
    }

    @Test
    void createWhereClause_match()
    {
        List<String> pl = Arrays.asList( "(a|b){1,3}ig", "(a|b){1,3}ig[hfj4-5kd]*", "[^-123-8]" );

        for ( String p : pl )
        {
            assertThat( where( "id.match." + p ) ).isEqualTo( "data ->> 'id' similar to '" + p + "'" );
        }

        assertThat( where( "id.not.match.[0-9]*" ) ).isEqualTo( "data ->> 'id' not similar to '[0-9]*'" );
    }

    @Test
    void toList()
    {
        assertThat( QueryPostgresSql.toList( "1,2,3", false ) ).isEqualTo( "1,2,3" );
        assertThat( QueryPostgresSql.toList( "1,2,3", true ) ).isEqualTo( "'1','2','3'" );
        assertThat( QueryPostgresSql.toList( "abc,2,3", false ) ).isEqualTo( "'abc',2,3" );
        assertThat( QueryPostgresSql.toList( "data>id,34,56", false ) ).isEqualTo( "(data ->> 'id')::numeric,34,56" );
        assertThat( QueryPostgresSql.toList( "data>id,34,56", null ) ).isEqualTo( "data ->> 'id',34,56" );
    }

    @Test
    void toValue_basics()
    {
        assertThat( QueryPostgresSql.toValue( "abc", true ) ).isEqualTo( "'abc'" );
        assertThat( QueryPostgresSql.toValue( "abc", false ) ).isEqualTo( "'abc'" );
        assertThat( QueryPostgresSql.toValue( "'ab'c'", false ) ).isEqualTo( "'ab'c'" );
        assertThat( QueryPostgresSql.toValue( "'ab\'c'", false ) ).isEqualTo( "'ab\'c'" );
        assertThat( QueryPostgresSql.toValue( "1234", true ) ).isEqualTo( "'1234'" );
        assertThat( QueryPostgresSql.toValue( "1234", false ) ).isEqualTo( "1234" );
        assertThat( QueryPostgresSql.toValue( "3.14", false ) ).isEqualTo( "3.14" );
        assertThat( QueryPostgresSql.toValue( "a'bc", false ) ).isEqualTo( "'a''bc'" );
        assertThat( QueryPostgresSql.toValue( "I'm I'm", false ) ).isEqualTo( "'I''m I''m'" );
    }

    @Test
    void toCast()
    {
        assertThat( QueryPostgresSql.toCast( "col", null, "true" ) ).isEqualTo( "(col)::boolean" );
        assertThat( QueryPostgresSql.toCast( "col", null, "false" ) ).isEqualTo( "(col)::boolean" );
        assertThat( QueryPostgresSql.toCast( "col", null, "1234" ) )
                .as( "implicite numeric" )
                .isEqualTo( "(col)::numeric" );
        assertThat( QueryPostgresSql.toCast( "col", false, "3.14" ) )
                .as( "explicite numeric" )
                .isEqualTo( "(col)::numeric" );
        assertThat( QueryPostgresSql.toCast( "col", true, "nothing" ) ).as( "string" ).isEqualTo( "col" );
    }

    @Test
    void toColumn_data()
    {
        String s = JsonStoragePostgres.DATA_COLUMN_NAME;
        assertThat( QueryPostgresSql.toColumn( "id", s, false ) ).isEqualTo( "data ->> 'id'" );
        assertThat( QueryPostgresSql.toColumn( "id", s, true ) ).isEqualTo( "data -> 'id'" );
    }

    @Test
    void toColumn_column()
    {
        String s = JsonStoragePostgres.DATA_COLUMN_NAME;
        assertThat( QueryPostgresSql.toColumn( "_id", s, false ) ).isEqualTo( "\"id\"" );
        assertThat( QueryPostgresSql.toColumn( "_data", s, false ) ).isEqualTo( "\"data\"" );
        assertThat( QueryPostgresSql.toColumn( "_other>id", s, false ) ).isEqualTo( "other ->> 'id'" );
    }

    @Test
    void toColumn_json()
    {
        String s = JsonStoragePostgres.DATA_COLUMN_NAME;
        assertThat( QueryPostgresSql.toColumn( "data>id", s, false ) ).isEqualTo( "data ->> 'id'" );
        assertThat( QueryPostgresSql.toColumn( "id", s, false ) ).isEqualTo( "data ->> 'id'" );
        assertThat( QueryPostgresSql.toColumn( "_data>id", s, false ) ).isEqualTo( "data ->> 'id'" );
    }

    @Test
    void toColumn_jsonPath()
    {
        String s = JsonStoragePostgres.DATA_COLUMN_NAME;
        assertThat( QueryPostgresSql.toColumn( "data>a1>a2>a3", s, false ) ).isEqualTo(
                "data -> 'a1' -> 'a2' ->> 'a3'" );
        assertThat( QueryPostgresSql.toColumn( "a1>a2>a3", s, false ) ).isEqualTo(
                "data -> 'a1' -> 'a2' ->> 'a3'" );
    }

    @Test
    void toColumn_jsonIndex()
    {
        String s = JsonStoragePostgres.DATA_COLUMN_NAME;
        assertThat( QueryPostgresSql.toColumn( "a1>a2>0", s, false ) ).isEqualTo( "data -> 'a1' -> 'a2' ->> 0" );
        assertThat( QueryPostgresSql.toColumn( "a2>456", s, true ) ).isEqualTo( "data -> 'a2' -> 456" );
    }

    @Test
    void toSelect()
    {
        QueryParameter qp = new QueryParameter( "", "a,b", "", "" );
        assertThat( QueryPostgresSql.createSelection( qp ) ).isEqualTo(
                "jsonb_build_object('a', data -> 'a','b', data -> 'b')" );

        qp = new QueryParameter( "", "data,a", "", "" );
        assertThat( QueryPostgresSql.createSelection( qp ) ).isEqualTo(
                "data || jsonb_build_object('a', data -> 'a')" );

        qp = new QueryParameter( "", "distinct,data,a", "", "" );
        assertThat( QueryPostgresSql.createSelection( qp ) ).isEqualTo(
                "distinct data || jsonb_build_object('a', data -> 'a')" );

        qp = new QueryParameter( "", "distinct,distinct,_a", "", "" );
        assertThat( QueryPostgresSql.createSelection( qp ) ).isEqualTo(
                "distinct jsonb_build_object('distinct', data -> 'distinct','_a', \"a\")" );
    }
}