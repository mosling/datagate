package me.mosling.datagate.formatter;

import org.json.JSONObject;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

class CsvFormatterTest
{
    static private List<JSONObject> l   = new ArrayList<>();

    @BeforeAll
    static void createTestResult()
    {
        JSONObject o1 = new JSONObject( "{\"name\":\"alfred\", \"age\":32}" );
        JSONObject o2 = new JSONObject( "{\"name\":\"beate\", \"age\":44}" );
        JSONObject o3 = new JSONObject( "{\"name\":\"zorkan\", \"age\":234}" );

        l.add( o1 );
        l.add( o2 );
        l.add( o3 );
    }

    @Test
    void formatWithoutSelectList()
    {
        CsvDataFormatter csv = new CsvDataFormatter( false, ",", "\"" );
        String r1 = csv.format( l, null );
        assertThat( r1 ).isEqualTo( "alfred,32\nbeate,44\nzorkan,234\n" );
    }

    @Test
    void formatWithSelectList()
    {
        CsvDataFormatter csv = new CsvDataFormatter( false, ";", "\"" );
        String r2 = csv.format( l, Arrays.asList( "age", "name" ) );
        assertThat( r2 ).isEqualTo( "32;alfred\n44;beate\n234;zorkan\n" );
    }
}