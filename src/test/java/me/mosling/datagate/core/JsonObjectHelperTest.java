package me.mosling.datagate.core;

import me.mosling.datagate.common.JsonObjectHelper;
import me.mosling.httpfixture.common.CommonHelper;
import org.assertj.core.api.Assertions;
import org.json.JSONObject;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;

class JsonObjectHelperTest
{

    String     jsonStr = "";
    JSONObject jsonObj;

    @BeforeEach
    void setupJson()
    {
        InputStream is = CommonHelper.getInputStreamFromName( "test.json", false, true );
        if ( is != null )
        {
            jsonObj = new JSONObject(
                    new BufferedReader( new InputStreamReader( is ) ).lines().collect( Collectors.joining( "\n" ) ) );
        }

        assertThat( jsonObj ).isNotNull();
    }

    @Test
    void getField()
    {
        Assertions.assertThat( JsonObjectHelper.getField( jsonObj, "name" ) ).isEqualTo( "test-object" );
        assertThat( JsonObjectHelper.getField( jsonObj, "deeper.id" ) ).isEqualTo( "number two" );
    }

    @Test
    void updateOptionsJsonLookup()
    {
        JsonObjectHelper.updateOptionsJsonLookup( jsonObj );

        assertThat( JsonObjectHelper.getField( jsonObj, "dataSource" ) ).isEqualTo(
                "LOOKUP(\"a\", \"b\", \"c\", true)" );
        assertThat( JsonObjectHelper.getField( jsonObj, "deeper.dataSource" ) ).isEqualTo(
                "LOOKUP(\"a\", \"b\", " + "\"c\", false)" );

    }
}