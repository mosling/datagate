package me.mosling.datagate.core;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

class ResourceMetadataTest
{
    private static final String TEST_RESOURCE = "testdata";

    @BeforeEach
    void setUp()
    {
        Metadata.loadConfiguration("testdata.json", null);

        assertThat(Metadata.get(TEST_RESOURCE)).isNotNull();
    }

    @Test
    void checkAttributes()
    {
        ResourceMetadata rmd = Metadata.get(TEST_RESOURCE + "-app");
        assertThat(rmd.getFqn()).isEqualTo("MD.testdata-app");
        assertThat(rmd.getLocalStorageName()).isEqualTo("md_testdata_app");
        assertThat(rmd.getListPropertiesAsString()).isEmpty();
        assertThat(rmd.getListProperties().isEmpty()).isTrue();
    }

    @Test
    void checkSorting()
    {
        List<String> l = Metadata.getSortedResourceNames();
        assertThat(l).containsExactly("testdata", "testdata-app");

        Metadata.get(TEST_RESOURCE).setOrder(0);
        Metadata.get(TEST_RESOURCE + "-app").setOrder(0);

        l = Metadata.getSortedResourceNames();
        assertThat(l).containsExactly("testdata-app", "testdata");
    }

    @Test
    void checkOptionLookupExpansion()
    {
        String o = Metadata.get(TEST_RESOURCE).getOptionsSource();

        assertThat(o).doesNotStartWith("{");

        assertThat(o).contains(
                "function (callback) { callback( getLookupData(\"metadata-app\", \"shortname\", \"shortname\", false)"
                        + " ) }");
    }

    @Test
    void checkSchema()
    {
        String s = Metadata.get(TEST_RESOURCE).getSchemaSource();

        assertThat(s).startsWith("{");
        assertThat(s).contains("\"application\"");
    }
}