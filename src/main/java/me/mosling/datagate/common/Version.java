package me.mosling.datagate.common;

import me.mosling.httpfixture.common.CommonHelper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.util.Map;
import java.util.Properties;
import java.util.TreeMap;

public final class Version {
    private static final Logger LOGGER = LogManager.getLogger(Version.class);

    private static final String noVersionstring = "0.0.0";
    private static final Map<String, Properties> versionInfoMap = new TreeMap<>();

    private static void readVersionInfo(String artifact) {
        String bp = artifact + "-build.properties";
        String gp = artifact + "-git.properties";
        InputStream bs = CommonHelper.getInputStreamFromName(bp, true);
        InputStream gs = CommonHelper.getInputStreamFromName(gp, true);

        Properties vp = new Properties();
        versionInfoMap.put(artifact, vp);

        if (null != bs) {
            try {
                vp.load(bs);
            } catch (IOException e) {
                LOGGER.error("can't read build properties '{}' --> {}", bp, e.getMessage());
            }
        }

        if (null != gs) {
            try {
                vp.load(gs);
            } catch (IOException e) {
                LOGGER.error("can't read git properties '{}' --> {}", gp, e.getMessage());
            }
        }
    }

    public static String info(String artifact, String whichProperty) {

        if (!versionInfoMap.containsKey(artifact)) {
            readVersionInfo(artifact);
        }

        Properties p = versionInfoMap.getOrDefault(artifact, null);

        String wp = whichProperty;
        String notFound;
        if (wp.startsWith("!")) {
            wp = whichProperty.substring(1);
            notFound = "";

        } else {
            notFound = "<unknown " + whichProperty + ">";
        }

        return p != null ? p.getProperty(wp, notFound) : notFound;
    }

}
