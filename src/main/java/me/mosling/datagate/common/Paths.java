package me.mosling.datagate.common;

import java.util.Map;
import java.util.TreeMap;

public class Paths {

    public static final String RESOURCE_NAME = "RESOURCE-NAME";
    public static final String WEB_STATIC = "WEB-STATIC";
    public static final String WEB_LIST = "WEB-LIST";
    public static final String WEB_DETAILS = "WEB-DETAILS";
    public static final String WEB_DATA = "WEB-DATA";
    public static final String WEB_LOGIN = "WEB-LOGIN";
    public static final String WEB_INFO = "WEB-INFO";
    public static final String TEMPLATE_LIST = "TEMPLATE-LIST";
    public static final String TEMPLATE_INFO = "TEMPLATE-INFO";
    public static final String TEMPLATE_DETAILS = "TEMPLATE-DETAILS";
    public static final String TEMPLATE_CHART = "TEMPLATE-CHART";
    public static final String TEMPLATE_ERROR = "TEMPLATE-ERROR";
    public static final String TEMPLATE_ERROR_JSON = "TEMPLATE-ERROR-JSON";

    private Paths() {
        // lookup data class
    }

    public static Map<String, String> getDefaultPathMapping() {
        Map<String, String> m = new TreeMap<>();
        m.put(Paths.RESOURCE_NAME, ":name");
        m.put(Paths.WEB_STATIC, "/public");
        m.put(Paths.WEB_LIST, "/list/");
        m.put(Paths.WEB_DETAILS, "/details");
        m.put(Paths.WEB_DATA, "/data/");
        m.put(Paths.WEB_LOGIN, "/login/");
        m.put(Paths.WEB_INFO, "/meta/info/");
        m.put(Paths.TEMPLATE_LIST, "/templates/mainpage-list.vm");
        m.put(Paths.TEMPLATE_INFO, "/templates/metadata-info.vm");
        m.put(Paths.TEMPLATE_DETAILS, "/templates/alpaca-details.vm");
        m.put(Paths.TEMPLATE_CHART, "/templates/chart-visjs.vm");
        m.put(Paths.TEMPLATE_ERROR, "/templates/error.vm");
        m.put(Paths.TEMPLATE_ERROR_JSON, "/templates/error-json.vm");

        return m;
    }
}
