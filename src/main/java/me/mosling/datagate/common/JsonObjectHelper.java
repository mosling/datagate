package me.mosling.datagate.common;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONString;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class JsonObjectHelper {
    private static final Logger LOGGER = LogManager.getLogger(JsonObjectHelper.class);
    public static final String IS_NULL = " is null";

    private static final Pattern lookupPattern = Pattern.compile("LOOKUP\\(([^)]*)\\)");
    private static final String TYPE_STRING = "type";
    private static final String DATASOURCE_STRING = "dataSource";

    private JsonObjectHelper() {
        // functional helper class
    }

    public static String getField(JSONObject document, String path) {
        if (document == null || path == null || path.isEmpty()) {
            return "";
        }

        if (path.contains(".")) {
            int indexOfDot = path.indexOf('.');
            String subKey = path.substring(0, indexOfDot);
            JSONObject jsonObject = (JSONObject) document.get(subKey);
            if (jsonObject == null) {
                throw new JSONException(subKey + IS_NULL);
            }
            try {
                return getField(jsonObject, path.substring(indexOfDot + 1));
            } catch (JSONException e) {
                throw new JSONException(subKey + "." + e.getMessage());
            }
        } else if (document.has(path)) {
            return document.get(path).toString();
        }

        return "";
    }

    public static void putStringField(JSONObject document, String path, String value) {
        if (document == null || path == null || path.isEmpty()) {
            return;
        }

        if (path.contains(".")) {
            int indexOfDot = path.indexOf('.');
            String subKey = path.substring(0, indexOfDot);
            JSONObject jsonObject = (JSONObject) document.get(subKey);
            if (jsonObject == null) {
                throw new JSONException(subKey + IS_NULL);
            }
            try {
                putStringField(jsonObject, path.substring(indexOfDot + 1), value);
            } catch (JSONException e) {
                throw new JSONException(subKey + "." + e.getMessage());
            }
        } else {
            // we have reached the node which should contains the node
            document.put(path, value);
        }
    }

    public static void updateOptionsJsonLookup(JSONObject jsonOptions) {
        if (null == jsonOptions || jsonOptions.keySet().isEmpty()) {
            return;
        }

        if (jsonOptions.has(TYPE_STRING) && jsonOptions.has(DATASOURCE_STRING)) {
            String t = jsonOptions.getString(TYPE_STRING);
            if ("chooser".equalsIgnoreCase(t) || "select".equalsIgnoreCase(t)) {
                Matcher matcher = lookupPattern.matcher(jsonOptions.getString(DATASOURCE_STRING));
                if (matcher.matches()) {
                    String m = matcher.group(1);
                    String[] a = m.split(",");
                    if (a.length != 3) {
                        LOGGER.error(
                                "lookup needs exactly three parameter (<resource>,<primary-key-field>,<label-field>) "
                                        + "but is {}", m);
                    } else {
                        jsonOptions.put(DATASOURCE_STRING,
                                String.format("LOOKUP(\"%s\", \"%s\", \"%s\", %s)", a[0], a[1], a[2],
                                        "chooser".equalsIgnoreCase(t) ? "true" : "false"));
                    }
                }
            }
        }

        jsonOptions.keySet().forEach(o -> updateOptionsJsonLookup(jsonOptions.optJSONObject(o)));
    }

    public static void analyzeJson(String json) {
        if (!json.isEmpty()) {
            try {
                explainJson("$.", new JSONObject(json));
            } catch (JSONException ex) {
                LOGGER.error("JSON Exception {} for '{}'", ex.getMessage(), json);
            }
        } else {
            LOGGER.debug("empty result from server");
        }
    }

    @SuppressWarnings("squid:S3776")
    private static void explainJson(String path, JSONObject obj) {
        for (String k : obj.keySet()) {
            Object o = obj.get(k);
            if (o instanceof JSONObject) {
                LOGGER.debug("{}object: {} {}", path, k, (obj.isNull(k)) ? IS_NULL : "");
                if (!obj.isNull(k)) {
                    explainJson(path + k + ".", (JSONObject) o);
                }
            } else if (o instanceof JSONArray) {
                int l = ((JSONArray) o).length();
                LOGGER.debug("{}{}{} array size {}", path, k, (obj.isNull(k)) ? IS_NULL : "", l);
                if (!obj.isNull(k) && l > 0) {
                    if (((JSONArray) o).get(0) instanceof JSONObject) {
                        explainJson(path + k + "[0].", ((JSONArray) o).getJSONObject(0));
                    } else {
                        for (int i = 0; i < l; ++i) {
                            showJsonNode(path, String.format("%s[%d]", k, i), ((JSONArray) o).get(i));
                        }
                    }
                }
            } else {
                showJsonNode(path, k, o);
            }
        }
    }

    private static void showJsonNode(String path, String k, Object o) {
        if (o instanceof JSONString) {
            LOGGER.debug("{}string: {} = '{}'", path, k, o.toString());
        } else {
            LOGGER.debug("{}{} = {} ({})", path, k, o.toString(), o.getClass().getName());
        }
    }

}
