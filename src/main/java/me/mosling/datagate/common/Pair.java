package me.mosling.datagate.common;

import lombok.Getter;

// implement this class because Heroku doesn't support packages from javax
public class Pair<K, V> {
    @Getter
    private final K key;
    @Getter
    private final V value;

    public Pair(K key, V value) {
        this.key = key;
        this.value = value;
    }
}
