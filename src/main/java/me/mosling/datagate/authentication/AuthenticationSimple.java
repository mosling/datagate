package me.mosling.datagate.authentication;

import lombok.Getter;
import lombok.Setter;
import me.mosling.datagate.conversion.Conversion;
import me.mosling.datagate.conversion.ConversionDictionary;
import me.mosling.datagate.conversion.ConversionPbkdf2;
import me.mosling.datagate.core.GeneralException;
import me.mosling.datagate.core.Metadata;
import me.mosling.datagate.core.ResourceMetadata;
import me.mosling.datagate.parser.QueryParameter;
import me.mosling.datagate.storage.JsonStorage;
import me.mosling.httpfixture.common.JsonHelper;
import me.mosling.httpfixture.security.PasswordHelper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;
import spark.Request;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toSet;
import static spark.Spark.post;

public class AuthenticationSimple
        implements Authentication {
    private static final Logger LOGGER = LogManager.getLogger(AuthenticationSimple.class);

    private static final String AUTHENTICATION_RESOURCE = "authentication-simple";
    private static final String ADMIN_USER = "admin";

    @Getter
    private AuthenticationConfig authenticationConfig = new AuthenticationConfig();
    @Getter
    @Setter
    private String landingPage = "/";

    private final String adminRole;
    private final JsonStorage jsonStorage;

    public AuthenticationSimple(JsonStorage jsonStorage, String adminRole, String adminPassword) {
        this.jsonStorage = jsonStorage;
        this.adminRole = adminRole;

        // create the metadata entry for access_user
        ResourceMetadata mde = new ResourceMetadata();
        mde.setTitle("Access User");
        mde.setResourceName(AUTHENTICATION_RESOURCE);
        mde.setPrimaryKey(User.FN_USERNAME);
        mde.setNeedRole(Stream.of(ADMIN_USER).collect(toSet()));
        mde.setListProperties(Arrays.asList(User.FN_USERNAME, User.FN_ROLES));
        mde.setMergeBeforeUpdate(true); // to not lose the hashcode without a given password

        mde.updateEntryFields();
        Metadata.put(mde);
        ConversionDictionary.add(mde.getResourceName(), ConversionDictionary.NODE_DATA,
                new ConversionPbkdf2("password", User.FN_HASH));

        // update development setting
        QueryParameter adminUser = QueryParameter.newEqual(User.FN_USERNAME, ADMIN_USER);

        // remove development mode admin user
        jsonStorage.delete(mde.getLocalStorageName(), adminUser, false);

        String generatedPasswd = adminPassword;
        if (null == adminPassword || adminPassword.isEmpty()) {
            if (Metadata.isDevelopmentMode()) {
                generatedPasswd = ADMIN_USER;
            } else {
                generatedPasswd = UUID.randomUUID().toString();
            }
        }

        // add an admin user in each case
        LOGGER.info("adding development user '{}' with secret '{}'", ADMIN_USER, generatedPasswd);

        String admin = JsonHelper.createJsonFromString(
                User.FN_USERNAME + ",admin,password," + generatedPasswd + "," + User.FN_ROLES
                        + ",[,admin,user,guest,AM_ROLE_ADMIN,]", ",");

        Conversion conversion = ConversionDictionary
                .find(mde.getResourceName(), ConversionDictionary.NODE_DATA);
        JSONObject jsonObject = new JSONObject(admin);

        jsonStorage.insert(mde.getLocalStorageName(), (conversion != null ? conversion.map(jsonObject) : jsonObject)
                .toString());

        LOGGER.info("admin-role: '{}' .. ready", adminRole);
    }

    public User login(String username, String password) {
        User user = null;

        ResourceMetadata rmd = Metadata.get(AUTHENTICATION_RESOURCE);
        if (null == rmd) {
            LOGGER.error("user management not active ...");
            return null;
        }

        List<String> l = jsonStorage.select(rmd.getLocalStorageName(),
                QueryParameter.newEqual(rmd.getPrimaryKey(), username));

        if (!l.isEmpty()) {
            JSONObject u = new JSONObject(l.get(0));
            if (u.has(User.FN_HASH) && PasswordHelper.validatePbkdf2Password(u.getString(User.FN_HASH),
                    password)) {
                if (u.has(User.FN_ROLES)) {
                    user = new User(username, u.getJSONArray(User.FN_ROLES).join(",").replaceAll("\"", ""),
                            adminRole);
                } else {
                    user = new User(username, "", "");
                }
            } else {
                LOGGER.error("non matching password for user '{}'", username);
            }
        } else {
            LOGGER.error("no user '{}' found", username);
        }

        return user;
    }

    @Override
    public void addSparkPaths() {
        post("/login", (req, res) -> {
            User user;
            if ((user = login(req.queryMap().get("user").value(), req.queryMap().get("password").value()))
                    != null) {
                req.session().attribute(SESSION_USER, user);
                LOGGER.debug("add user session '{}'", req.session(true).attribute(SESSION_USER).toString());
            }
            res.redirect(landingPage);
            return "";
        });

        Authentication.addGeneralLogoutPath(landingPage);
    }

    @Override
    public User determineAccessingUser(Request req)
            throws GeneralException {
        return req.session().attribute(SESSION_USER);
    }
}