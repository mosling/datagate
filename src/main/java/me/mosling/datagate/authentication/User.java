package me.mosling.datagate.authentication;

import lombok.Getter;
import lombok.Setter;
import me.mosling.datagate.core.Metadata;
import me.mosling.datagate.core.ResourceMetadata;
import me.mosling.httpfixture.common.StringHelper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.Collections;
import java.util.Set;
import java.util.TreeSet;

public class User {
    private static final Logger LOGGER = LogManager.getLogger(User.class);

    // define some field names
    public static final String FN_HASH = "hash";
    public static final String FN_ROLES = "roles";
    public static final String FN_USERNAME = "username";

    @Getter
    @Setter
    private Set<String> roles = Collections.emptySet();
    @Getter
    @Setter
    private String username;
    @Getter
    @Setter
    private boolean admin = false;
    @Getter
    @Setter
    private boolean logoutEnabled = true;

    public User(String username, String roles, String adminRole) {
        this.username = username;
        if (!roles.isEmpty()) {
            this.roles = new TreeSet<>();
            this.roles.addAll(Arrays.asList(roles.split(",")));
            this.admin = !StringHelper.nonNullStr(adminRole).isEmpty() && hasRole(adminRole);
        }
        LOGGER.info("create user '{}' with roles: {}{}", username, roles.isEmpty() ? "(not set)" : roles,
                isAdmin() ? " as ADMIN (based on '" + adminRole + "')" : " as USER");
    }

    // This method checks if the user has the required role to see this resource
    public boolean hasAccess(String r) {
        ResourceMetadata rmd = Metadata.get(r);

        if (rmd == null || rmd.getNeedRole().isEmpty() || isAdmin()) {
            // resource doesn't exists or need no roles or user is admin
            LOGGER.debug("Grant access to {} because {}",
                    rmd == null ? "unknown resource " + r : rmd.getResourceName(),
                    isAdmin() ? " user is admin" : "no role needed");
            return true;
        } else if (roles.isEmpty()) {
            // user has no roles but the resource needs roles
            LOGGER.debug("Forbidden access to {} because user hasn't any role", rmd.getResourceName());
            return false;
        }

        // both (resource,user) has roles, grant access if the intersection isn't empty
        long c = roles.stream().filter(rmd.getNeedRole()::contains).count();

        return c > 0;
    }

    // returns true if the user has the given role
    public boolean hasRole(String r) {
        return roles.contains(r);
    }

    public static User buildFromJson(String jsonUser, String roleArrayName, String adminRole) {
        String username;

        JSONObject uo = new JSONObject(jsonUser);
        if (uo.has("given_name") && uo.has("family_name")) {
            username = uo.getString("given_name") + " " + uo.getString("family_name");
        } else if (uo.has("email")) {
            username = uo.getString("email");
        } else if (uo.has("sub")) {
            username = uo.getString("sub");
        } else {
            LOGGER.error("no username ([given_name,family_name],email or sub) field found in '{}'", jsonUser);
            return null;
        }

        String roles = "";
        if (uo.has(roleArrayName)) {
            roles = uo.getJSONArray(roleArrayName).join("," + "").replaceAll("\"", "");
        }

        return new User(username, roles, adminRole);
    }
}
