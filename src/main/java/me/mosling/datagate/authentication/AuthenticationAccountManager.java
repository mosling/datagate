package me.mosling.datagate.authentication;

import lombok.Getter;
import lombok.Setter;
import me.mosling.datagate.core.GeneralException;
import me.mosling.datagate.fixture.Fixture;
import me.mosling.datagate.fixture.FixtureAccountManager;
import me.mosling.httpfixture.fixture.ResponseData;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;
import spark.Request;

import static spark.Spark.post;

public class AuthenticationAccountManager
        implements Authentication {
    private static final Logger LOGGER = LogManager.getLogger(AuthenticationAccountManager.class);

    @Getter
    @Setter
    private String landingPage = "/";
    @Getter
    private AuthenticationConfig authenticationConfig = new AuthenticationConfig();

    private FixtureAccountManager fixtureAm;
    private String adminRole;

    public AuthenticationAccountManager(Fixture fixtureAm, String adminRole) {
        if (fixtureAm instanceof FixtureAccountManager) {
            this.fixtureAm = (FixtureAccountManager) fixtureAm;
            this.adminRole = adminRole;

            LOGGER.info("using {} and admin-role: '{}' .. ready", fixtureAm.getName(), adminRole);
        } else {
            LOGGER.fatal("{} need fixture of type {}", this.getClass().getSimpleName().toUpperCase(),
                    FixtureAccountManager.class.getName().toUpperCase());
        }
    }

    public User login(String username, String password) {
        User user = null;

        if (null != fixtureAm && fixtureAm.login(username, password)) {
            ResponseData ud = fixtureAm.doGetRequest("", "dw/rest/v1/users/current");
            if (ud.getStatus() == 200) {
                JSONObject uo = new JSONObject(ud.getResponseContent());
                if (uo.has("mail") && uo.has("roles")) {
                    user = new User(uo.getString("mail"),
                            uo.getJSONArray(User.FN_ROLES).join(",").replaceAll("\"", ""), adminRole);
                }
            } else {
                LOGGER.error("No current user information available for {}", username);
                ud.showResponse(true, Level.ERROR);
            }
        } else {
            LOGGER.error("can't login user '{}'", username);
        }

        return user;
    }

    @Override
    public void addSparkPaths() {
        post("/login", (req, res) -> {
            User user;
            if ((user = login(req.queryMap().get("user").value(), req.queryMap().get("password").value()))
                    != null) {
                req.session().attribute(SESSION_USER, user);
            }
            res.redirect(landingPage);
            return "";
        });

        Authentication.addGeneralLogoutPath(landingPage);
    }

    @Override
    public User determineAccessingUser(Request req)
            throws GeneralException {
        return req.session().attribute(SESSION_USER);
    }
}
