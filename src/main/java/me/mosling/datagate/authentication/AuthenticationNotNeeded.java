package me.mosling.datagate.authentication;

import lombok.Getter;
import lombok.Setter;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import spark.Request;

import static spark.Spark.before;

public class AuthenticationNotNeeded
        implements Authentication {
    private static final Logger LOGGER = LogManager.getLogger(AuthenticationNotNeeded.class);

    @Getter
    private AuthenticationConfig authenticationConfig = new AuthenticationConfig();
    @Getter
    @Setter
    private String landingPage = "";
    private final boolean simulatedAdmin;
    private final String simulatedRoles;

    public AuthenticationNotNeeded(boolean simulatedAdmin, String simulatedRoles) {
        this.simulatedAdmin = simulatedAdmin;
        this.simulatedRoles = simulatedRoles;

        LOGGER.info("with roles: '{}' and admin-role: '{}' .. ready", simulatedRoles, simulatedAdmin);
    }

    @Override
    public void addSparkPaths() {
        // no authentication needed
        before("/*", (req, res) -> determineAccessingUser(req));
    }

    @Override
    public User determineAccessingUser(Request req) {
        User accessingUser = req.session().attribute(SESSION_USER);

        if (null == accessingUser) {
            accessingUser = new User("anonymous", simulatedRoles, "");
            accessingUser.setAdmin(simulatedAdmin);
            accessingUser.setLogoutEnabled(false);

            req.session().attribute(SESSION_USER, accessingUser);
        }

        return accessingUser;
    }
}
