package me.mosling.datagate.authentication;

import me.mosling.datagate.common.Paths;
import me.mosling.datagate.core.GeneralException;
import me.mosling.datagate.core.Metadata;
import me.mosling.datagate.core.ResourceMetadata;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import spark.Request;

import static spark.Spark.get;

public interface Authentication {
    Logger LOGGER = LogManager.getLogger(Authentication.class);

    String SESSION_USER = "user-session";

    void setLandingPage(String landingPage);

    String getLandingPage();

    void addSparkPaths();

    AuthenticationConfig getAuthenticationConfig();

    /**
     * This function implements without flow (i.e. no GeneralException throwing) the determining of
     * the cuurent user, which can also be null.
     *
     * @param req the request object from sparkjava
     *
     * @return a user object or null
     *
     * @throws GeneralException a general exception for unauthorized access or not existing resources
     */
    User determineAccessingUser(Request req)
            throws GeneralException;

    static ResourceMetadata examineAccess(Request req, Authentication authentication, String resourceParameterName)
            throws GeneralException {
        boolean isDataAccess = req.pathInfo().startsWith(Metadata.pathFor(Paths.WEB_DATA));
        boolean asJson = req.session().attribute(SESSION_USER) == null || isDataAccess;

        // 1. check general access (i.e. the user exists), if not return null without GeneralException, because the
        //    user must have the possibility to login into the system
        User accessingUser = authentication.determineAccessingUser(req);
        if (null == accessingUser) {
            if (Metadata.isDevelopmentMode()) {
                LOGGER.info("UNKNOWN-USER: {}::{}", req.requestMethod(), req.url());
            }
            if (isDataAccess) {
                throw new GeneralException(401, asJson, "Please login before requesting resources.");
            }
            return null;
        }

        // 2. check if the requested resource exists
        String mdn = req.params(resourceParameterName);
        ResourceMetadata rmd = Metadata.get(mdn);
        if (null == rmd) {
            throw new GeneralException(404, asJson, String.format("Resource <b>%s</b> not found.", mdn));
        }

        // 3. check if the requested details action is possible
        if (req.pathInfo().startsWith(Metadata.pathFor(Paths.WEB_DETAILS)) && !rmd.isHasDetailsView()) {
            throw new GeneralException(404, asJson,
                    String.format("Resource <b>%s</b> has no details view configured.", mdn));
        }

        // 4. check if the user has access to this resource
        if (!accessingUser.hasAccess(mdn)) {
            throw new GeneralException(401, asJson, String.format("Missing a role [%s] to access resource <b>%s</b>",
                    String.join(",", rmd.getNeedRole()), mdn));
        }

        // 5. return the resource metadata object if all tests passed
        return rmd;
    }

    static void addGeneralLogoutPath(String landingPage) {
        get("/logout", (req, res) -> {
            if (req.session().attribute(SESSION_USER) != null) {
                req.session().removeAttribute(SESSION_USER);
            }
            res.redirect(landingPage);
            return "";
        });
    }

}
