package me.mosling.datagate.authentication;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import me.mosling.httpfixture.jwt.JwtRecord;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class OpenIdConnect {
    private static final Logger LOGGER = LogManager.getLogger(OpenIdConnect.class);

    @Getter
    @Setter
    @JsonProperty("access_token")
    String accessToken = "";
    @Getter
    @Setter
    @JsonProperty("refresh_token")
    String refreshToken = "";
    @Getter
    @Setter
    @JsonProperty("scope")
    String scope = "";
    @Getter
    @Setter
    @JsonProperty("id_token")
    String idToken = "";
    @Getter
    @Setter
    @JsonProperty("token_type")
    String tokenType = "";
    @Getter
    @Setter
    @JsonProperty("expires_in")
    Integer expiresIn = 0;

    public void showConnection(Level l) {
        if (LOGGER.isEnabled(l)) {
            LOGGER.log(l, "Access-Token  : {}", accessToken);
            LOGGER.log(l, "Refresh-Token : {}", refreshToken);
            LOGGER.log(l, "Scope         : {}", scope);
            LOGGER.log(l, "Token-Type    : {}", tokenType);
            LOGGER.log(l, "Expires In    : {}", expiresIn);

            JwtRecord idt = new JwtRecord().importString(idToken);
            LOGGER.log(l, "IdToken Header: {}", idt.getHeader());
            LOGGER.log(l, "IdToken Body  : {}", idt.getPayload());
        }

    }
}
