package me.mosling.datagate.authentication;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Getter;
import lombok.Setter;
import me.mosling.datagate.core.GeneralException;
import me.mosling.httpfixture.common.StringHelper;
import me.mosling.httpfixture.fixture.*;
import me.mosling.httpfixture.security.JksManager;
import me.mosling.httpfixture.security.JksManagerArgs;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;
import spark.Request;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.UUID;

import static spark.Spark.get;
import static spark.Spark.redirect;

/**
 * This class offers the possibility to get access to the application using OpenID Connect using the
 * authorization_code grant_type.
 * To do this we need some configuration, to not have a bunch of parameters the configuration file
 * which is used by normal HttpClients are used, this file needs the following entries:
 * <ul>
 * <li>auth-server is the connector for authorization sever</li>
 * <li>api-client is the client authentication for this application</li>
 * <li>openid-configuration address is used to get the endpoints from the authorization server</li>
 * <li></li>
 * </ul>
 * To do a successful OpenID Connect login there are some additional settings necessary
 * <ul>
 * <li>landingPage to have a static page which is used after login/logout</li>
 * <li>redirectUrl which is used by the authorization server to send the code tho the application</li>
 * <li>space separated list of scopes which are used to receive information from authorization server</li>
 * </ul>
 * Normally the redirectUrl must be set at the authorization server as parameter of the application client.
 */
public class AuthenticationOpenIdConnect
        extends BaseHttpFixture
        implements Authentication {
    private static final Logger LOGGER = LogManager.getLogger(AuthenticationOpenIdConnect.class);

    public static final String SESSION_OIDC = "oidc";
    public static final String AUTH_HEADER = "Authorization";

    @Getter
    private AuthenticationConfig authenticationConfig = new AuthenticationConfig()
            .loginFields(false)
            .methodAction("get");
    @Getter
    @Setter
    private String landingPage = "/";

    @Getter
    private String callbackAddress;
    @Getter
    private String redirectUrl;
    @Getter
    private String authorizeUrl = "";
    @Getter
    private String userinfoUrl = "";
    @Getter
    private String tokenUrl = "";
    @Getter
    private String endSessionUrl = "";
    @Getter
    private String scopes;
    @Getter
    private String state = UUID.randomUUID().toString().substring(0, 8);
    @Getter
    private String apiClientKey = "api-client";
    @Getter
    private String authServerKey = "auth-server";
    @Getter
    private String adminRole;

    private final ObjectMapper mapper = new ObjectMapper();

    public AuthenticationOpenIdConnect() {
        LOGGER.fatal("construct an object from class '{}' without parameters will not work",
                this.getClass().getName());
    }

    public AuthenticationOpenIdConnect(String configuration, String redirectUrl, String scopes, String adminRole)
            throws MalformedURLException {
        build(httpArgs(configuration), jksManager());
        HttpClientConfig config = getClient().httpClientConfig();

        checkArgument(config.getAuths().get(apiClientKey), apiClientKey + " authorization");
        checkArgument(config.getConnections().get(authServerKey), authServerKey + " connection");

        this.callbackAddress = new URL(redirectUrl).getPath();
        this.redirectUrl = redirectUrl;
        this.scopes = scopes;
        this.adminRole = adminRole;
        getOpenIdConfiguration();

        LOGGER.info("Redirect-URI : {}", redirectUrl);
        LOGGER.info("Callback path: {}", this.callbackAddress);
        LOGGER.info("   Admin Role: {}", this.adminRole);
        LOGGER.info(".. ready.");
    }

    private void checkArgument(Object obj, String msg) {
        if (null == obj) {
            LOGGER.error("Missing configuration parameter {}", msg);
        }
    }

    private HttpClientArgs httpArgs(String configuration) {
        return new HttpClientArgs().requestTimeoutMs(20000).configFile(configuration).enableRedirect(true);
    }

    private JksManager jksManager() {
        JksManagerArgs ja = new JksManagerArgs();
        ja.truststore("certs/truststore.jks").truststoreSecret("4demandware");

        return new JksManager(ja);
    }

    private void getOpenIdConfiguration() {
        ResponseData rd = executeHttp("GET", authServerKey, "", "openid-configuration", "", 200);

        rd.showResponse(true, Level.DEBUG);
        JSONObject joConfig = new JSONObject(rd.getResponseContent());

        authorizeUrl = joConfig.getString("authorization_endpoint");
        userinfoUrl = joConfig.getString("userinfo_endpoint");
        tokenUrl = joConfig.getString("token_endpoint");
        endSessionUrl = joConfig.getString("end_session_endpoint");

        LOGGER.info("Issuer       : {}", joConfig.getString("issuer"));
        LOGGER.info("AuthorizeUrl : {}", authorizeUrl);
        LOGGER.info("UserinfoUrl  : {}", userinfoUrl);
        LOGGER.info("TokenUrl     : {}", tokenUrl);
    }

    public User getUserInformation(Request req, String authorization) {
        HttpRequest request = HttpClientFactory.defaultRequest("GET", false);
        request.addHeader(AUTH_HEADER, authorization);
        ResponseData rd = getClient().executeAddress(userinfoUrl, request);
        if (200 == rd.getStatus()) {
            return User.buildFromJson(rd.getResponseContent(), "roles", adminRole);
        }

        return null;
    }

    @Override
    public User determineAccessingUser(Request req)
            throws GeneralException {
        User accessUser = req.session().attribute(SESSION_USER);

        String authHeader = req.headers(AUTH_HEADER);

        if (null == accessUser && authHeader != null && authHeader.toUpperCase().startsWith("BEARER")) {
            LOGGER.debug("get user information for bearer token ...");
            accessUser = getUserInformation(req, authHeader);
        }

        return accessUser;
    }

    public String getLoginRedirect() {
        return String.format("%s?client_id=%s&redirect_uri=%s&scope=%s&response_type=code&state=%s", authorizeUrl,
                getClient().httpClientConfig().getAuths().get(apiClientKey).getAuthName(),
                StringHelper.encodeUrl(redirectUrl), StringHelper.encodeUrl(scopes), state);
    }

    public void addSparkPaths() {
        redirect.get("/login", getLoginRedirect());

        get(callbackAddress, (req, res) -> {
            HttpRequest tr = HttpClientFactory.defaultRequest("POST", false);
            tr.addHeader("Content-Type", "application/x-www-form-urlencoded");
            tr.addHeader(AUTH_HEADER,
                    getClient().httpClientConfig().getAuths().get(apiClientKey).getHttpAuthorization());
            String b = String.format("grant_type=authorization_code&code=%s&redirect_uri=%s",
                    req.queryMap("code").value(), StringHelper.encodeUrl(redirectUrl));
            tr.setTextBody(b);
            ResponseData rd = getClient().executeAddress(tokenUrl, tr);
            rd.showResponse(true, Level.DEBUG);
            if (rd.getStatus() == 200) {
                OpenIdConnect oidc = mapper.readValue(rd.getResponseContent(), OpenIdConnect.class);
                if (oidc != null) {
                    oidc.showConnection(Level.DEBUG);
                    req.session().attribute(SESSION_OIDC, oidc);
                    req.session().attribute(SESSION_USER, getUserInformation(req, String.format("%s %s", oidc.getTokenType(), oidc.getAccessToken())));
                }
                res.redirect(landingPage);
                return "";

            }
            throw new GeneralException(rd.getStatus(), false, rd.getResponseContent());
        });

        get("/logout", (req, res) -> {
            OpenIdConnect oidc = req.session().attribute(SESSION_OIDC);
            req.session().removeAttribute(SESSION_OIDC);
            req.session().removeAttribute(SESSION_USER);
            if (oidc != null) {
                final HttpRequest tr = HttpClientFactory.defaultRequest("GET", false);
                final ResponseData rd = getClient().executeAddress(
                        endSessionUrl + "?id_token_hint=" + oidc.getIdToken(), tr);
                rd.showResponse(true, Level.DEBUG);
                if (rd.getStatus() != 204) {
                    throw new GeneralException(rd.getStatus(), false, rd.getResponseContent());
                }
            }
            res.redirect(landingPage);
            return "";
        });

    }
}
