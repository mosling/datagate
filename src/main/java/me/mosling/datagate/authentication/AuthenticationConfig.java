package me.mosling.datagate.authentication;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Accessors(fluent = true)
public class AuthenticationConfig {
    @Getter
    @Setter
    private boolean loginFields = true;
    @Getter
    @Setter
    private String methodAction = "post";
}
