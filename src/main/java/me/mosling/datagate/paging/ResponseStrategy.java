package me.mosling.datagate.paging;

import me.mosling.datagate.connector.Connector;
import me.mosling.datagate.core.ResourceMetadata;
import org.apache.logging.log4j.Logger;
import org.json.JSONArray;

public interface ResponseStrategy {

    String helper();

    boolean checkParameter(ResourceMetadata rmd, String[] parameter);

    void fetchData(Connector connector, ResourceMetadata rmd, String[] parameter);

    default void addArrayToCache(Connector connector, ResourceMetadata rmd, JSONArray content, boolean progressBar, Logger logger) {
        int cnt = content.length();
        int x = cnt / 5;
        int c = 1;
        for (int row = 0; row < cnt; ++row) {
            if (null != logger && progressBar && c * x < row) {
                c++;
                logger.info("progress add array for '{}' is {}/{}", rmd.getResourceName(), row, cnt);
            }
            connector.getDataManager().addOrUpdateData(rmd, connector.getName(), content.getJSONObject(row));
        }
    }
}
