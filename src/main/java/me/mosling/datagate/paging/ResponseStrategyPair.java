package me.mosling.datagate.paging;

import me.mosling.datagate.connector.Connector;
import me.mosling.datagate.core.ResourceMetadata;
import me.mosling.httpfixture.fixture.ResponseData;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;

public class ResponseStrategyPair implements ResponseStrategy {

    private static final Logger LOGGER = LogManager.getLogger(ResponseStrategyPair.class);

    @Override
    public String helper() {
        return "PAIR,<generated-id-fieldname>,<generated-value-fieldname>";
    }

    @Override
    public boolean checkParameter(ResourceMetadata rmd, String[] parameter) {
        if (parameter.length != 3) {
            LOGGER.error(
                    "[{}] wrong parameter for strategy '{}' but is '{}'",
                    rmd.getResourceName(), helper(), rmd.getPagingStrategy());
            return false;
        }

        return true;
    }

    @Override
    public void fetchData(Connector connector, ResourceMetadata rmd, String[] parameter) {
        String url = rmd.getResourcePath();

        ResponseData rd = connector.getFixture().doGetRequest(rmd.getResourceName(), url);
        if (200 == rd.getStatus()) {
            JSONObject r = new JSONObject(rd.getResponseContent());
            for (String k : r.keySet()) {
                if (r.get(k) instanceof JSONObject) {
                    JSONObject jo = r.getJSONObject(k);
                    jo.put(parameter[1], k);
                    connector.getDataManager().addOrUpdateData(rmd, connector.getName(), jo);
                } else {
                    JSONObject jo = new JSONObject();
                    jo.put(parameter[1], k);
                    jo.put(parameter[2], r.getString(k));
                    connector.getDataManager().addOrUpdateData(rmd, connector.getName(), jo);
                }
            }
        }
    }
}
