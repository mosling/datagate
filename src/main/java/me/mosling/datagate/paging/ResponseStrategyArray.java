package me.mosling.datagate.paging;

import me.mosling.datagate.connector.Connector;
import me.mosling.datagate.core.ResourceMetadata;
import me.mosling.httpfixture.fixture.ResponseData;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

public class ResponseStrategyArray implements ResponseStrategy {

    private static final Logger LOGGER = LogManager.getLogger(ResponseStrategyArray.class);

    @Override
    public String helper() {
        return "ARRAY,(<array-fieldname> | '.' | <empty string>)";
    }

    @Override
    public boolean checkParameter(ResourceMetadata rmd, String[] parameter) {
        if (parameter.length != 2) {
            LOGGER.error(
                    "[{}] wrong parameter for strategy '{}' but is '{}'",
                    rmd.getResourceName(), helper(), rmd.getPagingStrategy());
            return false;
        }
        return true;
    }

    @Override
    public void fetchData(Connector connector, ResourceMetadata rmd, String[] parameter) {
        String url = rmd.getResourcePath();

        ResponseData rd = connector.getFixture().doGetRequest(rmd.getResourceName(), url);
        if (200 == rd.getStatus()) {
            if (".".equalsIgnoreCase(parameter[1]) || parameter[1].isEmpty()) {
                addArrayToCache(connector, rmd, new JSONArray(rd.getResponseContent()), true, LOGGER);
            } else {
                JSONObject r = new JSONObject(rd.getResponseContent());
                addArrayToCache(connector, rmd, r.getJSONArray(parameter[1]), true, LOGGER);
            }
        }
    }
}
