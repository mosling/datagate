package me.mosling.datagate.paging;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Map;
import java.util.TreeMap;

public class ResponseStrategyDictionary {

    private static final Logger LOGGER = LogManager.getLogger(ResponseStrategyDictionary.class);

    private static Map<String, ResponseStrategy> responsePagingStrategyMap = new TreeMap<>();

    public static void addSystemPagingStrategies() {
        if (!responsePagingStrategyMap.containsKey("NEXT")) {
            addResponsePagingStrategy("NEXT", new ResponseStrategyNext());
        }
        if (!responsePagingStrategyMap.containsKey("PAGE")) {
            addResponsePagingStrategy("PAGE", new ResponseStrategyPage());
        }
        if (!responsePagingStrategyMap.containsKey("PAIR")) {
            addResponsePagingStrategy("PAIR", new ResponseStrategyPair());
        }
        if (!responsePagingStrategyMap.containsKey("ENTRY")) {
            addResponsePagingStrategy("ENTRY", new ResponseStrategyEntry());
        }
        if (!responsePagingStrategyMap.containsKey("ARRAY")) {
            addResponsePagingStrategy("ARRAY", new ResponseStrategyArray());
        }
    }

    public static void addResponsePagingStrategy(String name, ResponseStrategy rps) {
        String mapName = getMapName(name);
        if (responsePagingStrategyMap.containsKey(mapName)) {
            LOGGER.warn("overwrite existing response paging strategy {}", mapName);
        }

        responsePagingStrategyMap.put(mapName, rps);
    }

    public static ResponseStrategy findResponsePagingStrategy(String name) {

        return responsePagingStrategyMap.getOrDefault(getMapName(name), null);
    }

    private static String getMapName(String name) {
        String mapName = name;
        if (name.contains(",")) {
            mapName = name.split(",")[0];
        }

        return mapName.toUpperCase();
    }
}
