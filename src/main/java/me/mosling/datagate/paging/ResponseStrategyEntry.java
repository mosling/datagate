package me.mosling.datagate.paging;

import me.mosling.datagate.connector.Connector;
import me.mosling.datagate.core.ResourceMetadata;
import me.mosling.httpfixture.fixture.ResponseData;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;

public class ResponseStrategyEntry implements ResponseStrategy {
    private static final Logger LOGGER = LogManager.getLogger(ResponseStrategyEntry.class);

    @Override
    public String helper() {
        return "ENTRY";
    }

    @Override
    public boolean checkParameter(ResourceMetadata rmd, String[] parameter) {
        return true;
    }

    @Override
    public void fetchData(Connector connector, ResourceMetadata rmd, String[] parameter) {
        String url = rmd.getResourcePath();

        ResponseData rd = connector.getFixture().doGetRequest(rmd.getResourceName(), url);
        if (200 == rd.getStatus()) {
            connector.getDataManager().addOrUpdateData(rmd, connector.getName(), new JSONObject(rd.getResponseContent()));
        }
    }
}
