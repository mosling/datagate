package me.mosling.datagate.paging;

import me.mosling.datagate.common.JsonObjectHelper;
import me.mosling.datagate.connector.Connector;
import me.mosling.datagate.core.ResourceMetadata;
import me.mosling.httpfixture.common.StringHelper;
import me.mosling.httpfixture.fixture.ResponseData;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

public class ResponseStrategyPage implements ResponseStrategy {

    private static final Logger LOGGER = LogManager.getLogger(ResponseStrategyPage.class);

    @Override
    public String helper() {
        return "PAGE,<page-size>,<array-fieldname>,<total-page-fieldname>";
    }

    @Override
    public boolean checkParameter(ResourceMetadata rmd, String[] parameter) {
        if (parameter.length != 4) {
            LOGGER.error(
                    "[{}] wrong parameter for strategy '{}' but is '{}'",
                    rmd.getResourceName(), helper(), rmd.getPagingStrategy());
            return false;
        }

        return true;
    }

    @Override
    public void fetchData(Connector connector, ResourceMetadata rmd, String[] parameter) {
        int page = 0;
        int totalPages = 1;
        int pageSize = 1000;

        try {
            pageSize = Integer.valueOf(parameter[1]);
        } catch (NumberFormatException ex) {
            LOGGER.error("{} isn't a number -- default to {}", parameter[1], pageSize);
        }

        String arrayField = parameter[2];
        String totalPageField = parameter[3];

        while (page < totalPages) {
            ResponseData rd = connector.getFixture().doGetRequest(rmd.getResourceName(),
                    String.format("%s?size=%d&page=%d", rmd.getResourcePath(), pageSize, page));

            if (200 == rd.getStatus()) {
                long pt = System.currentTimeMillis();
                JSONObject r = new JSONObject(rd.getResponseContent());
                String tps = JsonObjectHelper.getField(r, totalPageField);
                try {
                    totalPages = Integer.valueOf(tps);
                } catch (NumberFormatException ex) {
                    LOGGER.error("total page field '{}' contains no number {} --> end process", totalPageField, tps);
                    page = totalPages;
                    continue;
                }

                JSONArray content = r.getJSONArray(arrayField);
                addArrayToCache(connector, rmd, content, false, LOGGER);

                pt = System.currentTimeMillis() - pt;

                LOGGER.info("addedData       : {} for page ({}/{}) takes (fetch={}, process={})", content.length(),
                        1 + page, totalPages, StringHelper.millisecondsToTimeString(rd.getResponseTimeMillis(), true),
                        StringHelper.millisecondsToTimeString(pt, true));

                page++;
            } else {
                LOGGER.error("fetch page {} from {} failed with status {}", page, rmd.getResourcePath(),
                        rd.getStatus());
                page = totalPages;
            }
        }
    }
}
