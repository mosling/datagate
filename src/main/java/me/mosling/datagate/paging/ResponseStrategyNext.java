package me.mosling.datagate.paging;

import me.mosling.datagate.connector.Connector;
import me.mosling.datagate.core.ResourceMetadata;
import me.mosling.httpfixture.common.StringHelper;
import me.mosling.httpfixture.fixture.ResponseData;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

public class ResponseStrategyNext implements ResponseStrategy {

    private static final Logger LOGGER = LogManager.getLogger(ResponseStrategyNext.class);

    @Override
    public String helper() {
        return "NEXT,<array-fieldname>,<next-fieldname>";
    }

    @Override
    public boolean checkParameter(ResourceMetadata rmd, String[] parameter) {
        if (parameter.length != 3) {
            LOGGER.error(
                    "[{}] wrong parameter for strategy '{}' but is '{}'",
                    rmd.getResourceName(), helper(), rmd.getPagingStrategy());
            return false;
        }

        return true;
    }

    @Override
    public void fetchData(Connector connector, ResourceMetadata rmd, String[] parameter) {

        String nextUrl = rmd.getResourcePath();
        String arrayField = parameter[1];
        String nextField = parameter[2];

        while (!nextUrl.isEmpty()) {
            ResponseData rd = connector.getFixture().doGetRequest(rmd.getResourceName(), nextUrl);
            if (200 == rd.getStatus()) {
                long pt = System.currentTimeMillis();
                JSONObject r = new JSONObject(rd.getResponseContent());

                JSONArray content = r.getJSONArray(arrayField);
                addArrayToCache(connector, rmd, content, false, LOGGER);

                pt = System.currentTimeMillis() - pt;
                LOGGER.info("addedData       : {} for {} takes (fetch={}, process={})", content.length(), nextUrl,
                        StringHelper.millisecondsToTimeString(rd.getResponseTimeMillis(), true),
                        StringHelper.millisecondsToTimeString(pt, true));

                nextUrl = r.isNull(nextField) ? "" : r.getString(nextField);
            } else {
                nextUrl = "";
            }
        }
    }
}
