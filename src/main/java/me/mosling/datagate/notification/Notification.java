package me.mosling.datagate.notification;

public interface Notification {
    void notify(NotificationEvent event, String resourceKey, String message);

}
