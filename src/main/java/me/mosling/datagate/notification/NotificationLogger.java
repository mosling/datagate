package me.mosling.datagate.notification;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class NotificationLogger
        implements Notification {
    private static final Logger LOGGER = LogManager.getLogger(NotificationLogger.class);
    private static final Level always = Level.getLevel("ALWAYS");

    @Override
    public void notify(NotificationEvent event, String resourceKey, String message) {
        String key = String.format("%s.%s", resourceKey, event.getShortname());
        LOGGER.log(always, "notify {} with message ('{}')", key, message);
    }
}
