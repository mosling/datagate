package me.mosling.datagate.notification;

public enum NotificationEvent {
    NEW("new"),
    UPDATE("update"),
    REMOVE("remove"),
    HEARTBEAT("heartbeat"),
    COMMON("common");

    private final String shortname;

    NotificationEvent(String shortname) {
        this.shortname = shortname;
    }

    public String getShortname() {
        return shortname;
    }

}
