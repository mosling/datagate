package me.mosling.datagate.notification;

import me.mosling.datagate.mqtt.MessageClient;
import me.mosling.httpfixture.common.StringHelper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Date;

public class NotificationPublisher
        implements Notification {
    private static final Logger LOGGER = LogManager.getLogger(NotificationPublisher.class);
    private static final String TS = "timestamp";

    private final MessageClient messageClient;

    public NotificationPublisher(MessageClient messageClient) {
        this.messageClient = messageClient;
    }

    @Override
    public void notify(NotificationEvent event, String resourceKey, String message) {
        if (null == messageClient) {
            LOGGER.error("message client is NULL, please set during object construction ");
            return;
        }

        String publishMessage = message;
        try {
            JSONObject j = new JSONObject(message);
            if (!j.has(TS)) {
                j.put(TS, StringHelper.getUtcString(new Date()));
                publishMessage = j.toString();
            }
        } catch (JSONException e) {
            // ignore this exception, there is no JSON message and publish string only
        }

        String routingKey = String.format("%s.%s", resourceKey, event.getShortname());
        messageClient.publish(routingKey, publishMessage);
    }
}
