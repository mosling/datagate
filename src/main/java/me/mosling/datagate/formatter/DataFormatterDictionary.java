package me.mosling.datagate.formatter;

import me.mosling.datagate.core.GeneralException;
import me.mosling.httpfixture.common.StringHelper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.HashMap;
import java.util.Map;

public class DataFormatterDictionary {
    private static final Logger LOGGER = LogManager.getLogger(DataFormatterDictionary.class);

    private static Map<String, DataFormatter> dictionary = new HashMap<>();

    public static final String DEFAULT_FORMAT = "json";

    private DataFormatterDictionary() {
        // service dictionary
    }

    public static void add(String name, DataFormatter formatter) {
        if (null != formatter) {
            if (dictionary.containsKey(name)) {
                String fc = dictionary.get(name).getClass().getName();
                String nfc = formatter.getClass().getName();
                if (!nfc.equalsIgnoreCase(fc)) {
                    LOGGER.warn("overwrite existing entry {}({}) with {}", name, fc, nfc);
                }
            }

            dictionary.put(name, formatter);
        }
    }

    public static DataFormatter find(String name)
            throws GeneralException {
        if (!dictionary.containsKey(DEFAULT_FORMAT)) {
            add(DEFAULT_FORMAT, new DefaultDataFormatter());
        }

        if (dictionary.containsKey(name)) {
            return dictionary.get(name);
        } else if (StringHelper.nonNullStr(name).isEmpty() || DEFAULT_FORMAT.equalsIgnoreCase(name)) {
            return dictionary.get(DEFAULT_FORMAT);
        } else {
            String errMsg = String.format("unknown format '%s' possible formats are (%s)", name, supportedFormats());
            throw new GeneralException(400, true, errMsg);
        }
    }

    public static String supportedFormats() {
        return String.join(",", dictionary.keySet());
    }
}
