package me.mosling.datagate.formatter;

import me.mosling.datagate.common.JsonObjectHelper;
import org.json.JSONObject;
import spark.Response;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class CsvDataFormatter
        implements DataFormatter {
    private boolean withHeader;
    private String separator;
    private String quotationMark;

    public CsvDataFormatter(boolean withHeader, String separator, String quotationMark) {
        this.withHeader = withHeader;
        this.separator = separator;
        this.quotationMark = quotationMark;
    }

    @Override
    public String format(List<JSONObject> jsonObjectList, List<String> fieldOrder) {
        if (jsonObjectList.isEmpty()) {
            return "";
        }

        List<String> fields = fieldOrder != null ? new ArrayList<>(fieldOrder) : Collections.emptyList();

        if (fields.isEmpty()) {
            fields = new ArrayList<>();
            // if no fields are set, use the first level keys from the json
            JSONObject fo = jsonObjectList.get(0);
            Iterator<String> it = fo.keys();
            while (it.hasNext()) {
                fields.add(it.next());
            }
        } else {
            fields.replaceAll(s -> s.replace(">", "_"));
        }

        StringBuilder sb = new StringBuilder();

        if (withHeader) {
            sb.append(String.join(separator, fields));
            sb.append(System.lineSeparator());
        }

        for (JSONObject jo : jsonObjectList) {
            boolean first = true;
            for (String f : fields) {
                if (!first) {
                    sb.append(separator);
                } else {
                    first = false;
                }
                String s = JsonObjectHelper.getField(jo, f);
                if (s.contains(separator)) {
                    sb.append(quotationMark);
                    sb.append(s);
                    sb.append(quotationMark);
                } else {
                    sb.append(s);
                }
            }
            sb.append(System.lineSeparator());
        }

        return sb.toString();
    }

    @Override
    public void modifySparkResponse(String resourceName, Response response) {
        response.type("application/csv");
        response.header("Content-Disposition", String.format("attachment; filename=\"%s.csv\"", resourceName));
    }
}
