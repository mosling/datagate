package me.mosling.datagate.formatter;

import org.json.JSONObject;
import spark.Response;

import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

public class DefaultDataFormatter
        implements DataFormatter {
    @Override
    public String format(List<JSONObject> jsonObjectList, List<String> orderedList) {
        AtomicBoolean b = new AtomicBoolean(true);
        int cnt = 0;
        StringBuilder sb = new StringBuilder();

        sb.append("{\"content\": [");
        for (JSONObject obj : jsonObjectList) {
            sb.append(b.getAndSet(false) ? "" : ",");
            sb.append(obj.toString());
            cnt++;
        }
        sb.append(String.format("], \"page\": { \"totalElements\": %d } } ", cnt));

        return sb.toString();
    }

    @Override
    public void modifySparkResponse(String resourceName, Response response) {
        response.type("application/json");
    }

}
