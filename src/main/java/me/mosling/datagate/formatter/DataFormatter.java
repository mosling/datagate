package me.mosling.datagate.formatter;

import org.json.JSONObject;
import spark.Response;

import java.util.List;

public interface DataFormatter {
    String format(List<JSONObject> jsonObjectList, List<String> fieldOrder);

    void modifySparkResponse(String resourceName, Response response);
}
