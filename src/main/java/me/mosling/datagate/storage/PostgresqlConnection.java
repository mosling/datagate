package me.mosling.datagate.storage;

import lombok.Getter;
import lombok.Setter;
import org.apache.commons.dbcp2.BasicDataSource;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.net.URI;
import java.net.URISyntaxException;
import java.sql.SQLException;

public class PostgresqlConnection {
    private static final Logger LOGGER = LogManager.getLogger(PostgresqlConnection.class);
    private static final String CONNECTION_CLASSNAME = org.postgresql.Driver.class.getName();
    private boolean connected = false;

    @Getter
    @Setter
    private String schema = "";
    @Getter
    private String databaseUrl;
    @Getter
    private BasicDataSource connectionPool;

    public PostgresqlConnection(String databaseUrl)
            throws URISyntaxException {
        if (null == databaseUrl || databaseUrl.isEmpty()) {
            LOGGER.error("database connection parameter not set");
            return;
        }

        if (!databaseUrl.startsWith("postgres")) {
            LOGGER.warn("databaseUrl='{}' is set but doesn't start with 'postgres://'", databaseUrl);
            return;
        }

        this.databaseUrl = databaseUrl;
        URI dbUri = new URI(databaseUrl);

        String dbPort = dbUri.getPort() > 0 ? String.format(":%d", dbUri.getPort()) : "";
        String dbUrl = String.format("jdbc:postgresql://%s%s%s", dbUri.getHost(), dbPort, dbUri.getPath());
        connectionPool = new BasicDataSource();

        if (dbUri.getUserInfo() != null) {
            String[] userInfo = dbUri.getUserInfo().split(":");
            if (userInfo.length > 0)
            {
                connectionPool.setUsername(userInfo[0]);
                if (userInfo.length > 1)
                {
                    connectionPool.setPassword(userInfo[1]);
                }
            }
        }
        connectionPool.setDriverClassName(CONNECTION_CLASSNAME);
        connectionPool.setUrl(dbUrl);
        connectionPool.setInitialSize(1);
        connectionPool.setMaxTotal(50);
        connectionPool.setValidationQuery("select 1");
        connected = true;
    }

    public void closeConnection() {
        if (null != this.connectionPool) {
            try {
                this.connectionPool.close();
                connected = false;
            } catch (SQLException e) {
                LOGGER.error("closing database connection {}", databaseUrl, e);
            }
        }
    }

    public String getTableName(String tabName) {
        if (!schema.isEmpty()) {
            return schema + '.' + tabName;
        }
        return tabName;
    }

    public String convertToTimestamp(String value) {
        return "to_timestamp(" + value + ')';
    }

    public String convertToDateString(String colName) {
        return "to_char(" + colName + ",'YYYY-MM-DD HH24:MI:SS')";
    }

    @SuppressWarnings("SpellCheckingInspection")
    public String convertToUnixtime(String colName) {
        return "extract(epoch from " + colName + ')';
    }

    // The current Postgresql driver doesn't support this feature, why???
    public boolean isConnected() {
        return this.connected;
    }
}
