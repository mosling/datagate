package me.mosling.datagate.storage;

import me.mosling.datagate.common.JsonObjectHelper;
import me.mosling.datagate.parser.QueryFilterEntry;
import me.mosling.datagate.parser.QueryParameter;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.stream.Collectors;

class JsonStorageMemoryTable {
    private static final Logger LOGGER = LogManager.getLogger(JsonStorageMemoryTable.class);

    private final List<JSONObject> jsonData = new LinkedList<>();
    private final Map<String, JsonStorageMemoryIndex> indexMap = new TreeMap<>();

    public void addIndex(String fieldName) {
        if (!indexMap.containsKey(fieldName)) {
            indexMap.put(fieldName, new JsonStorageMemoryIndex());
        }

        // add index to existing data
        if (!jsonData.isEmpty()) {
            JsonStorageMemoryIndex idx = indexMap.get(fieldName);
            for (JSONObject jo : jsonData) {
                String v = JsonObjectHelper.getField(jo, fieldName);
                if (!v.isEmpty()) {
                    idx.addToIndex(v, jo);
                }
            }
        }
    }

    int size(QueryParameter qp) {
        if (null == qp || null == qp.getFilterList() || qp.getFilterList().isEmpty()) {
            return jsonData.size();
        }

        return createResultList(qp).size();
    }

    public List<String> select(QueryParameter qp) {
        List<JSONObject> l;

        if (null == qp || null == qp.getFilterList() || qp.getFilterList().isEmpty()) {
            l = jsonData;
        } else {
            l = createResultList(qp);
        }

        return l.stream().map(JSONObject::toString).collect(Collectors.toList());
    }

    boolean delete(QueryParameter qp) {
        if (null == qp || null == qp.getFilterList() || qp.getFilterList().isEmpty()) {
            jsonData.clear();
            indexMap.clear();
        } else {
            List<JSONObject> l = createResultList(qp);
            jsonData.removeAll(l);

            // most consuming operation, but ist the mostly less called operation
            for (Map.Entry<String, JsonStorageMemoryIndex> e : indexMap.entrySet()) {
                l.forEach(j -> e.getValue().removeFromIndex(j));
            }
        }

        return true;
    }

    void insert(JSONObject data) {
        jsonData.add(data);

        for (Map.Entry<String, JsonStorageMemoryIndex> e : indexMap.entrySet()) {
            String v = JsonObjectHelper.getField(data, e.getKey());
            if (!v.isEmpty()) {
                e.getValue().addToIndex(v, data);
            }
        }
    }

    private List<JSONObject> createResultList(QueryParameter qp) {
        List<JSONObject> r = new LinkedList<>();

        for (QueryFilterEntry f : qp.getFilterList()) {
            if ("AND".equalsIgnoreCase(f.operator())) {
                continue;
            }

            if (!"EQ".equalsIgnoreCase(f.operator())) {
                LOGGER.warn("Memory database supports EQ operator only is '{}'", f.toString());
                continue;
            }

            List<JSONObject> il;
            if (indexMap.keySet().contains(f.lhs())) {
                LOGGER.debug("use index for {}", f.toString());
                il = indexMap.get(f.lhs()).getListFor(f.rhs());
            } else {
                LOGGER.debug("full table scan for {}", f.toString());
                il = jsonData.stream().filter(jo -> matchJson(jo, f)).collect(Collectors.toList());
            }

            if (il != null && !il.isEmpty()) {
                r = r.isEmpty() ? il : andJsonList(r, il);
            }
        }

        return r;
    }

    // called from createResultList only and the preconditions are fulfilled.
    // which are EQ only and existing lhs
    private boolean matchJson(JSONObject jo, QueryFilterEntry filter) {
        return JsonObjectHelper.getField(jo, filter.lhs().replace(">", ".")).equals(filter.rhs());
    }

    private List<JSONObject> andJsonList(List<JSONObject> main, List<JSONObject> andList) {
        main.removeIf(jo -> !andList.contains(jo));
        return main;
    }

}
