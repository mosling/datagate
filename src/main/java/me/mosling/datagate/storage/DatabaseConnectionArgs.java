package me.mosling.datagate.storage;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Accessors(fluent = true)
public class DatabaseConnectionArgs {
    @JsonProperty("server")
    @Getter
    @Setter
    private String server = "";
    @JsonProperty("database")
    @Getter
    @Setter
    private String database = "";
    @JsonProperty("username")
    @Getter
    @Setter
    private String username = "";
    @JsonProperty("password")
    @Getter
    @Setter
    private String password = "";
    @JsonProperty("jdbcUrl")
    @Getter
    @Setter
    private String jdbcUrl = "";
}
