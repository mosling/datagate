package me.mosling.datagate.storage;

import me.mosling.datagate.core.Metadata;
import me.mosling.datagate.core.ResourceMetadata;
import me.mosling.datagate.parser.QueryParameter;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.net.URISyntaxException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.*;

public class JsonStoragePostgres
        implements JsonStorage {
    private static final Logger LOGGER = LogManager.getLogger(JsonStoragePostgres.class);

    public static final String ID_COLUMN_NAME = "id";
    public static final String DATA_COLUMN_NAME = "data";
    public static final String CREATED_COLUMN_NAME = "created";

    private PostgresqlConnection pgConnection;
    private String createTableColumnsSql = "";
    private final Map<String, String> createTableColumns = new TreeMap<>();

    public JsonStoragePostgres(String databaseUrl, String schema) {
        try {
            pgConnection = new PostgresqlConnection(databaseUrl);
            pgConnection.setSchema(schema);

            createTableColumns.put(ID_COLUMN_NAME, "serial primary key not null");
            createTableColumns.put(DATA_COLUMN_NAME, "jsonb");
            createTableColumns.put(CREATED_COLUMN_NAME,
                    "timestamp without time zone default (now() at time zone 'utc')");
            setCreateTableColumnsSql();
        } catch (URISyntaxException e) {
            LOGGER.error(e.getMessage());
            return;
        }

        LOGGER.info("configure '{}'{} .. ready.", databaseUrl, pgConnection.getSchema().isEmpty() ?
                " without schema" :
                " using schema '" + pgConnection.getSchema() + "'");
    }

    private void setCreateTableColumnsSql() {
        StringBuilder sb = new StringBuilder();

        sb.append("( ");
        boolean f = true;
        for (Map.Entry<String, String> e : createTableColumns.entrySet()) {
            sb.append(!f ? "," : "");
            f = false;
            sb.append(e.getKey());
            sb.append(" ");
            sb.append(e.getValue());
        }
        sb.append(" )");

        createTableColumnsSql = sb.toString();
    }

    public void addColumnForNewTables(String name, String type) {
        if (createTableColumns.containsKey(name)) {
            LOGGER.warn("overwrite existing column ({} {}) with new type ({})", name, createTableColumns.get(name),
                    type);
        }

        createTableColumns.put(name, type);
        setCreateTableColumnsSql();
    }

    private Boolean prepare(String storageName) {
        String methodName = "JsonStoragePostgres.prepare";
        String rtn = pgConnection.getTableName(storageName);
        String xt = String.format("SELECT to_regclass('%s') NOTNULL;", rtn);
        boolean success = false;

        try (Connection connection = pgConnection.getConnectionPool().getConnection();
             Statement s = connection.createStatement();
             ResultSet rs = s.executeQuery(xt)) {
            rs.next();
            success = rs.getBoolean(1);
        } catch (SQLException ex) {
            logSqlError(methodName, xt, ex);
        }

        if (success) {
            return Boolean.TRUE;
        }

        String ct = String.format("CREATE TABLE %s %s", rtn, createTableColumnsSql);
        String[] p = rtn.split("\\.");
        String cs = (2 == p.length) ? String.format("CREATE SCHEMA %s", p[0]) : "";

        try (Connection connection = pgConnection.getConnectionPool().getConnection();
             Statement s = connection.createStatement()) {
            LOGGER.info("create postgresql table '{}'", rtn);
            s.execute(ct);
        } catch (SQLException ex) {
            if ("3F000".equals(ex.getSQLState()) && !cs.isEmpty()) {
                try (Connection connection = pgConnection.getConnectionPool().getConnection();
                     Statement s = connection.createStatement()) {
                    LOGGER.info("create postgresql schema '{}'", pgConnection.getSchema());
                    s.execute(cs);
                    s.execute(ct);
                } catch (SQLException exx) {
                    logSqlError(methodName, String.format("create schema (%s), create table (%s)", cs, ct), exx);
                    return Boolean.FALSE;
                }
            } else {
                logSqlError(methodName, String.format("create table (%s)", ct), ex);
                return Boolean.FALSE;
            }
        }

        // if we reach this line the [schema].table was successfully created
        // index can be a json path part1.part2
        // if primaryKeyTypeHint is set the entry is used to cast the index (i.e. numeric)
        ResourceMetadata rmd = Metadata.getResourceForLocalStorageName(storageName);
        String pkName = null == rmd ? "" : rmd.getPrimaryKey();
        if (!pkName.isEmpty()) {
            String idxName = 2 == p.length ? p[1] : p[0];
            String indexCol = QueryPostgresSql.toColumn(rmd.getPrimaryKey().replace(".", ">"), DATA_COLUMN_NAME,
                    false);
            if (!rmd.getPrimaryKeyType().isEmpty()) {
                indexCol = String.format("cast (%s as %s)", indexCol, rmd.getPrimaryKeyType());
            } else {
                indexCol = "(" + indexCol + ")";
            }
            String idx = String.format("CREATE INDEX idx_btree_%s_%s ON %s USING BTREE (%s)", idxName,
                    pkName.replace(".", "_"), rtn, indexCol);

            try (Connection connection = pgConnection.getConnectionPool().getConnection();
                 Statement s = connection.createStatement()) {
                LOGGER.info(idx);
                s.execute(idx);
            } catch (SQLException ex) {
                logSqlError(methodName, String.format("create btree index on %s (%s)", rtn, idxName), ex);
            }
        }

        return Boolean.TRUE;
    }

    @Override
    public Boolean delete(String storageName, QueryParameter qp, boolean dropIfSupported) {
        if (null == qp && dropIfSupported) {
            return drop(storageName);
        }

        String wc = QueryPostgresSql.createWhereClause(qp);
        String tn = pgConnection.getTableName(storageName);

        LOGGER.info("DELETE resource '{}'{}", tn, wc.isEmpty() ? "" : " entries matching '" + wc + "'");

        return executeSqlStatement(storageName, String.format("DELETE FROM %s%s", tn, wc));
    }

    public Boolean drop(String storageName) {
        String tn = pgConnection.getTableName(storageName);
        LOGGER.info("DROP resource data for '{}'", tn);

        return executeSqlStatement(storageName, String.format("DROP TABLE %s", tn));
    }

    @Override
    public void closeStorage() {
        pgConnection.closeConnection();
    }

    @Override
    public Boolean isConnected() {
        String sql = "select 1";

        try (Connection connection = pgConnection.getConnectionPool().getConnection();
             Statement s = connection.createStatement();
             ResultSet rs = s.executeQuery(sql)) {
            return true;
        } catch (SQLException ex) {
            logSqlError("JsonStoragePostgres.isConnected?", sql, ex);
        }

        return false;
    }

    /**
     * @param storageName name of the table
     * @param qp          an optional where clause without the "WHERE" string
     *
     * @return -1 if table not exists otherwise the number of entries starting at zero
     */
    @Override
    public Integer count(String storageName, QueryParameter qp) {
        Integer cols = -1;

        String sql = String.format("SELECT COUNT(*) FROM %s%s", pgConnection.getTableName(storageName),
                QueryPostgresSql.createWhereClause(qp));

        if (prepare(storageName)) {
            try (Connection connection = pgConnection.getConnectionPool().getConnection();
                 Statement s = connection.createStatement();
                 ResultSet rs = s.executeQuery(sql)) {
                rs.next();
                cols = rs.getInt(1);
                LOGGER.debug("found {} object(s) stored at '{}'", cols, storageName);
            } catch (SQLException ex) {
                logSqlError("JsonStoragePostgres.countEntries (i.e. count(*) )", sql, ex);
            }
        }

        return cols;
    }

    @Override
    public List<String> select(String storageName, QueryParameter qp) {
        if (qp.getSelectList().isEmpty()) {
            qp.getSelectList().add(DATA_COLUMN_NAME);
            qp.getSelectList().add("_" + CREATED_COLUMN_NAME);
        }

        String sql = QueryPostgresSql.createSelectStatementForTable(qp, pgConnection.getTableName(storageName));

        return executeSqlSelectStatement(storageName, sql);
    }

    @Override
    public Boolean insert(String storageName, String jsonData) {
        String b = jsonData.replace("'", "''");
        String sql = String.format("insert into %s (data) values ('%s')", pgConnection.getTableName(storageName),
                b);

        return executeSqlStatement(storageName, sql);
    }

    @Override
    public Boolean update(String storageName, String jsonData, QueryParameter qp) {
        String sql;

        if (!jsonData.isEmpty()) {
            sql = String.format("update %s set data = '%s'%s", pgConnection.getTableName(storageName),
                    jsonData.replace("'", "''"), QueryPostgresSql.createWhereClause(qp));

            return executeSqlStatement(storageName, sql);
        }

        return true;
    }

    @Override
    public String updateStorageNameWithFixture(String storageName, String fixtureName) {
        String f = fixtureName.replace("-", "_");
        return f.isEmpty() ? storageName : f + "." + storageName;
    }

    public List<String> executeSqlSelectStatement(String resource, String sql) {
        if (prepare(resource)) {
            List<String> rowList = new ArrayList<>();
            try (Connection connection = pgConnection.getConnectionPool().getConnection();
                 Statement s = connection.createStatement();
                 ResultSet rs = s.executeQuery(sql)) {
                LOGGER.trace("SQL: {}", sql);
                while (rs.next()) {
                    rowList.add(rs.getString(1));
                }
                return rowList;
            } catch (SQLException e) {
                logSqlError("execute sqlSelect(" + resource + ") statement '{}'", sql, e);
            }
        }

        return Collections.emptyList();
    }

    public Boolean executeSqlStatement(String resource, String sql) {
        if (prepare(resource)) {
            try (Connection connection = pgConnection.getConnectionPool().getConnection();
                 Statement s = connection.createStatement()) {
                LOGGER.trace("SQL: {}", sql);
                s.execute(sql);
            } catch (SQLException e) {
                logSqlError("execute sql(" + resource + ") statement '{}'", sql, e);
                return false;
            }
        }
        return true;
    }

    private void logSqlError(String methodName, String sqlCommand, SQLException sqlException) {
        LOGGER.error("SQL: /{}/", sqlCommand);
        LOGGER.error("method {}: code {} --> {}", methodName, sqlException.getSQLState(), sqlException);
    }
}
