package me.mosling.datagate.storage;

import lombok.Getter;
import org.json.JSONObject;

import java.util.*;

// An Index holds a list of references to the stored data with this value.
public class JsonStorageMemoryIndex {
    @Getter
    private final Map<String, List<JSONObject>> indexMap = new TreeMap<>();

    void addToIndex(String v, JSONObject o) {
        if (!indexMap.containsKey(v)) {
            indexMap.put(v, new LinkedList<>());
        }

        indexMap.get(v).add(o);
    }

    void removeFromIndex(String v, JSONObject o) {
        if (indexMap.containsKey(v)) {
            indexMap.get(v).remove(o);
        }
    }

    void removeFromIndex(JSONObject o) {
        for (Map.Entry<String, List<JSONObject>> e : indexMap.entrySet()) {
            e.getValue().remove(o);
        }
    }

    List<JSONObject> getListFor(String v) {
        if (indexMap.containsKey(v)) {
            return indexMap.get(v);
        }

        return Collections.emptyList();
    }

}
