package me.mosling.datagate.storage;

import me.mosling.datagate.common.Pair;
import me.mosling.datagate.core.Metadata;
import me.mosling.datagate.parser.QueryFilterEntry;
import me.mosling.datagate.parser.QueryParameter;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.*;
import java.util.regex.Pattern;

public class QueryPostgresSql {
    private static final Logger LOGGER = LogManager.getLogger(QueryPostgresSql.class);

    private static final Map<String, Pair<String, String>> sqlOperatorMapping = new TreeMap<>();
    private static final List<String> jsonOperators = new ArrayList<>();
    private static final Pattern INTEGER_PATTERN = Pattern.compile("^0|[1-9][0-9]*$");

    static {
        sqlOperatorMapping.put("EQ", new Pair<>("=", "<>"));
        sqlOperatorMapping.put("GT", new Pair<>(">", "<="));
        sqlOperatorMapping.put("GTE", new Pair<>(">=", "<"));
        sqlOperatorMapping.put("LT", new Pair<>("<", ">="));
        sqlOperatorMapping.put("LTE", new Pair<>("<=", ">"));
        sqlOperatorMapping.put("IS", new Pair<>("is", "is not"));
        sqlOperatorMapping.put("HAS", new Pair<>("?", ""));
        sqlOperatorMapping.put("ANY", new Pair<>("?|", ""));
        sqlOperatorMapping.put("ALL", new Pair<>("?&", ""));
        sqlOperatorMapping.put("IN", new Pair<>("in", "not in"));
        sqlOperatorMapping.put("LIKE", new Pair<>("like", "not like"));
        sqlOperatorMapping.put("ILIKE", new Pair<>("ilike", "not ilike"));
        sqlOperatorMapping.put("MATCH", new Pair<>("similar to", "not similar to"));
        sqlOperatorMapping.put("BTW", new Pair<>("between", "not between"));
        sqlOperatorMapping.put("BETWEEN", new Pair<>("between", "not between"));

        jsonOperators.add("HAS");
        jsonOperators.add("ANY");
        jsonOperators.add("ALL");
    }

    private QueryPostgresSql() {
        // functional class
    }

    public static String createSelection(QueryParameter qp) {
        if (qp.getSelectList().isEmpty()) {
            return JsonStoragePostgres.DATA_COLUMN_NAME;
        }

        // otherwise create a new json object

        boolean withData = false;
        boolean withDistinct = false;
        boolean firstTry = true;
        List<String> sl = new ArrayList<>();

        for (String c : qp.getSelectList()) {
            if (firstTry && "distinct".equalsIgnoreCase(c)) {
                withDistinct = true;
            } else if (c.equalsIgnoreCase(JsonStoragePostgres.DATA_COLUMN_NAME)) {
                withData = true;
            } else {
                String cn = toColumn(c, JsonStoragePostgres.DATA_COLUMN_NAME, true);
                // adding a 'normal' column
                sl.add(String.format("'%s', %s", c.replace(">", "_"), cn));
            }

            firstTry = false;
        }

        StringBuilder sb = new StringBuilder();
        if (withDistinct) {
            sb.append("distinct ");
        }

        if (withData) {
            sb.append(JsonStoragePostgres.DATA_COLUMN_NAME);
        }

        if (!sl.isEmpty()) {
            if (withData) {
                sb.append(" || ");
            }
            sb.append("jsonb_build_object(");
            sb.append(String.join(",", sl));
            sb.append(")");
        }

        return sb.toString();
    }

    public static String createOrderBy(QueryParameter qp) {
        if (null == qp || qp.getOrderList().isEmpty()) {
            return "";
        }

        StringBuilder sb = new StringBuilder();
        boolean addComma = false;
        for (String op : qp.getOrderList()) {
            if ("asc".equalsIgnoreCase(op) || "desc".equalsIgnoreCase(op)) {
                sb.append(" ");
                sb.append(op);
            } else {
                if (addComma)
                    sb.append(", ");
                sb.append(toColumn(op, JsonStoragePostgres.DATA_COLUMN_NAME, false));
            }
            addComma = true;
        }

        return sb.length() > 0 ? " ORDER BY " + sb.toString() : "";
    }

    @SuppressWarnings("squid:S3776")
    public static String createWhereClause(QueryParameter qp) {
        if (null == qp || qp.getFilterList().isEmpty()) {
            return "";
        }

        StringBuilder sb = new StringBuilder();

        for (QueryFilterEntry wc : qp.getFilterList()) {
            if (wc.isOperator()) {
                sb.append(surround(wc.operator()));
                continue;
            }

            String op = wc.operator().toUpperCase();
            if (!sqlOperatorMapping.containsKey(op)) {
                LOGGER.error("unsupported operator '{}'", op);
                continue;
            }

            String lhs = toColumn(wc.lhs(), JsonStoragePostgres.DATA_COLUMN_NAME,
                    jsonOperators.contains(op));
            Pair<String, String> pair = sqlOperatorMapping.get(op);
            boolean withNot = wc.negate();

            if (withNot && pair.getValue().isEmpty()) {
                sb.append(surround("not"));
                withNot = false;
            }
            sb.append(toCast(lhs, wc.stringInfo(), wc.rhs()));
            sb.append(surround(withNot ? pair.getValue() : pair.getKey()));

            if ("BTW".equalsIgnoreCase(op) || "BETWEEN".equalsIgnoreCase(op)) {
                sb.append(toValue(wc.rhs(), wc.stringInfo()));
                sb.append(surround("and"));
                sb.append(toValue(wc.rhs2(), wc.stringInfo()));
            } else if ("ANY".equalsIgnoreCase(op) || "ALL".equalsIgnoreCase(op)) {
                sb.append(surround(toList(wc.rhs(), wc.stringInfo()), "array[", "]"));
            } else if ("IN".equalsIgnoreCase(op)) {
                sb.append(surround(toList(wc.rhs(), wc.stringInfo()), "(", ")"));
            } else if ("IS".equalsIgnoreCase(op)) {
                sb.append(wc.rhs());
            } else {
                String v = toValue(wc.rhs(), wc.stringInfo());
                if ("LIKE".equalsIgnoreCase(op) || "ILIKE".equalsIgnoreCase(op)) {
                    v = v.replaceAll("\\*", "%");
                }
                sb.append(v);
            }
        }

        return sb.length() > 0 ? " WHERE " + sb.toString() : "";
    }

    public static String surround(String str) {
        return " " + str.trim() + " ";
    }

    public static String surround(String str, String sb, String se) {
        return sb + str.trim() + se;
    }

    public static String toList(String value, Boolean strHint) {
        List<String> l = Arrays.asList(value.split(","));
        l.replaceAll(e -> toValue(e, strHint));

        return String.join(",", l);
    }

    public static String toValue(String value, Boolean strHint) {
        boolean retString = false;

        // return quoted string without changes
        if (value.startsWith("'")) {
            return value;
        }

        // check if we have a string
        if (null != strHint && strHint) {
            retString = true;
        }
        // otherwise detect column name
        else if (value.startsWith("_") || value.contains(">")) {
            String col = toColumn(value, JsonStoragePostgres.DATA_COLUMN_NAME, false);
            return toCast(col, strHint, null);
        }
        // test for a number
        else {
            // otherwise check if it is a number
            try {
                Double.parseDouble(value);
            } catch (NumberFormatException ex) {
                retString = true;
            }
        }

        return retString ? "'" + value.replaceAll("'", "''") + "'" : value;
    }

    public static String toCast(String lhs, Boolean strHint, String exampleValue) {
        if (exampleValue == null) {
            if (strHint != null && !strHint) {
                return surround(lhs, "(", ")::numeric");
            }

            return lhs;
        }

        String cast = "";

        if ("true".equalsIgnoreCase(exampleValue) || "false".equalsIgnoreCase(exampleValue)) {
            cast = "boolean";
        } else {
            try {
                Double.parseDouble(exampleValue);
                cast = "numeric";
            } catch (NumberFormatException ex) {
                // no action needed
            }
        }

        return cast.isEmpty() ? lhs : surround(lhs, "(", ")::" + cast);
    }

    public static String toColumn(String name, String jsonColumn, boolean asJson) {
        boolean nativeColumn = name.startsWith("_");
        String tmpName = nativeColumn ? name.substring(1) : name;

        List<String> p = new ArrayList<>(Arrays.asList(tmpName.split(">")));

        if (!nativeColumn && !jsonColumn.equalsIgnoreCase(p.get(0))) {
            p.add(0, jsonColumn);
        }

        if (p.size() == 1) {
            return nativeColumn ? surround(p.get(0), "\"", "\"") : p.get(0);
        }

        // now create the column string for the json element
        // 0: the database column name
        // 1..n-1: the json path
        // n: the element (asJson ? ->> : ->)

        StringBuilder sb = new StringBuilder();
        sb.append(p.get(0));
        int ll = p.size() - 1;
        for (int i = 1; i < ll; ++i) {
            sb.append(" -> ");
            sb.append("'");
            sb.append(p.get(i));
            sb.append("'");
        }

        // if the last part is a number, this will be interpreted as index of an array
        String attr = p.get(ll);
        String t = INTEGER_PATTERN.matcher(attr).matches() ? "" : "'";

        sb.append(String.format(" %s %s", asJson ? "->" : "->>", surround(attr, t, t)));

        return sb.toString();
    }

    public static String createSelectStatementForTable(QueryParameter qp, String tableName) {
        String sql = String.format("SELECT %s FROM %s%s%s", createSelection(qp), tableName, createWhereClause(qp),
                createOrderBy(qp));

        if (qp.getLimit() > 0) {
            sql += String.format(" LIMIT %d", qp.getLimit());
        }

        if (Metadata.isDevelopmentMode() && LOGGER.isDebugEnabled()) {
            LOGGER.debug("DEVELOPMENT-SQL: {}", sql);
        }

        return sql;
    }

}
