package me.mosling.datagate.storage;

import me.mosling.datagate.core.Metadata;
import me.mosling.datagate.core.ResourceMetadata;
import me.mosling.datagate.parser.QueryParameter;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.TreeMap;

public class JsonStorageMemory
        implements JsonStorage {
    private static final Logger LOGGER = LogManager.getLogger(JsonStorageMemory.class);

    private final Map<String, JsonStorageMemoryTable> jsonDb = new TreeMap<>();

    public JsonStorageMemory() {
        LOGGER.info("use memory database .. ready");
    }

    private void prepare(String storageName) {
        if (!jsonDb.containsKey(storageName)) {
            JsonStorageMemoryTable t = new JsonStorageMemoryTable();
            String pk = Optional
                    .ofNullable(Metadata.getResourceForLocalStorageName(storageName))
                    .map(ResourceMetadata::getPrimaryKey)
                    .orElse("");

            if (!pk.isEmpty()) {
                t.addIndex(pk);
            }
            jsonDb.put(storageName, t);
        }
    }

    @Override
    public void closeStorage() {
        jsonDb.clear();
    }

    @Override
    public Boolean isConnected() {
        return Boolean.TRUE;
    }

    @Override
    public Integer count(String storageName, QueryParameter qp) {
        prepare(storageName);
        return jsonDb.get(storageName).size(qp);
    }

    @Override
    public Boolean delete(String storageName, QueryParameter qp, boolean dropIfSupported) {
        prepare(storageName);
        return jsonDb.get(storageName).delete(qp);
    }

    @Override
    public List<String> select(String storageName, QueryParameter qp) {
        prepare(storageName);
        return jsonDb.get(storageName).select(qp);
    }

    @Override
    public Boolean insert(String storageName, String data) {
        prepare(storageName);
        jsonDb.get(storageName).insert(new JSONObject(data));

        return true;
    }

    @Override
    public Boolean update(String storageName, String data, QueryParameter qp) {
        delete(storageName, qp, false);
        return insert(storageName, data);
    }

    @Override
    public String updateStorageNameWithFixture(String storageName, String fixtureName) {
        return fixtureName + "." + storageName;
    }

}
