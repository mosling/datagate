package me.mosling.datagate.storage;

import me.mosling.datagate.parser.QueryParameter;

import java.util.List;

public interface JsonStorage {
    void closeStorage();

    Boolean isConnected();

    Integer count(String storageName, QueryParameter qp);

    Boolean delete(String storageName, QueryParameter qp, boolean dropIfSupported);

    List<String> select(String storageName, QueryParameter qp);

    Boolean insert(String storageName, String data);

    Boolean update(String storageName, String data, QueryParameter qp);

    String updateStorageNameWithFixture(String storageName, String fixtureName);
}
