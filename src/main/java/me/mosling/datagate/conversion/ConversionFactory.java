package me.mosling.datagate.conversion;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class ConversionFactory {
    private static final Logger LOGGER = LogManager.getLogger(ConversionFactory.class);

    public static List<String> checkAndGetVariables(String vars, JSONObject jo) {
        List<String> vl = Arrays.asList(vars.split(","));
        List<String> l = new ArrayList<>();
        boolean f = false;

        for (String v : vl) {
            boolean isArray = false;
            if (v.startsWith("[")) {
                isArray = true;
                v = v.substring(1);
            }
            if (jo.has(v)) {
                Object o = jo.get(v);
                if (isArray && o instanceof JSONArray) {
                    l.add(((JSONArray) o).join(","));
                } else if (!isArray) {
                    l.add(o.toString());
                } else {
                    LOGGER.error("parameter {}='{}' isn't an array", v, o.toString());
                    f = true;
                }
            } else {
                LOGGER.error("missing parameter {}{}", vl, isArray ? " as array" : "");
                f = true;
            }
        }
        return f ? Collections.emptyList() : l;
    }
}
