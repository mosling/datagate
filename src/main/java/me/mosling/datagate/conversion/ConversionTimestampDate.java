package me.mosling.datagate.conversion;

import me.mosling.httpfixture.common.StringHelper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

public class ConversionTimestampDate
        implements Conversion {
    private static final Logger LOGGER = LogManager.getLogger(ConversionTimestampDate.class);

    private final List<String> dateElementList;

    public ConversionTimestampDate(String dateElements) {
        dateElementList = new ArrayList<>(Arrays.asList(dateElements.split(",")));
    }

    @Override
    public String getParameter() {
        return "date-elements[]";
    }

    @Override
    public JSONObject map(JSONObject apiJsonObject) {
        boolean converted = false;
        for (String de : dateElementList) {
            try {
                converted = convertField(apiJsonObject, de) || converted;

            } catch (JSONException ex) {
                LOGGER.error("field '{}' can't converted --> {}", de, ex.getMessage());
            }
        }

        return apiJsonObject;
    }

    @Override
    public JSONObject unmap(JSONObject internalJsonObject) {
        return internalJsonObject;
    }

    private static boolean convertField(JSONObject document, String key) {
        if (key.contains(".")) {
            int indexOfDot = key.indexOf('.');
            String subKey = key.substring(0, indexOfDot);
            JSONObject jsonObject = (JSONObject) document.get(subKey);
            if (jsonObject == null) {
                throw new JSONException(subKey + " is null");
            }
            try {
                return convertField(jsonObject, key.substring(indexOfDot + 1));
            } catch (JSONException e) {
                throw new JSONException(subKey + "." + e.getMessage());
            }
        } else if (document.has(key) && !document.isNull(key)) {
            long dt = document.getLong(key);
            // unix timestamp 11/20/2286 @ 5:46pm (UTC) otherwise we suggest a milliseconds timestamp
            if (dt < 9999999999L) {
                dt = 1000 * dt;
            }
            document.put(key, StringHelper.getUtcString(new Date(dt)));
            return true;
        }
        return false;
    }

    @Override
    public String toString() {
        return "ConversionTimestampDate{" + "dateElementList=" + dateElementList + '}';
    }
}
