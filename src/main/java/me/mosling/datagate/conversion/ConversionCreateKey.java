package me.mosling.datagate.conversion;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ConversionCreateKey
        implements Conversion {
    private final List<String> keyElementList;
    private final String keyFieldName;
    private final String separator;
    private final boolean removeCreatedKey;

    public ConversionCreateKey(String keyFieldName, String keyElements, String separator) {
        this.keyFieldName = keyFieldName;
        this.separator = separator;
        keyElementList = new ArrayList<>(Arrays.asList(keyElements.split(",")));
        removeCreatedKey = !keyElementList.contains(keyFieldName);
    }

    @Override
    public String getParameter() {
        return "keyFieldName,keyElements[],separator";
    }

    @Override
    public JSONObject map(JSONObject apiJsonObject) {
        StringBuilder newKey = new StringBuilder();

        for (String ke : keyElementList) {
            if (apiJsonObject.has(ke)) {
                newKey.append(newKey.length() == 0 ? "" : separator);
                newKey.append(apiJsonObject.get(ke).toString());
            }
        }

        apiJsonObject.put(keyFieldName, newKey.toString());

        return apiJsonObject;
    }

    @Override
    public JSONObject unmap(JSONObject internalJsonObject) {

        if (removeCreatedKey && internalJsonObject.has(keyFieldName)) {
            internalJsonObject.remove(keyFieldName);
        }

        return internalJsonObject;
    }

    @Override
    public String toString() {
        return "ConversionCreateKey{" + "keyElementList=" + keyElementList + ", keyFieldName='" + keyFieldName + '\''
                + ", separator='" + separator + '\'' + ", removeCreatedKey=" + removeCreatedKey + '}';
    }
}
