package me.mosling.datagate.conversion;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class ConversionRemoveFields
        implements Conversion {
    private final String notImported;
    private final String notExported;

    private final List<String> notImportedList;
    private final List<String> notExportedList;

    public ConversionRemoveFields(String notImported, String notExported) {
        this.notImported = notImported;
        this.notExported = notExported;

        notImportedList = (null == notImported || notImported.isEmpty()) ?
                Collections.emptyList() :
                new ArrayList<>(Arrays.asList(notImported.split(",")));

        notExportedList = (null == notExported || notExported.isEmpty()) ?
                Collections.emptyList() :
                new ArrayList<>(Arrays.asList(notExported.split(",")));
    }

    @Override
    public String getParameter() {
        return "fields-not-imported[],fields-not-exported[]";
    }

    @Override
    public JSONObject map(JSONObject apiJsonObject) {
        for (String e : notImportedList) {
            apiJsonObject.remove(e);
        }

        return apiJsonObject;
    }

    @Override
    public JSONObject unmap(JSONObject internalJsonObject) {
        for (String e : notExportedList) {
            internalJsonObject.remove(e);
        }

        return internalJsonObject;
    }

    @Override
    public String toString() {
        return String.format("(field removal notImported[%s], notExported[%s])", notImported, notExported);
    }
}
