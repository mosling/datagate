package me.mosling.datagate.conversion;

import org.json.JSONObject;

import java.util.Collections;
import java.util.List;
import java.util.ListIterator;

public class ConversionChain
        implements Conversion {
    private final List<Conversion> conversionChainList;

    public ConversionChain(List<Conversion> conversionChainList) {
        this.conversionChainList = null == conversionChainList ? Collections.emptyList() : conversionChainList;
    }

    @Override
    public String getParameter() {
        return "";
    }

    @Override
    public JSONObject map(JSONObject apiJsonObject) {
        if (conversionChainList != null) {
            for (Conversion c : conversionChainList) {
                apiJsonObject = c.map(apiJsonObject);
            }
        }

        return apiJsonObject;
    }

    @Override
    public JSONObject unmap(JSONObject internalJsonObject) {
        if (conversionChainList != null) {
            ListIterator<Conversion> it = conversionChainList.listIterator(conversionChainList.size());

            while (it.hasPrevious()) {
                internalJsonObject = it.previous().unmap(internalJsonObject);
            }
        }

        return internalJsonObject;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("chain: ");

        boolean first = true;
        for (Conversion c : conversionChainList) {
            sb.append(first ? "" : " --> ");
            sb.append(c.toString());
            first = false;
        }

        return sb.toString();
    }
}
