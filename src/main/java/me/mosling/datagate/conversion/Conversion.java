package me.mosling.datagate.conversion;

import org.json.JSONObject;

public interface Conversion {
    // return a comma separated list of parameters, all strings,
    // comma separated arrays starts wit [
    String getParameter();

    // map the API result to the internal String
    JSONObject map(JSONObject apiJsonObject);

    // unmap the internal representation to the API
    JSONObject unmap(JSONObject internalJsonObject);
}
