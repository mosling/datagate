package me.mosling.datagate.conversion;

import me.mosling.httpfixture.common.StringHelper;

import java.util.*;

public class ConversionDictionary {
    public static final String NODE_DATA = "DATA";
    private static final Map<String, Map<String, Conversion>> transformationTuple = new HashMap<>();

    private ConversionDictionary() {
        // functional class
    }

    public static void add(String dataNode, String connectionNode, Conversion conversion) {
        Map<String, Conversion> tm;

        if (transformationTuple.containsKey(dataNode)) {
            tm = transformationTuple.get(dataNode);
        } else {
            tm = new TreeMap<>();
            transformationTuple.put(dataNode, tm);
        }

        tm.put(connectionNode, conversion);
    }

    public static Conversion find(String resourceNode, String connectionNode) {
        if (StringHelper.nonNullStr(resourceNode).isEmpty() || StringHelper.nonNullStr(connectionNode).isEmpty()) {
            return null;
        }

        return transformationTuple.containsKey(resourceNode) ?
                transformationTuple.get(resourceNode).getOrDefault(connectionNode, null) :
                null;
    }

    public static Map<String, Conversion> resourceEntries(String resourceNode) {
        return transformationTuple.getOrDefault(resourceNode, Collections.emptyMap());
    }

    public static int size() {
        return transformationTuple.size();
    }

    public static List<String> toStringList() {
        List<String> rl = new ArrayList<>();

        for (Map.Entry<String, Map<String, Conversion>> e : transformationTuple.entrySet()) {
            rl.add(e.getKey());
            for (Map.Entry<String, Conversion> ee : e.getValue().entrySet()) {
                rl.add(String.format("    %s -> %s", ee.getKey(), ee.getValue().toString()));
            }
        }

        return rl;
    }
}
