package me.mosling.datagate.conversion;

import lombok.Getter;
import lombok.Setter;
import me.mosling.httpfixture.security.PasswordHelper;
import org.json.JSONObject;

public class ConversionPbkdf2
        implements Conversion {
    @Getter
    @Setter
    private String secretFieldName;
    @Getter
    @Setter
    private String hashFieldName;

    public ConversionPbkdf2(String secretFieldName, String hashFieldName) {
        this.secretFieldName = secretFieldName;
        this.hashFieldName = hashFieldName;
    }

    @Override
    public String getParameter() {
        return "secretFieldName,hashFieldName";
    }

    @Override
    public JSONObject map(JSONObject apiJsonObject) {
        if (apiJsonObject.has(secretFieldName)) {
            String pwd = apiJsonObject.getString(secretFieldName);
            apiJsonObject.remove(secretFieldName);
            apiJsonObject.put(hashFieldName, PasswordHelper.generatePbkdf2PasswordHash(pwd));
        }

        return apiJsonObject;
    }

    @Override
    public JSONObject unmap(JSONObject internalJsonObject) {
        if (internalJsonObject.has(hashFieldName)) {
            internalJsonObject.remove(hashFieldName);
        }
        return internalJsonObject;
    }

    @Override
    public String toString() {
        return "ConversionPbkdf2{" + "secretFieldName='" + secretFieldName + '\'' + ", hashFieldName='" + hashFieldName
                + '\'' + '}';
    }
}
