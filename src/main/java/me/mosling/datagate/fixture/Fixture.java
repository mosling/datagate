package me.mosling.datagate.fixture;

import me.mosling.httpfixture.fixture.ResponseData;

public interface Fixture {
    String getName();

    ResponseData doGetRequest(String resource, String address);

    ResponseData doWriteRequest(String resource, String method, String address, String data);

    boolean isConnected();

    static String correctUrlAddress(String address, String url) {
        if (address.startsWith(url)) {
            return address.substring(url.length() + 1);
        }

        return address;
    }
}
