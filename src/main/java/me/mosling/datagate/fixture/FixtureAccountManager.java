package me.mosling.datagate.fixture;

import lombok.Getter;
import lombok.Setter;
import me.mosling.datagate.core.Metadata;
import me.mosling.httpfixture.common.StringHelper;
import me.mosling.httpfixture.fixture.*;
import me.mosling.httpfixture.security.JksManager;
import me.mosling.httpfixture.security.JksManagerArgs;
import me.mosling.httpfixture.security.OAuthHelper;
import me.mosling.httpfixture.security.OAuthToken;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.List;

public class FixtureAccountManager
        extends BaseHttpFixture
        implements Fixture {
    private static final Logger LOGGER = LogManager.getLogger(FixtureAccountManager.class);

    private static final String USER = "userAuthKey";
    private static final String BEARER = "bearerAuthKey";

    @Getter
    private OAuthToken lastToken;
    @Getter
    private final String fixtureName;
    @Getter
    private boolean connected = false;
    @Getter
    @Setter
    List<String> requestedScopes = new ArrayList<>();

    /**
     * The Http fixture uses OAuth2 grant_type=password to login a client using an existing client with
     * password (the serverkey stored in authorization section) and server (the fixtureName stored in connection
     * section) with the given user/secret at startup.
     * It is a little bit hard to configure the fixture at the startup, because there a lot of possible
     * parameters which configure a http connection with trust- and keystore. The solution is to setup all things
     * into the well known configuration file for QA-Adapter which setup connections, authorizations and addresses.
     * And we use the same key to access the connection and the authorization which is used to connect the data server.
     *
     * @param configFile      the name of the config file can be a resource or an existing file
     * @param fixtureName     the key for connection and authorization to connect the server
     * @param userLogin       the login to login a user
     * @param userSecret      the secret for the user login
     * @param requestedScopes list of scopes, this is used by OpenId-Connect to define the scopes which are included
     *                        in the response
     */
    public FixtureAccountManager(String configFile, String fixtureName, String userLogin, String userSecret,
                                 List<String> requestedScopes) {
        this.requestedScopes.addAll(requestedScopes);

        build(httpArgs(configFile), jksManager());
        this.fixtureName = fixtureName;

        if (fixtureName.isEmpty()) {
            LOGGER.error("     no server key Id (-Dfixture=) given for {} with config file {}",
                    this.getClass().getName(), configFile);
            connected = true;
        } else if (null == getClient().httpClientConfig().getConnections().get(fixtureName)) {
            LOGGER.error("     no entry find in '{}' for '{}' possible values ({})", configFile, fixtureName,
                    String.join(",", getClient().httpClientConfig().getConnections().keySet()));
            connected = false;
        } else {
            if (!userLogin.isEmpty() && !userSecret.isEmpty()) {
                getClient().putAuth(USER, EnumAuthType.USER, userLogin, userSecret, true);
            } else {
                LOGGER.warn(
                        "     please add user (-Duser=) and secret (-Dsecret=) property to login as user --> assume "
                                + "client login only");
            }

            login();

            LOGGER.info("http client for {} .. ready", fixtureName.isEmpty() ?
                    "<no server given>" :
                    getClient().httpClientConfig().getConnections().get(fixtureName).getConnectionUrl());
        }
    }

    private HttpClientArgs httpArgs(String configFile) {
        return new HttpClientArgs().requestTimeoutMs(80000).configFile(configFile).enableRedirect(true);
    }

    private JksManager jksManager() {
        JksManagerArgs ja = new JksManagerArgs();
        ja
                .truststore(StringHelper.findProperty("truststore", "certs/truststore.jks"))
                .truststoreSecret(StringHelper.findProperty("truststoreSecret", "4demandware"))
                .allowFilesystemLookup(true)
                .jdkIncludeCertificates();

        return new JksManager(ja);
    }

    @Override
    public String getName() {
        return fixtureName;
    }

    @Override
    public ResponseData doGetRequest(String resource, String address) {
        return internalGetRequest(address, 1);
    }

    private ResponseData internalGetRequest(String address, Integer counter) {
        String url = getClient().getConnectionUrl(fixtureName);
        String addr = Fixture.correctUrlAddress(address, url);

        connected = true; // think positive, important to return to connected after network error
        ResponseData rd = executeHttp("GET", fixtureName, BEARER, addr, "", -1);

        if (200 == rd.getStatus()) {
            return rd;
        } else {
            LOGGER.info(" ({}) last execution '{}' responds status {}{}", counter, addr, rd.getStatus(),
                    counter > 0 ? "" : " --> give up");
        }

        if (0 == rd.getStatus()) {
            connected = false;
            counter = 0;
        }

        if (counter > 0) {
            if (502 == rd.getStatus()) {
                LOGGER.info("   --> retry");
                rd = internalGetRequest(address, --counter);
            }

            if ((401 == rd.getStatus()) && login()) {
                LOGGER.info("   --> retry with refreshed token");
                rd = internalGetRequest(address, --counter);
            }
        }

        return rd;
    }

    @Override
    public ResponseData doWriteRequest(String resource, String method, String address, String data) {
        return internalWriteRequest(method, address, data, 1);
    }

    private ResponseData internalWriteRequest(String method, String address, String data, Integer counter) {
        String url = getClient().getConnectionUrl(fixtureName);
        String addr = Fixture.correctUrlAddress(address, url);


        HttpRequest req = HttpClientFactory.defaultRequest(method, !data.isEmpty());
        if (!data.isEmpty()) {
            req.setTextBody(data);
        }
        connected = true; // think positive, important to return to connected after network error
        ResponseData rd = executeRequest(req, fixtureName, BEARER, addr, -1);

        if (0 == rd.getStatus()) {
            connected = false;
        }

        if ((401 == rd.getStatus()) && (counter > 0) && login()) {
            rd = internalWriteRequest(method, addr, data, --counter);
        }

        return rd;
    }

    public boolean login(String userLogin, String userSecret) {
        getClient().putAuth(USER, EnumAuthType.USER, userLogin, userSecret, true);
        return login();
    }

    /**
     * This methods get the OAuth token for the using the client credentials from
     * the configuration file and optional a given user/secret pair.
     *
     * @return true if login was successfully
     */
    private boolean login() {
        HttpAuth u = getClient().httpClientConfig().getAuths().get(USER);
        HttpAuth c = getClient().httpClientConfig().getAuths().get(fixtureName);

        // remove BEARER because the existing of this entry decides if the login was successfully
        getClient().httpClientConfig().getAuths().remove(BEARER);

        if (null != lastToken && !lastToken.getRefreshToken().isEmpty()) {
            LOGGER.info("refresh token for: '{}' ...", lastToken.getAccessToken());
            lastToken = refreshUserToken(lastToken);
        } else if (null != u) {
            LOGGER.info("login user: '{}' ...", u.getAuthName());
            lastToken = loginClientUser(fixtureName, fixtureName, USER, String.join(" ", requestedScopes), BEARER);
        } else if (null != c) {
            LOGGER.info("login client: '{}' ...", c.getAuthName());
            lastToken = loginClient(fixtureName, fixtureName, BEARER);
        }

        connected = true;
        if (!getClient().httpClientConfig().getAuths().containsKey(BEARER)) {
            String up = u != null && u.getAuthPassword().isEmpty() ? "<no password given>" : "<with password>";
            String cp = c != null && c.getAuthPassword().isEmpty() ? "<no password given>" : "<with password>";

            LOGGER.fatal(Metadata.getHeader("CAN'T LOGIN TO SERVER"));
            LOGGER.fatal("can't login (client:{}; user:{}) at host {}",
                    (null != c ? c.getAuthName() + ":" + cp : "<null>"),
                    (null != u ? u.getAuthName() + ":" + up : "<null>"),
                    getClient().getConnectionUrl(fixtureName));

            connected = false;
        }

        return connected;
    }

    public OAuthToken refreshUserToken(OAuthToken token) {
        String h = token.getUsedHostKey();
        String c = token.getUsedClientKey();

        return OAuthHelper.refreshToken(getClient(), h, c, token);
    }

    /**
     * Get an OAuth2 token for an client (without user credentials).
     *
     * @param hostId     OAuth server to response the token, normally an account manager
     * @param clientKey  a key for {@link HttpAuth} which defines client login data
     * @param bearerAuth A key name that is used for later accessing data by using the TOKEN from the login process.
     */
    public OAuthToken loginClient(String hostId, String clientKey, String bearerAuth) {
        return OAuthHelper.getTokenForClient(getClient(), hostId, clientKey, bearerAuth);
    }

    /**
     * Get a OAuth2 token for a service provider with user credentials.
     *
     * @param hostId     OAuth server to response the token, normally an account manager
     * @param clientKey  a key for {@link HttpAuth} which defines client login data
     * @param userKey    a key for {@link HttpAuth} which defines user login data
     * @param scope      not used
     * @param bearerAuth A key name that is used for later accessing data by using the TOKEN from the login process.
     */
    public OAuthToken loginClientUser(String hostId, String clientKey, String userKey, String scope,
                                      String bearerAuth) {
        return OAuthHelper.getTokenForClientUser(getClient(), hostId, clientKey, userKey, scope, bearerAuth, false);
    }
}
