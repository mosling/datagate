package me.mosling.datagate.fixture;

import me.mosling.httpfixture.fixture.*;
import me.mosling.httpfixture.security.JksManager;
import me.mosling.httpfixture.security.JksManagerArgs;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class FixturePublicHttp
        extends BaseHttpFixture
        implements Fixture {
    private static final Logger LOGGER = LogManager.getLogger(FixturePublicHttp.class);

    private final String configFile;
    private final String configConnection;

    public FixturePublicHttp(String configFile, String configConnection) {
        this.configFile = configFile;
        this.configConnection = configConnection;

        build(httpArgs(), jksManager());
        LOGGER.info("'{}' .. ready", getClient().getConnectionUrl(configConnection));
    }

    private HttpClientArgs httpArgs() {
        return new HttpClientArgs().requestTimeoutMs(80000).configFile(configFile).enableRedirect(true);
    }

    private JksManager jksManager() {
        JksManagerArgs ja = new JksManagerArgs().jdkIncludeCertificates(true);

        return new JksManager(ja);
    }

    public ResponseData doGetRequest(String resource, String address) {
        String con = configConnection.isEmpty() ? resource : configConnection;
        return executeHttp("GET", con, "", Fixture.correctUrlAddress(address, getClient().getConnectionUrl(con)),
                "", -1);
    }

    public ResponseData doWriteRequest(String resource, String method, String address, String data) {
        HttpRequest req = HttpClientFactory.defaultRequest(method, !data.isEmpty());
        if (!data.isEmpty()) {
            req.setTextBody(data);
        }

        String con = configConnection.isEmpty() ? resource : configConnection;
        return executeRequest(req, con, "", Fixture.correctUrlAddress(address, getClient().getConnectionUrl(con)),
                -1);
    }

    @Override
    public boolean isConnected() {
        return true;
    }

    @Override
    public String getName() {
        return "";
    }
}
