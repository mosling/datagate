package me.mosling.datagate.mqtt;

import me.mosling.httpfixture.common.StringHelper;

import java.util.Date;
import java.util.TimerTask;
import java.util.concurrent.atomic.AtomicInteger;

public class PublishHeartbeatTask
        extends TimerTask {
    private final MessageClient messageClient;
    private final AtomicInteger counter = new AtomicInteger(0);

    @Override
    public void run() {
        messageClient.publish("heartbeat",
                String.format("%d -- %s -- %d", counter.getAndIncrement(), StringHelper.getUtcString(new Date()),
                        System.identityHashCode(this)));
    }

    public PublishHeartbeatTask(MessageClient messageClient) {
        this.messageClient = messageClient;
    }
}

