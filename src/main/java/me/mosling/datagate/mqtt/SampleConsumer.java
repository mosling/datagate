package me.mosling.datagate.mqtt;

import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.DefaultConsumer;
import com.rabbitmq.client.Envelope;
import me.mosling.httpfixture.common.StringHelper;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.nio.charset.StandardCharsets;

public class SampleConsumer
        extends DefaultConsumer {

    private static final Logger LOGGER = LogManager.getLogger(SampleConsumer.class);
    private static final Level always = Level.getLevel("ALWAYS");

    private MessageClient messageClient;
    private String routingPattern = "";
    private String consumerName = "";
    private String queueName = "";

    public SampleConsumer(Channel channel) {
        super(channel);
    }

    public SampleConsumer(MessageClient mc, String consumerName) {
        super(mc.getClientChannel());
        this.messageClient = mc;

        this.consumerName = StringHelper.nonNullStr(consumerName);
    }

    public SampleConsumer subscribePattern(String routingPattern, boolean exclusive) {
        this.routingPattern = routingPattern;
        messageClient.addPatternConsumer(this, consumerName, routingPattern, exclusive);

        return this;
    }

    public SampleConsumer subscribeQueue(String queueName) {
        this.queueName = queueName;
        messageClient.addQueueConsumer(this, consumerName, queueName);

        return this;
    }

    @Override
    public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) {
        String message = new String(body, StandardCharsets.UTF_8);
        LOGGER.log(always, " [{}] Received '{}':'{}' from {}", routingPattern.isEmpty() ? queueName : routingPattern,
                envelope.getRoutingKey(), message, consumerTag);
    }
}

