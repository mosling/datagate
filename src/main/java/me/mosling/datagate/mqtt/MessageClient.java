package me.mosling.datagate.mqtt;

import com.rabbitmq.client.*;
import lombok.Getter;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.net.URISyntaxException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.concurrent.TimeoutException;

public class MessageClient
        implements AutoCloseable {

    private static final Logger LOGGER = LogManager.getLogger(MessageClient.class);

    @Getter
    private final ConnectionFactory factory = new ConnectionFactory();
    @Getter
    private final String exchangeName;
    @Getter
    private String amqpHost;

    @Getter
    private Connection clientConnection;
    @Getter
    private Channel clientChannel;

    public MessageClient(String exchangeName, String cloudamqpUrl) {
        this.exchangeName = exchangeName;

        if (cloudamqpUrl == null || cloudamqpUrl.isEmpty()) {
            LOGGER.error("missing cloudamqp url -- can't create message client");
            return;
        }

        try {
            // setup factory
            factory.setUri(cloudamqpUrl);
            factory.setAutomaticRecoveryEnabled(true);
            factory.setNetworkRecoveryInterval(10000); // milliseconds
            amqpHost = factory.getHost();

            // create connection
            clientConnection = factory.newConnection();

            // create channel and declare exchange passive to check if the exchange exists
            clientChannel = clientConnection.createChannel();

            try {
                clientChannel.exchangeDeclarePassive(exchangeName);
            } catch (IOException ex) {
                LOGGER.info("missing exchange '{}' create a new topic exchange ", exchangeName);
                clientChannel = clientConnection.createChannel();
                clientChannel.exchangeDeclare(exchangeName, BuiltinExchangeType.TOPIC, true, false, null);
            }

            LOGGER.info("connection to {}:{} using exchange '{}' {}", amqpHost, factory.getPort(), exchangeName,
                    (clientChannel.isOpen() ? ".. ready" : ".. failed"));
        } catch (URISyntaxException | NoSuchAlgorithmException | KeyManagementException e) {
            LOGGER.error("URI failure: {}", e.getMessage());
        } catch (TimeoutException ex) {
            LOGGER.error("Timeout establish connection to '{}':{} ({})", amqpHost, exchangeName, ex.getMessage());
        } catch (IOException ex) {
            LOGGER.error("Can't establish connection to '{}' possible missing exchange '{}'", amqpHost, exchangeName);
        }
    }

    public void publish(String routingKey, String text) {
        if (null != clientChannel && clientChannel.isOpen()) {
            try {
                clientChannel.basicPublish(exchangeName, routingKey, null, text.getBytes());
            } catch (IOException ex) {
                LOGGER.error("publishing '{}' to '{}' with routingKey '{}' >> '{}'", text, exchangeName, routingKey,
                        ex.getMessage());
            }
        } else {
            LOGGER.warn("publish '{}' to exchange '{}' not possible (channel is closed)", routingKey, exchangeName);
        }
    }

    @SuppressWarnings("squid:S2095")
    public void addPatternConsumer(DefaultConsumer consumer, String consumerName, String routingPattern,
                                   boolean exclusive) {
        try {
            Channel consumerChannel = clientConnection.createChannel();
            consumerChannel.exchangeDeclarePassive(exchangeName);

            // create a queue for the consumer
            String queueName = consumerChannel.queueDeclare(consumerName, true, exclusive, false, null).getQueue();
            // bind this new queue to the exchange for this message client
            consumerChannel.queueBind(queueName, exchangeName, routingPattern);

            consumerChannel.basicConsume(queueName, true, consumerName, consumer);

            LOGGER.info("add {}consumer {} as '{}' for exchange '{}' with pattern '{}'",
                    exclusive ? " exclusive " : "", consumer.getClass().getName(), consumerName, exchangeName,
                    routingPattern);
        } catch (IOException ex) {
            LOGGER.error("add consumer '{}' to exchange '{}' for routingPattern '{}' >> '{}'",
                    consumer.getClass().getName(), exchangeName, routingPattern, ex.getMessage());
        }
    }

    @SuppressWarnings("squid:S2095")
    public void addQueueConsumer(DefaultConsumer consumer, String consumerName, String queueName) {
        try {
            Channel consumerChannel = clientConnection.createChannel();
            consumerChannel.exchangeDeclarePassive(exchangeName);
            consumerChannel.basicConsume(queueName, true, consumerName, consumer);

            LOGGER.info("add consumer {}{} for queue '{}'", consumer.getClass().getName(),
                    consumerName.isEmpty() ? "" : " as '" + consumerName + "'", queueName);
        } catch (IOException ex) {
            LOGGER.error("add consumer '{}' to queue '{}' >> '{}'", consumer.getClass().getName(), queueName,
                    ex.getMessage());
        }

    }

    @Override
    public void close() {
        String broker = String.format("'%s':%s", amqpHost, exchangeName);

        if (clientChannel != null) {
            try {
                clientChannel.close();
                LOGGER.info("close channel to {}", broker);
            } catch (TimeoutException ex) {
                LOGGER.error("Timeout close channel to {} ({})", broker, ex.getMessage());
            } catch (IOException ex) {
                LOGGER.error("Error close channel to {} ({})", broker, ex.getMessage());
            }
        }

        if (clientConnection != null) {
            try {
                clientConnection.close();
                LOGGER.info("close connection to {}", broker);
            } catch (IOException ex) {
                LOGGER.error("Error close connection to {} ({})", broker, ex.getMessage());
            }
        }
    }

}

