package me.mosling.datagate.publisher;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import me.mosling.datagate.authentication.Authentication;
import me.mosling.datagate.authentication.User;
import me.mosling.datagate.common.Version;
import me.mosling.datagate.core.GeneralException;
import me.mosling.datagate.core.Metadata;
import me.mosling.datagate.core.ResourceMetadata;
import me.mosling.datagate.fixture.Fixture;
import me.mosling.httpfixture.common.StringHelper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.app.event.implement.IncludeRelativePath;
import org.apache.velocity.exception.VelocityException;
import org.apache.velocity.runtime.RuntimeConstants;
import org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader;
import org.apache.velocity.tools.generic.EscapeTool;

import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

@Accessors(fluent = true)
public class PublisherVelocity
        implements Publisher {
    private static final Logger LOGGER = LogManager.getLogger(PublisherVelocity.class);

    @Getter
    @Setter
    private Fixture fixture;

    @Getter
    private final VelocityEngine velocity;
    @Getter
    private final Authentication authentication;

    public PublisherVelocity(Authentication authentication) {
        Properties p = new Properties();
        p.setProperty(VelocityEngine.RESOURCE_LOADER, "classpath");
        p.setProperty("classpath." + VelocityEngine.RESOURCE_LOADER + ".class",
                ClasspathResourceLoader.class.getName());
        p.setProperty(RuntimeConstants.EVENTHANDLER_INCLUDE, IncludeRelativePath.class.getName());

        velocity = new VelocityEngine();
        velocity.init(p);

        this.authentication = authentication;

        LOGGER.info(".. ready");
    }

    public String render(String templatePath, User user, ResourceMetadata rmd)
            throws GeneralException {
        Map<String, Object> m = new HashMap<>();
        m.put("resource", rmd);

        return render(templatePath, user, m);
    }

    public String render(String templatePath, User user, Map<String, Object> model)
            throws GeneralException {
        Map<String, Object> m = model;
        if (null == m) {
            m = new HashMap<>();
        }

        if (user != null && !m.containsKey("user")) {
            m.put("user", user);
        }

        if (authentication != null) {
            m.put("authenticationFields", authentication.getAuthenticationConfig().loginFields());
            m.put("authenticationMethod", authentication.getAuthenticationConfig().methodAction());
        }

        if (null != fixture) {
            m.put("fixtureConnected", fixture.isConnected());
            m.put("fixtureName", StringHelper.nonNullStr(fixture.getName()));
        }

        m.put("serviceDisplayname", StringHelper.nonNullStr(Metadata.getServiceName()));
        m.put("serviceName", StringHelper.nonNullStr(Metadata.getServiceName()));
        m.put("developmentMode", Metadata.isDevelopmentMode());
        m.put("metadata", Metadata.class);
        m.put("Version", Version.class);
        m.put("esc", new EscapeTool());

        VelocityContext context = new VelocityContext(m);
        Template template = velocity.getTemplate(templatePath);
        if (null == template) {
            String errMsg = String.format("Template '%s' not found at server.", templatePath);
            LOGGER.error(errMsg);
            return errMsg;
        } else {
            if (Metadata.isDevelopmentMode()) {
                LOGGER.info("DEVELOPMENT-PUBLISH: '{}'", templatePath);
            }
            StringWriter sw = new StringWriter();
            try {
                template.merge(context, sw);
            } catch (VelocityException ex) {
                LOGGER.error(ex);
                throw new GeneralException(500, false, ex.getMessage());
            }
            return sw.toString();
        }
    }
}
