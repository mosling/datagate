package me.mosling.datagate.publisher;

import me.mosling.datagate.authentication.User;
import me.mosling.datagate.core.GeneralException;
import me.mosling.datagate.core.ResourceMetadata;

import java.util.Map;

public interface Publisher {
    String render(String templatePath, User user, ResourceMetadata rmd) throws GeneralException;

    String render(String templatePath, User user, Map<String, Object> model) throws GeneralException;
}
