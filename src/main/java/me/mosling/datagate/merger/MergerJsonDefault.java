package me.mosling.datagate.merger;

import lombok.Getter;
import lombok.Setter;
import me.mosling.datagate.common.JsonObjectHelper;
import me.mosling.datagate.core.ResourceMetadata;
import me.mosling.datagate.notification.Notification;
import me.mosling.datagate.notification.NotificationEvent;
import me.mosling.httpfixture.common.JsonHelper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class MergerJsonDefault
        implements Merger {
    private static final Logger LOGGER = LogManager.getLogger(MergerJsonDefault.class);

    @Getter
    @Setter
    private Notification notification = null;

    /**
     * @param rmd    the resource data for the objects
     * @param source the new data
     * @param target the base data and each new or modified field from source is overwritten
     *
     * @return false if nothing was merged
     */
    @Override
    public boolean mergeJson(ResourceMetadata rmd, JSONObject source, JSONObject target) {
        String fqn = rmd.getFqn();
        List<String> updateList = new ArrayList<>();

        boolean b = deepMerge(fqn, "$", source, target, updateList);

        if (null != notification && !updateList.isEmpty()) {
            updateList.add(0, "[");
            updateList.add(0, "changes");
            updateList.add("]");
            String pkValue = JsonObjectHelper.getField(target, rmd.getPrimaryKey());

            if (!pkValue.isEmpty()) {
                updateList.add(rmd.getPrimaryKey());
                updateList.add(pkValue);
            }

            notification.notify(NotificationEvent.UPDATE, fqn, JsonHelper.createJsonFromList(updateList));
        }

        return b;
    }

    @SuppressWarnings("squid:S3776")
    private boolean deepMerge(String fqn, String jsonPath, JSONObject source, JSONObject target,
                              List<String> updateList) {
        boolean merged = false;
        for (String key : getNodeNames(source)) {
            Object sourceObject = source.get(key);
            // put value if not exists or if one and only one  of the fields (xor) is null
            if (!target.has(key) || (source.isNull(key) ^ target.isNull(key))) {
                // new value for "key"
                if (null != notification) {
                    updateList.addAll(Arrays.asList("{", "action", "new", "path", jsonPath + "." + key, "value",
                            sourceObject.toString(), "}"));
                }
                merged = true;
                target.put(key, sourceObject);
            } else {
                Object targetObject = target.get(key);

                if (sourceObject.getClass() != targetObject.getClass()) {
                    LOGGER.warn(
                            "MERGE({}): ERR {}.{} with different types source={} and target={} --> ignore this field",
                            fqn, jsonPath, key, sourceObject.getClass().getSimpleName(),
                            targetObject.getClass().getSimpleName());
                    continue;
                }

                // existing value for "key" - recursively deep merge:
                if (sourceObject instanceof JSONObject) {
                    merged |= deepMerge(fqn, jsonPath + "." + key, (JSONObject) sourceObject,
                            target.getJSONObject(key), updateList);
                } else if (!sourceObject.toString().equals(target.get(key).toString())) {
                    // updating existing value
                    if (null != notification) {
                        updateList.addAll(
                                Arrays.asList("{", "action", "update", "path", jsonPath + "." + key, "newValue",
                                        sourceObject.toString(), "oldValue", target.get(key).toString(), "}"));
                    }
                    merged = true;
                    target.put(key, sourceObject);
                }
            }
        }

        return merged;
    }

    private static List<String> getNodeNames(JSONObject jo) {
        String[] a = JSONObject.getNames(jo);
        return null == a ? Collections.emptyList() : new ArrayList<>(Arrays.asList(a));
    }

    public String toJson() {
        return JsonHelper.createJsonFromString(
                String.format("name,%s,notification,%s", this.getClass().getSimpleName(),
                        notification == null ? "null" : notification.getClass().getSimpleName()), ",");
    }
}
