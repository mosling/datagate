package me.mosling.datagate.merger;

import me.mosling.datagate.core.ResourceMetadata;
import me.mosling.datagate.notification.Notification;
import org.json.JSONObject;

public interface Merger {
    void setNotification(Notification notification);

    Notification getNotification();

    boolean mergeJson(ResourceMetadata rmd, JSONObject source, JSONObject target);
}
