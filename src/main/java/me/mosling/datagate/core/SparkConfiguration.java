package me.mosling.datagate.core;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import me.mosling.datagate.authentication.Authentication;
import me.mosling.datagate.common.Paths;
import me.mosling.datagate.conversion.ConversionDictionary;
import me.mosling.datagate.formatter.DataFormatter;
import me.mosling.datagate.formatter.DataFormatterDictionary;
import me.mosling.datagate.parser.QueryFilterParser;
import me.mosling.datagate.parser.QueryParameter;
import me.mosling.datagate.publisher.Publisher;
import me.mosling.httpfixture.common.StringHelper;
import me.mosling.httpfixture.fixture.HttpStatusCode;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;
import spark.Request;
import spark.Response;

import java.io.File;
import java.util.*;

import static spark.Spark.*;

public final class SparkConfiguration {
    private static final Logger LOGGER = LogManager.getLogger(SparkConfiguration.class);
    private static final Level always = Level.getLevel("ALWAYS");

    private static boolean corsEnabled = false;
    private static Set<String> accessControlMethods = new HashSet<>();
    private static Set<String> accessControlHeaders = new HashSet<>();

    private SparkConfiguration() {
        // functional class
    }

    public static void logRequest(Request req)
            throws JsonProcessingException {
        if (LOGGER.isDebugEnabled()) {
            ObjectMapper om = new ObjectMapper();
            om.disable(SerializationFeature.FAIL_ON_UNWRAPPED_TYPE_IDENTIFIERS);
            om.disable(SerializationFeature.FAIL_ON_SELF_REFERENCES);

            String jid = "JSESSIONID";
            LOGGER.debug("before all ");
            LOGGER.debug("   url         : {}::{}", req.requestMethod(), req.url());
            LOGGER.debug("   path        : {}", req.pathInfo());
            LOGGER.debug("   session-id  : {}", req.session().id());
            LOGGER.debug("   JSESSIONID  : {}", req.cookie(jid) == null ? "no set" : req.cookie(jid));
            LOGGER.debug("   attributes  : {}", req.session().attributes());
            LOGGER.debug("   params      : {}", om.writeValueAsString(req.params()));
            LOGGER.debug("   queryMap    : {}", om.writeValueAsString(req.queryMap().toMap()));
            LOGGER.debug("   attributes  : {}", om.writeValueAsString(req.attributes()));
            LOGGER.debug("   cookies     : {}", om.writeValueAsString(req.cookies()));
            LOGGER.debug("   servletPath : {}", req.servletPath());
            LOGGER.debug("   userAgent   : {}", req.userAgent());
            LOGGER.debug("   body        : '{}'", req.body());
            LOGGER.debug("-----------------------------------------");
        } else if (Metadata.isDevelopmentMode()) {
            LOGGER.info("DEVELOPMENT-REQUEST : {}::{} with session id::{}{}", req.requestMethod(), req.url(),
                    req.session().isNew() ? "NEW " : "", req.session().id());
        }
    }

    public static void logResponse(Response res) {

        int l = res.body() != null ? res.body().length() : 0;

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("after all ");
            LOGGER.debug("   status : {}", res.status());
            LOGGER.debug("   type   : {}", res.type());
            LOGGER.debug("   length : {}", l);
            if (l > 0) {
                LOGGER.debug("   type   : {}",
                        res.body().replace(System.lineSeparator(), " ").substring(0, Math.min(l, 50)));
            }
            LOGGER.debug("-----------------------------------------");
        } else if (Metadata.isDevelopmentMode()) {
            LOGGER.info("DEVELOPMENT-RESPONSE: {} with type '{}'{}", res.status(), res.type(),
                    l > 0 ? String.format(" length=%d", l) : "");
        }
    }

    private static Map<String, Object> getError(GeneralException exception, Request req) {
        Map<String, Object> err = new HashMap<>();

        err.put("status", exception.getStatus());
        err.put("statusText", HttpStatusCode.explain(exception.getStatus()));
        err.put("message",
                exception.isAsJson() ? exception.getMessage().replaceAll("<[/]*b>", "**") : exception.getMessage());
        err.put("path", req.pathInfo());
        err.put("method", req.requestMethod());

        return err;
    }

    /**
     * @param methods list of allowed methods or empty to allow all
     * @param headers list of allowed header entries or empty to allow all
     */
    public static void enableCORS(final String methods, final String headers) {
        corsEnabled = true;

        if (!methods.isEmpty() && !"*".equalsIgnoreCase(methods)) {
            accessControlMethods.addAll(Arrays.asList(methods.toLowerCase().split(",")));
        }

        if (!headers.isEmpty() && !"*".equalsIgnoreCase(headers)) {
            // add simple headers
            accessControlHeaders.addAll(Arrays.asList("accept,accept-language,content-language,last-event-id,content-type,".split(",")));

            // add CORS request headers
            accessControlHeaders.addAll(Arrays.asList("origin,access-control-request-methods,access-control-request-headers".split(",")));

            // add header given by application
            accessControlHeaders.addAll(Arrays.asList(headers.toLowerCase().split(",")));
        }
    }

    // check a method against the allowed methods
    private static void methodAccessControl(String method)
            throws GeneralException {
        if (!accessControlMethods.isEmpty() && !accessControlMethods.contains(method.toLowerCase())) {
            throw new GeneralException(403, true, String.format("method '%s' not allowed", method));
        }
    }

    private static void headersAccessControl(Set<String> headers)
            throws GeneralException {
        if (!accessControlHeaders.isEmpty()) {
            List<String> wrongHeaders = new ArrayList<>();
            for (String h : headers) {
                if (!accessControlHeaders.contains(h.toLowerCase())) {
                    wrongHeaders.add(h);
                }
            }
            if (!wrongHeaders.isEmpty()) {
                throw new GeneralException(403, true, String.format("header ('%s') not allowed", String.join(",", wrongHeaders)));
            }
        }
    }

    private static void accessControl(Request req, Response res)
            throws GeneralException {

        if (!corsEnabled) {
            return;
        }

        String method = req.requestMethod();
        String origin = StringHelper.nonNullStr(req.headers("Origin"));

        if ("options".equalsIgnoreCase(method)) {
            // CORS preflight: Origin and Access-Control-Request-Method must be set
            String requestMethod = StringHelper.nonNullStr(req.headers("Access-Control-Request-Method"));

            if (origin.isEmpty() || requestMethod.isEmpty()) {
                halt(400, "");
            }
            methodAccessControl(requestMethod);
            headersAccessControl(new HashSet<>(Arrays.asList(StringHelper.nonNullStr(req.headers("Access-Control-Request-Headers")).toLowerCase().split(","))));

            // without GeneralException generate the Success Response
            res.header("Access-Control-Allow-Origin", origin);
            res.header("Access-Control-Allow-Methods", accessControlMethods.isEmpty() ? "*" : String.join(",", accessControlMethods));
            res.header("Access-Control-Allow-Headers", accessControlHeaders.isEmpty() ? "*" : String.join(",", accessControlHeaders));
            res.header("Access-Control-Max-Age", "86400");
            res.header("Access-Control-Allow-Credentials", "true");

            halt(200, "");

        } else if (!origin.isEmpty()) {
            // CORS request: Origin must be set
            methodAccessControl(method);
            headersAccessControl(req.headers());

            res.header("Access-Control-Allow-Origin", origin);
        }
    }

    public static void addStatic(String staticFolder) {
        boolean useStaticResource = true;

        if (!StringHelper.nonNullStr(staticFolder).isEmpty()) {
            if (Metadata.isDevelopmentMode()) {
                File f = new File(staticFolder);
                if (f.exists() && f.isDirectory()) {
                    staticFiles.externalLocation(staticFolder);
                    useStaticResource = false;

                    LOGGER.info("due on DEVELOPMENT mode set external static location to '{}'", staticFolder);
                } else {
                    LOGGER.warn("DEVELOPMENT mode and given the static folder '{}' but the folder dosen't exists",
                            staticFolder);
                }
            } else {
                LOGGER.warn("Static path '{}' is ignored, please set -Ddevelopment=true to use it", staticFolder);
            }
        }

        // need this in every case to find logo, csss, ...
        if (useStaticResource) {
            staticFiles.location(Metadata.pathFor(Paths.WEB_STATIC));
        }
    }

    private static void addException(String mainpage, Publisher p) {
        exception(GeneralException.class, (exception, req, res) -> {
            if (Metadata.isDevelopmentMode()) {
                LOGGER.info("ERROR-REQUEST : {}::{}", req.requestMethod(), req.url());
                LOGGER.info("ERROR-RESPONSE: {}", res.status());
                Metadata.logMeMoslingStacktrace();
            }
            res.status(exception.getStatus());
            res.type(exception.isAsJson() ? "application/json" : "text/html");
            try {
                res.body(p.render(
                        Metadata.pathFor(exception.isAsJson() ? Paths.TEMPLATE_ERROR_JSON : Paths.TEMPLATE_ERROR),
                        req.session().attribute(Authentication.SESSION_USER), getError(exception, req)));
            } catch (GeneralException ex) {
                res.type("text/html");
                res.body("Error rendering Error-Message --> " + ex.getMessage());
            }
        });

        notFound((req, res) -> {
            LOGGER.info("NOT FOUND '{}'", req.url());
            res.redirect(mainpage);
            return "";
        });

    }

    public static void addChartPath(Authentication authentication, Publisher publisher) {
        get("/chart/:name", (req, res) -> {
            ResourceMetadata rmd = Authentication.examineAccess(req, authentication,
                    Metadata.pathFor(Paths.RESOURCE_NAME));

            if (rmd != null) {
                Map<String, Object> publisherMap = new TreeMap<>();
                for (String p : Arrays.asList("select", "filter", "order", "limit", "labels")) {
                    if (req.queryParams().contains(p)) {
                        publisherMap.put(p, req.queryParams(p));
                    }
                }

                String[] selectArray = ((String) publisherMap.getOrDefault("select", "")).split(",");
                if (selectArray.length < 2) {
                    throw new GeneralException(400, true, "Chart need at minimum two elements in select parameter.");
                }

                publisherMap.put("xaxis", selectArray[0]);
                publisherMap.put("yaxis", selectArray[1]);
                if (selectArray.length > 2) {
                    publisherMap.put("group", selectArray[2]);
                }

                if (!publisherMap.containsKey("labels")) {
                    publisherMap.put("labels", publisherMap.get("select"));
                }

                if (!publisherMap.containsKey("order")) {
                    publisherMap.put("order", selectArray[0]);
                }

                publisherMap.put("labelList", String.format(("'%s'"),
                        String.join("', '", ((String) publisherMap.get("labels")).split(","))));

                publisherMap.put("resource", rmd);

                return publisher.render(Metadata.pathFor(Paths.TEMPLATE_CHART),
                        req.session().attribute(Authentication.SESSION_USER), publisherMap);
            } else {
                return "";
            }
        });
    }

    @SuppressWarnings("squid:S3776")
    public static void addPaths(String mainPage, Authentication authentication, DataManager dm, Publisher publisher) {
        String rn = Metadata.pathFor(Paths.RESOURCE_NAME);
        String landingPage = mainPage.isEmpty() ? "/list" : mainPage;

        addException(landingPage, publisher);

        // * add some redirect pages
        redirect.get("/", landingPage);

        // * add development mode request logging
        if (Metadata.isDevelopmentMode()) {
            before((req, res) -> {
                logRequest(req);
                accessControl(req, res);
            });

            afterAfter((req, res) -> logResponse(res));
        } else {
            before(SparkConfiguration::accessControl);
        }

        // * add the application endpoints
        // ** metadata activities visible for admin users
        path("/meta", () -> {
            before("*", (req, res) -> Authentication.examineAccess(req, authentication, rn));

            get("/info/:name", (req, res) -> {
                ResourceMetadata rmd = Authentication.examineAccess(req, authentication, rn);
                return publisher.render(Metadata.pathFor(Paths.TEMPLATE_INFO),
                        req.session().attribute(Authentication.SESSION_USER), rmd);
            });

            get("/update/:name", (req, res) -> {
                ResourceMetadata rmd = Authentication.examineAccess(req, authentication, rn);
                if (null != rmd) {
                    Metadata.updateResourceFromStorage(
                            Metadata.getMetadataFilterFor(rmd.getApplication(), rmd.getResourceName()), false, null);
                    res.redirect(Metadata.pathFor(Paths.WEB_INFO) + req.params(rn));
                }
                return "";
            });
        });

        // ** rest like query interface to the stored data
        get("/data/:name", (req, res) -> {
            ResourceMetadata rmd = Authentication.examineAccess(req, authentication, rn);
            if (rmd != null) {
                // first check the format to throws an exception before generate data
                DataFormatter fi = DataFormatterDictionary.find(req.queryParams("format"));

                String selectStr = req.queryParams("select");
                QueryParameter qp = new QueryParameter(req.queryParams("filter"), selectStr,
                        req.queryParams("order"), req.queryParams("limit"));

                if (!qp.getErrorEntries().isEmpty()) {
                    throw new GeneralException(400, true,
                            QueryFilterParser.prepareJsonErrorList(qp.getErrorEntries()));
                }

                fi.modifySparkResponse(rmd.getResourceName(), res);
                return dm.getDataList(rmd, qp, fi);
            } else {
                return "";
            }
        });

        // ** returns a key:value list for the requested resource
        get("/lookup/:name", (req, res) -> {
            ResourceMetadata rmd = Authentication.examineAccess(req, authentication, rn);
            res.type("application/json");
            if (rmd != null) {
                return dm.getLookupList(rmd, req.queryParams("key"), req.queryParams("label"),
                        "true".equalsIgnoreCase(req.queryParams("asObjects")));
            } else {
                return "{}";
            }
        });

        // ** return sidebar only, useful as landing page for user with restrictes access
        get("/list", (req, res) -> publisher.render(Metadata.pathFor(Paths.TEMPLATE_LIST), req.session().attribute(Authentication.SESSION_USER), (ResourceMetadata) null));

        // ** return the complete list (no paging) for the requested resource
        get("/list/:name", (req, res) -> {
            ResourceMetadata rmd = Authentication.examineAccess(req, authentication, rn);
            return publisher.render(Metadata.pathFor(Paths.TEMPLATE_LIST),
                    req.session().attribute(Authentication.SESSION_USER), rmd);
        });

        // ** refresh resource, that is delete all existing data (if resource not block this) an get new from connector
        get("/refresh/:name", (req, res) -> {
            ResourceMetadata rmd = Authentication.examineAccess(req, authentication, rn);
            dm.refreshData(rmd, false);
            res.redirect(Metadata.pathFor(Paths.WEB_LIST) + req.params(rn));
            return "";
        });

        // ** update resource, read data from collector and update existing data
        get("/update/:name", (req, res) -> {
            ResourceMetadata rmd = Authentication.examineAccess(req, authentication, rn);

            dm.refreshData(rmd, true);
            res.redirect(Metadata.pathFor(Paths.WEB_LIST) + req.params(rn));
            return "";
        });

        // ** open the details view to create a new resource entry
        get("/create/:name", (req, res) -> {
            Authentication.examineAccess(req, authentication, rn);
            return publisher.render(Metadata.pathFor(Paths.TEMPLATE_DETAILS),
                    req.session().attribute(Authentication.SESSION_USER),
                    dm.getDataResourceMap(req.params(rn), ""));
        });

        // ** activities from the details view
        path(Metadata.pathFor(Paths.WEB_DETAILS), () -> {

            // *** refresh the current id end goto details view
            get("/refresh/:name/*", (req, res) -> {
                ResourceMetadata rmd = Authentication.examineAccess(req, authentication, rn);
                if (rmd != null) {
                    if (rmd.getConnector() != null) {
                        rmd.getConnector().fetchEntry(rmd, getUrlId(req));
                    }
                    res.redirect(String.format("/details/%s/%s", rmd.getResourceName(), getUrlId(req)));
                }
                return "";
            });

            // *** delete the current entry and switch to the list view
            get("/delete/:name/*", (req, res) -> {
                if (Metadata.isDevelopmentMode()) {
                    LOGGER.log(always, "DEVELOPMENT-DELETE ... {}.{}", req.params(rn), getUrlId(req));
                }
                ResourceMetadata rmd = Authentication.examineAccess(req, authentication, rn);
                dm.deleteDataEntry(rmd, getUrlId(req));
                res.redirect(Metadata.pathFor(Paths.WEB_LIST) + req.params(rn));
                return "";
            });

            // *** show details for resource entry
            get("/:name/*", (req, res) -> {
                ResourceMetadata rmd = Authentication.examineAccess(req, authentication, rn);

                if (null == rmd || dm.getDataEntry(rmd, getUrlId(req)).isEmpty()) {
                    throw new GeneralException(404, false,
                            "Requested entry not found for <b>" + req.pathInfo() + "</b>");
                }

                return publisher.render(Metadata.pathFor(Paths.TEMPLATE_DETAILS),
                        req.session().attribute(Authentication.SESSION_USER),
                        dm.getDataResourceMap(req.params(rn), getUrlId(req)));
            });

            // *** create a new entry for the resource
            post("/:name", (req, res) -> {
                if (Metadata.isDevelopmentMode()) {
                    LOGGER.log(always, "DEVELOPMENT-CREATE ... {}", req.body());
                }
                ResourceMetadata rmd = Authentication.examineAccess(req, authentication, rn);
                if (rmd != null) {
                    if (rmd.isRemoteWritable()) {
                        // implicit update new data from connector
                        return rmd.getConnector().createEntry(rmd, "", req.body());
                    } else {
                        // without connector this is a cache only entry
                        dm.addOrUpdateData(rmd, "DATA", new JSONObject(req.body()));
                    }
                }
                return "{}";
            });

            // *** update entry :id for the resource, we use a star match all possible
            //     pattern, also some url encoded url like http://serviceprovider.com
            post("/:name/*", (req, res) -> {
                if (Metadata.isDevelopmentMode()) {
                    LOGGER.log(always, "DEVELOPMENT-UPDATE ... {}", req.body());
                }
                ResourceMetadata rmd = Authentication.examineAccess(req, authentication, rn);
                if (rmd != null) {
                    // update storage always to persists storage only attributes
                    dm.addOrUpdateData(rmd, ConversionDictionary.NODE_DATA, new JSONObject(req.body()));
                    if (rmd.isRemoteWritable()) {
                        // implicit update new data from connector
                        return rmd.getConnector().createEntry(rmd, getUrlId(req), req.body());
                    }
                }
                return "{}";
            });
        });
    }

    private static String getUrlId(Request req)
            throws GeneralException {
        if (0 == req.splat().length) {
            throw new GeneralException(400, false, "Missing url path parameter like .../MY-ID42");
        }

        return req.splat()[0];
    }
}
