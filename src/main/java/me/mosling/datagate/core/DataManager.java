package me.mosling.datagate.core;

import lombok.Getter;
import lombok.Setter;
import me.mosling.datagate.common.JsonObjectHelper;
import me.mosling.datagate.conversion.Conversion;
import me.mosling.datagate.conversion.ConversionDictionary;
import me.mosling.datagate.formatter.DataFormatter;
import me.mosling.datagate.notification.Notification;
import me.mosling.datagate.notification.NotificationEvent;
import me.mosling.datagate.parser.QueryParameter;
import me.mosling.datagate.storage.JsonStorage;
import me.mosling.httpfixture.common.JsonHelper;
import me.mosling.httpfixture.common.StringHelper;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;

import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;

public class DataManager {
    private static final Logger LOGGER = LogManager.getLogger(DataManager.class);

    @Getter
    private final JsonStorage storage;
    @Getter
    @Setter
    private Notification notification = null;
    private final Map<String, Thread> runningFetchLists = new TreeMap<>();

    public DataManager(JsonStorage storage) {
        this.storage = storage;

        LOGGER.info("setup for storage {} .. ready", storage.getClass().getName());
    }

    @SuppressWarnings("UnusedReturnValue")
    public int refreshCountData(ResourceMetadata rmd) {
        if (null == rmd) {
            return 0;
        }

        int c = storage.count(rmd.getLocalStorageName(), null);
        rmd.setCountData(c);

        return c;
    }

    @SuppressWarnings({"squid:S2589", "squid:3776"}) // rmd can be null if called from methods outside datagate
    public void refreshData(ResourceMetadata rmd, boolean asUpdate) {
        if (null == rmd) {
            LOGGER.error("refresh data called with null resource");
            Metadata.logMeMoslingStacktrace();
            return;
        }

        if (isFetchRunning(rmd)) {
            return;
        }

        if (!rmd.getResourcePath().isEmpty() && rmd.getConnector() != null) {
            if (rmd.isDropBeforeRefresh() && !asUpdate) {
                LOGGER.info("[{}] drop all cached data at {}", rmd.getResourceName(), rmd.getLocalStorageName());
                storage.delete(rmd.getLocalStorageName(), null, true);
            }
            startFetchThread(rmd);

        } else if (LOGGER.isInfoEnabled()) {
            String msg = String.format("Can't refresh%s resource %s; path: %s; connector: %s",
                    asUpdate ? " (asUpdate)" : "", rmd.getLocalStorageName(),
                    !rmd.getResourcePath().isEmpty() ? rmd.getResourcePath() : "<not set>",
                    rmd.getConnector() != null ? rmd.getConnector().getClass().getSimpleName() : "<is null>");
            LOGGER.info(msg);
        }
    }

    @SuppressWarnings("squid:S3776")
    public void addOrUpdateData(ResourceMetadata rmd, String connectorName, JSONObject newJsonData) {
        if (null == rmd) {
            LOGGER.error("call without ResourceMetadata from ");
            Metadata.logMeMoslingStacktrace();
            return;
        }

        Conversion c = ConversionDictionary.find(rmd.getResourceName(), connectorName);
        JSONObject newJson = c != null ? c.map(newJsonData) : newJsonData;
        if (null == newJson) {
            return;
        }

        String cn = rmd.getLocalStorageName();
        String pkName = rmd.getPrimaryKey();
        String pkValue = JsonObjectHelper.getField(newJson, pkName);
        String fqn = rmd.getFqn();
        boolean result = true;

        // check if resource has no pk or the new json has no primary key --> create uuid if pk is set and insert
        if (pkName.isEmpty() || pkValue.isEmpty()) {
            String newPkValue = "";
            if (!pkName.isEmpty()) {
                LOGGER.debug("insert generate uuid for {}", fqn);
                newPkValue = UUID.randomUUID().toString();
                JsonObjectHelper.putStringField(newJson, pkName, newPkValue);
            }

            if (null != notification) {
                List<String> ml = new ArrayList<>();

                if (pkName.isEmpty()) {
                    // no primary key given, create notification from list properties
                    if (!rmd.getListProperties().isEmpty()) {
                        rmd.getListProperties().forEach(f -> {
                            ml.add(f);
                            ml.add(newJson.has(f) ? newJson.getString(f) : "#null");
                        });
                    }
                } else {
                    ml.add(pkName);
                    ml.add(newPkValue);
                }

                notification.notify(NotificationEvent.NEW, fqn, JsonHelper.createJsonFromList(ml));
            }
            result = storage.insert(cn, newJson.toString());
        } else {
            // pk is set and exists in the json data
            // check if an entry with this key exists and pk is set
            QueryParameter sel = QueryParameter.newEqual(pkName, pkValue);

            List<String> dl = storage.select(cn, sel);

            // no entry with same pk exists --> insert
            if (dl.isEmpty()) {
                if (null != notification) {
                    notification.notify(NotificationEvent.NEW, fqn,
                            JsonHelper.createJsonFromList(Arrays.asList(pkName, pkValue)));
                }
                result = storage.insert(cn, newJson.toString());
            } else {
                // found an existing entry --> update
                LOGGER.log(dl.size() > 1 ? Level.WARN : Level.DEBUG, "update cached #{} data for {}", dl.size(), fqn);
                String updateJson = "";
                boolean mergeNotification = false;

                if (rmd.isMergeBeforeUpdate() && null != rmd.getContentMerger()) {
                    JSONObject n = new JSONObject(dl.get(0));
                    if (rmd.getContentMerger().mergeJson(rmd, newJson, n)) {
                        updateJson = n.toString();
                        mergeNotification = rmd.getContentMerger().getNotification() != null;
                    }
                } else {
                    updateJson = newJson.toString();
                }

                // updateJson is empty if merge creates no new data
                if (!updateJson.isEmpty()) {
                    // notify if no internal notification was sent
                    if (!mergeNotification && null != notification) {
                        notification.notify(NotificationEvent.UPDATE, fqn,
                                JsonHelper.createJsonFromList(Arrays.asList(pkName, pkValue)));
                    }

                    result = storage.update(cn, updateJson, sel);
                }
            }
        }

        LOGGER.log(result ? Level.DEBUG : Level.ERROR, "DataManager.addOrUpdateData result {} ",
                result ? "PASS" : "FAIL");

    }

    public String getDataEntry(ResourceMetadata rmd, String id) {
        if (null == rmd) {
            return "";
        }

        if (rmd.getPrimaryKey().isEmpty()) {
            LOGGER.error("getDataEntry possible with primary for {}.{} only", rmd.getApplication(),
                    rmd.getResourceName());
            return "";
        }

        LOGGER.debug("get cached data entry {} from {}", id, rmd.getLocalStorageName());
        List<String> l = storage.select(rmd.getLocalStorageName(),
                QueryParameter.newEqual(rmd.getPrimaryKey(), id));

        if (!l.isEmpty()) {
            if (l.size() > 1) {
                LOGGER.warn("fetch {} entries for {} where {} = {}, expected exactly one --> use first entry",
                        l.size(), rmd.getLocalStorageName(), rmd.getPrimaryKey(), id);
            }
            return l.get(0);
        }

        // if we reach this position dataManager has no entries empty
        return rmd.getConnector() != null ? rmd.getConnector().fetchEntry(rmd, id) : "";
    }

    public Map<String, Object> getDataResourceMap(String name, String id) {
        ResourceMetadata rmd = Metadata.get(name);
        if (null == rmd) {
            return null;
        }

        Map<String, Object> d = new HashMap<>();

        d.put("resource", rmd);
        if (!id.isEmpty()) {
            d.put("startdata", getDataEntry(rmd, id));
            d.put("identifier", id);
        }

        return d;
    }

    /**
     * @param rmd Resource Metadata for the resource to delete
     * @param id  delete all value with primaryKey has this value (normally exactly on entry)
     */
    public void deleteDataEntry(ResourceMetadata rmd, String id) {
        if (null == rmd) {
            return;
        }

        if (rmd.getPrimaryKey().isEmpty()) {
            LOGGER.error("deleteDataEntry possible with primary key for {}.{} only", rmd.getApplication(),
                    rmd.getResourceName());
            return;
        }

        if (null != notification) {
            notification.notify(NotificationEvent.REMOVE, rmd.getFqn(),
                    JsonHelper.createJsonFromList(Arrays.asList("pkvalue", id)));
        }

        boolean b = storage.delete(rmd.getLocalStorageName(), QueryParameter.newEqual(rmd.getPrimaryKey(), id),
                false);

        LOGGER.log(b ? Level.DEBUG : Level.ERROR, "   operation result {} ", b ? "PASS" : "FAIL");

        if (b) {
            refreshCountData(rmd);
        }

        // start remote deletion if allowed and possible
        if (rmd.isRemoteWritable()) {
            rmd.getConnector().deleteEntry(rmd, id);
        }
    }

    public String getDataList(ResourceMetadata rmd, QueryParameter qp, DataFormatter outputFormatter) {
        if (null == rmd) {
            return "{}";
        }

        Conversion c = ConversionDictionary.find(rmd.getResourceName(), ConversionDictionary.NODE_DATA);
        List<String> l = fetchDataList(rmd, qp);
        long ms = System.currentTimeMillis();

        // refresh the data count for the resource, otherwise there is no chance to update the badge
        // if data changed by other processes
        refreshCountData(rmd);

        List<JSONObject> lj = new ArrayList<>();
        for (String s : l) {
            lj.add(c != null ? c.unmap(new JSONObject(s)) : new JSONObject(s));
        }

        String res = outputFormatter.format(lj, qp.getSelectList());

        long timeMs = System.currentTimeMillis() - ms;
        LOGGER.info("return {} entries consuming ({}) takes {}", lj.size(),
                StringHelper.byteCountDisplay(res.length()), StringHelper.millisecondsToTimeString(timeMs, false));

        return res;
    }

    // If as objects is set the result is a json array og object each containing the field "value" and "text",
    // Otherwise the result is a simple object with "value":"text" pairs.
    public String getLookupList(ResourceMetadata rmd, String key, String label, boolean asObjects) {
        String myKey = StringHelper.nonNullStr(key);
        String myLabel = StringHelper.nonNullStr(label);

        if (myKey.isEmpty() && myLabel.isEmpty()) {
            LOGGER.error("lookup needs key and value column (both not set)");
            return "{}";
        }

        if (myKey.isEmpty()) {
            myKey = myLabel;
        }

        if (myLabel.isEmpty()) {
            myLabel = myKey;
        }

        List<String> l = fetchDataList(rmd, QueryParameter.newSelection(
                myKey.equalsIgnoreCase(myLabel) ? myKey : String.format("%s,%s", myKey, myLabel)));

        AtomicBoolean b = new AtomicBoolean(true);
        StringBuilder sb = new StringBuilder();
        sb.append(asObjects ? "[" : "{");
        for (String r : l) {
            JSONObject o = new JSONObject(r);
            if (o.has(myKey) && o.has(myLabel)) {
                sb.append(b.getAndSet(false) ? "" : ",");
                if (asObjects) {
                    sb.append(String.format("{\"value\":\"%s\",\"text\":\"%s\"}",
                            o.getString(myKey).replace("\"", "\\\""),
                            o.getString(myLabel).replace("\"", "\\\"")));
                } else {
                    sb.append(String.format("\"%s\":\"%s\"", o.getString(myKey).replace("\"", "\\\""),
                            o.getString(myLabel).replace("\"", "\\\"")));
                }
            } else {
                LOGGER.debug("missing {} and {} values for '{}'", key, label, r);
            }
        }
        sb.append(asObjects ? "]" : "}");

        return sb.toString();
    }

    private List<String> fetchDataList(ResourceMetadata rmd, QueryParameter qp) {
        if (null == rmd) {
            return Collections.emptyList();
        }

        if (rmd.getConnector() != null && !isFetchRunning(rmd) && 0 == storage.count(rmd.getLocalStorageName(),
                null)) {
            startFetchThread(rmd);
        }

        LOGGER.debug("get cached data list for {}", rmd.getLocalStorageName());

        return storage.select(rmd.getLocalStorageName(), qp);
    }

    private boolean isFetchRunning(ResourceMetadata rmd) {
        if (runningFetchLists.containsKey(rmd.getResourceName()) && runningFetchLists
                .get(rmd.getResourceName())
                .isAlive()) {
            LOGGER.info("remote data fetch is running for resource '{}'", rmd.getResourceName());
            return true;
        }

        return false;
    }

    public void waitForFetchThreads() {
        long l = runningFetchLists.size();

        if (l > 0) {
            LOGGER.info(" start waiting for {} running fetches", l);
            while (l > 0) {
                l = runningFetchLists.values().stream().filter(Thread::isAlive).count();
            }
        }
    }

    private void startFetchThread(ResourceMetadata rmd) {
        runningFetchLists.put(rmd.getResourceName(), new RunnableFetchlist(rmd, this).start());
    }
}
