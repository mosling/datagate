package me.mosling.datagate.core;

import lombok.Getter;

public class GeneralException
        extends Exception {
    @Getter
    private final int status;
    @Getter
    private final boolean asJson;

    public GeneralException(int status, boolean asJson, String message) {
        super(message);
        this.asJson = asJson;
        this.status = status;
    }
}
