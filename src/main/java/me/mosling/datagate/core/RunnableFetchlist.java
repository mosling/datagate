package me.mosling.datagate.core;

import me.mosling.httpfixture.common.StringHelper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class RunnableFetchlist
        implements Runnable {
    private static final Logger LOGGER = LogManager.getLogger(RunnableFetchlist.class);

    private Thread t;
    private final ResourceMetadata resourceMetadata;
    private final DataManager dataManager;

    RunnableFetchlist(ResourceMetadata rmd, DataManager dm) {
        dataManager = dm;
        resourceMetadata = rmd;
        resourceMetadata.setCountData(-1);
    }

    @Override
    public void run() {
        long timeMillis = System.currentTimeMillis();
        LOGGER.info("Starting fetching list for resource '{}'", resourceMetadata.getResourceName());
        resourceMetadata.getConnector().fetchList(resourceMetadata);
        if (null != dataManager) {
            dataManager.refreshCountData(resourceMetadata);
        }

        LOGGER.info("Ready fetching {} entries from resource '{}' takes {} ", resourceMetadata.getCountData(),
                resourceMetadata.getResourceName(),
                StringHelper.millisecondsToTimeString(System.currentTimeMillis() - timeMillis, true));
    }

    Thread start() {
        if (t == null) {
            t = new Thread(this, resourceMetadata.getResourceName());
            t.start();
        }

        return t;
    }
}
