package me.mosling.datagate.core;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Getter;
import lombok.Setter;
import me.mosling.datagate.common.Paths;
import me.mosling.datagate.common.Version;
import me.mosling.datagate.connector.Connector;
import me.mosling.datagate.conversion.ConversionDictionary;
import me.mosling.datagate.fixture.Fixture;
import me.mosling.datagate.notification.Notification;
import me.mosling.datagate.parser.QueryParameter;
import me.mosling.datagate.storage.JsonStorage;
import me.mosling.httpfixture.common.CommonHelper;
import me.mosling.httpfixture.common.StringHelper;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.util.*;
import java.util.stream.Collectors;

public class Metadata {
    private static final Logger LOGGER = LogManager.getLogger(Metadata.class);
    private static final Level always = Level.getLevel("ALWAYS");

    @Getter
    @Setter
    private static String serviceName = "";
    @Getter
    @Setter
    private static Map<String, ResourceMetadata> entryMapping = new TreeMap<>();
    @Getter
    private static boolean developmentMode = false;
    @Getter
    @Setter
    private static String configurationFolder = "config";
    @Getter
    @Setter
    private static JsonStorage metadataStorage;
    @Getter
    @Setter
    private static Map<String, String> pathMapping = Paths.getDefaultPathMapping();

    private static final ObjectMapper om = new ObjectMapper();

    private static final String METADATA_RESOURCE = "md_metadata";

    private Metadata() {
        // functional class
    }

    public static void setDevelopmentMode(boolean devMode) {
        developmentMode = devMode;
        if (devMode) {
            LOGGER.log(always, getHeader("DEVELOPMENT MODE"));
        }
    }

    public static String getHeader(String header) {
        String tmpHeader = null != header && !header.isEmpty() ? " " + header + " " : "";
        String pp = "==============================";
        int l = tmpHeader.length();
        int h = (pp.length() * 2 - l) / 2;
        int b = h + (60 - 2 * h - l);

        return String.format("%s%s%s", pp.substring(0, b), tmpHeader, pp.substring(0, h));
    }

    public static List<String> getSortedResourceNames() {
        List<ResourceMetadata> l = new ArrayList<>(entryMapping.values());

        Collections.sort(l);

        return l.stream().filter(r -> r.getOrder() >= 0).map(ResourceMetadata::getResourceName).collect(Collectors.toList());
    }

    private static String getResourceJson(ResourceMetadata rmd) {
        try {
            return om.writeValueAsString(rmd);
        } catch (JsonProcessingException e) {
            LOGGER.error(e);
        }

        return rmd.toJson();
    }

    public static List<String> getSummary(String applist, boolean verboseResource) {
        List<String> l = new ArrayList<>();

        l.add(getHeader("RESOURCES"));

        Metadata
                .getEntryMapping()
                .forEach((k, v) -> l.add((verboseResource ? getResourceJson(v) : v.toJson())
                        .replace("\\n", "")
                        .replaceAll("[ ]+", " ")));

        l.add(getHeader("TRANSFORMATIONS"));
        if (ConversionDictionary.size() > 0) {
            l.addAll(ConversionDictionary.toStringList());
        } else {
            l.add("    no transformation registered.");
        }

        if (!applist.isEmpty()) {
            l.add(getHeader("VERSIONS"));
            for (String s : applist.split(",")) {
                if (!s.trim().isEmpty()) {
                    l.add(String.format("%s - %s", s.toUpperCase(), Version.info(s, "version")));
                }
            }
        }

        l.add(getHeader(""));

        return l;
    }

    public static String pathFor(String p) {
        return pathMapping.getOrDefault(p, String.format("<no path set for template '%s'>", p));
    }

    /**
     * Start to load a given configuaration name:
     * 1. load from storage with configuration equal application name, each resource is on entry
     * 2. load configuration file
     *
     * @param configurationName the given application name can includes
     * @param connector         the used Connector or null
     *
     * @return true if the configuration was loaded successfully
     */
    public static boolean loadConfiguration(String configurationName, Connector connector) {
        boolean success = false;

        if (null != metadataStorage && !configurationName.endsWith(".json")) {
            if (!(success = readConfigFromStorage(configurationName, connector))) {
                LOGGER.error("Can't read configuration '{}' from storage '{}'", configurationName,
                        metadataStorage.getClass().getSimpleName());
            }
        }

        if (!success) {
            String cfn = configurationName.endsWith(".json") ? configurationName : configurationName + ".json";
            if (!(success = Metadata.readConfigFromFile(cfn, connector))) {
                LOGGER.error("Can't read configuration file '{}'", cfn);
            }
        }

        return success;
    }

    /**
     * Read a list of ResourceMetdata from a given file.
     *
     * @param fname the name of the file which contains the resource configurations
     *
     * @return true if the files
     */
    private static boolean readConfigFromFile(String fname, Connector connector) {
        String configName = configurationFolder + (configurationFolder.endsWith("/") ? "" : "/") + fname;

        if (!configName.endsWith(".json")) {
            LOGGER.warn("reading '{}' supports json configuration only --> add '.json' extension.", fname);
            configName += ".json";
        }

        try (InputStream s = CommonHelper.getInputStreamFromName(configName, true)) {
            if (null == s) {
                throw new IOException();
            }

            ObjectMapper mapper = new ObjectMapper();
            TypeReference<Map<String, ResourceMetadata>> typeRef = new TypeReference<Map<String, ResourceMetadata>>() {
            };

            Map<String, ResourceMetadata> tmpEntryMapping = mapper.readValue(s, typeRef);

            tmpEntryMapping.forEach((k, v) -> {
                v.setResourceName(k);
                v.updateEntryFields();
                v.setConnector(connector);
                v.setCanBeUpdated(false);
            });

            entryMapping.putAll(tmpEntryMapping);
        } catch (JsonParseException | JsonMappingException e) {
            LOGGER.fatal("can't parse description file '{}'", configName);
            LOGGER.fatal(e);
            return false;
        } catch (IOException e) {
            LOGGER.fatal("can't read metadata description file '{}'", configName);
            return false;
        }

        return true;
    }

    public static boolean readConfigFromStorage(String appName, Connector connector) {
        if (null != metadataStorage) {
            return updateResourceFromStorage(getMetadataFilterFor(appName, ""), true, connector);
        }

        LOGGER.warn("Please call setMetadataStorage(!null) before calling readConfigFromStorage().");
        return false;
    }

    public static boolean updateResourceFromStorage(QueryParameter qp, boolean allowNew, Connector connector) {
        LOGGER.info("Try reading resources from database where ({})", qp.filterToString());

        List<String> l = metadataStorage.select(METADATA_RESOURCE, qp);

        for (String e : l) {
            ResourceMetadata rmd = ResourceMetadata.createFromJson(e);
            rmd.setCanBeUpdated(true);
            rmd.setConnector(connector);

            ResourceMetadata ermd = entryMapping.get(rmd.getResourceName());
            boolean updateExisting = ermd != null;

            if (updateExisting) {
                rmd.setConnector(ermd.getConnector());
                rmd.setLocalStorageName(ermd.getLocalStorageName());
                rmd.setContentMerger(ermd.getContentMerger());
            }

            if (allowNew || updateExisting) {
                entryMapping.put(rmd.getResourceName(), rmd);
            }
        }

        if (l.isEmpty()) {
            LOGGER.info("NO configuration for {}({}) found in {} ", METADATA_RESOURCE, qp.getOriginalFilter(),
                    metadataStorage.getClass().getName());
        }

        return !l.isEmpty();
    }

    public static void put(ResourceMetadata rmd) {
        if (entryMapping.containsKey(rmd.getResourceName())) {
            LOGGER.warn("overwrite existing entry '{}'", rmd.getResourceName());
        }
        entryMapping.put(rmd.getResourceName(), rmd);
    }

    public static ResourceMetadata get(String md, boolean withLogging) {
        if (md != null && entryMapping.containsKey(md)) {
            return entryMapping.get(md);
        }

        if (withLogging) {
            LOGGER.error("can't find metadata entry '{}'", null == md ? "null-value" : md);
            logMeMoslingStacktrace();
        }

        return null;
    }

    public static ResourceMetadata getResourceForLocalStorageName(String cn) {
        for (ResourceMetadata rmd : entryMapping.values()) {
            if (cn.equalsIgnoreCase(rmd.getLocalStorageName())) {
                return rmd;
            }
        }

        return null;
    }

    public static void logMeMoslingStacktrace() {
        StackTraceElement[] st = Thread.currentThread().getStackTrace();

        // ignore the first entry [0] which is the call of the logging method
        for (int i = 1; i < st.length; ++i) {
            StackTraceElement e = st[i];
            if (e.getClassName().startsWith("me.mosling")) {
                LOGGER.error("{}::{} ({})", e.getClassName(), e.getMethodName(), e.getLineNumber());
            } else {
                break;
            }
        }
    }

    public static ResourceMetadata get(String md) {
        return get(md, true);
    }

    public static String getFirstEntry() {
        return entryMapping.keySet().iterator().next();
    }

    public static QueryParameter getMetadataFilterFor(String appName, String resourceName) {
        String appFilter = "";
        String resFilter = "";
        String filter = "";

        if (!StringHelper.nonNullStr(appName).isEmpty()) {
            appFilter = String.format("%s.eq.%s", ResourceMetadata.APPLICATION_NAME, appName);
        }

        if (!StringHelper.nonNullStr(resourceName).isEmpty()) {
            resFilter = String.format("%s.eq.%s", ResourceMetadata.RESOURCE_NAME, resourceName);
        }

        if (appFilter.isEmpty() && resFilter.isEmpty()) {
            LOGGER.warn("empty application and resource returns all metadata entries");
        } else if (!appFilter.isEmpty() && !resFilter.isEmpty()) {
            filter = "and(" + appFilter + "," + resFilter + ")";
        } else {
            filter = resFilter.isEmpty() ? appFilter : resFilter;
        }

        return new QueryParameter(filter, "", "", "");
    }

    public static void updateAllContentMergerNotification(Notification notification) {
        entryMapping.forEach((k, v) -> {
            if (v.getContentMerger() != null) {
                v.getContentMerger().setNotification(notification);
            }
        });
    }

    public static void initializeCountDataFromStorage(JsonStorage jsonStorage) {
        entryMapping.forEach((k, v) -> v.setCountData(jsonStorage.count(v.getLocalStorageName(), null)));
    }

    public static void updateAllFixtureStorageNames(JsonStorage jdb) {
        if (null != jdb) {
            for (Map.Entry<String, ResourceMetadata> e : entryMapping.entrySet()) {
                ResourceMetadata rmd = e.getValue();
                if (rmd.getConnector() != null) {
                    Fixture f = rmd.getConnector().getFixture();
                    if (f != null && !StringHelper.nonNullStr(f.getName()).isEmpty()) {
                        String s = rmd.getLocalStorageName();
                        rmd.setLocalStorageName(jdb.updateStorageNameWithFixture(s, f.getName()));
                        if (!s.equalsIgnoreCase(rmd.getLocalStorageName())) {
                            LOGGER.info("[{}] update local storage name '{}' --> '{}'", e.getKey(), s,
                                    rmd.getLocalStorageName());
                        }
                    }

                }
            }
        }
    }
}
