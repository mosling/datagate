package me.mosling.datagate.core;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;
import me.mosling.datagate.common.JsonObjectHelper;
import me.mosling.datagate.connector.Connector;
import me.mosling.datagate.conversion.Conversion;
import me.mosling.datagate.conversion.ConversionDictionary;
import me.mosling.datagate.merger.Merger;
import me.mosling.datagate.merger.MergerJsonDefault;
import me.mosling.datagate.paging.ResponseStrategy;
import me.mosling.datagate.paging.ResponseStrategyDictionary;
import me.mosling.httpfixture.common.CommonHelper;
import me.mosling.httpfixture.common.JsonHelper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class ResourceMetadata
        implements Comparable<ResourceMetadata> {
    private static final Logger LOGGER = LogManager.getLogger(ResourceMetadata.class);

    private static final Pattern lookupReplacePattern = Pattern.compile("\"LOOKUP\\(([^)]*)\\)\"");

    public static final String APPLICATION_NAME = "application";
    public static final String RESOURCE_NAME = "resourceName";

    private static final String OPTIONS_SOURCE = "optionsSource";
    private static final String SCHEMA_SOURCE = "schemaSource";
    private static final String VIEW_SOURCE = "viewSource";

    // @formatter:off
    // ATTENTION:: if you add a new field --> add it to the setFieldsFromJson also !!
    @Getter @Setter private String       application         = "";
    @Getter @Setter private boolean      dropBeforeRefresh   = true;
    @Getter @Setter private boolean      editOnly            = false;
    @Getter @Setter private Integer      groupBy             = -1;
    @Getter @Setter private String       jsonReference       = "";
    @Getter @Setter private boolean      listOnly            = false;
    @Getter @Setter private List<String> listProperties      = new ArrayList<>();
    @Getter @Setter private List<String> listPropertiesHtml  = new ArrayList<>();
    @Getter @Setter private List<String> listPropertiesTitle = new ArrayList<>();
    @Getter @Setter private String       localStorageName    = "";
    @Getter @Setter private boolean      mergeBeforeUpdate   = false;
    @Getter @Setter private Set<String>  needRole            = new TreeSet<>();
    @Getter @Setter private String       optionsSource       = "";
    @Getter @Setter private Integer      order               = 0;
    @Getter @Setter private String       pagingStrategy      = "";
    @Getter @Setter private String       primaryKey          = "";
    @Getter @Setter private String       primaryKeyType      = "";
            @Setter private boolean      remoteWritable      = true;
    @Getter @Setter private String       resourceName        = "";
    @Getter @Setter private String       resourcePath        = "";
    @Getter @Setter private String       schemaSource        = "";
    @Getter @Setter private Integer      sortBy              = 0;
    @Getter @Setter private String       title               = "";
    @Getter @Setter private String       tooltip             = "";
    @Getter @Setter private String       urlTarget           = "";
    @Getter @Setter private String       viewSource          = "";

    @Getter @Setter @JsonIgnore private Merger    contentMerger    = null;
    @Getter @Setter @JsonIgnore private boolean   canBeUpdated     = true;
    @Getter @Setter @JsonIgnore private boolean   hasDetailsView   = false;
    @Getter         @JsonIgnore private Connector connector        = null;
    @Getter @Setter @JsonIgnore private int       countData        = 0;
    // @formatter:on

    public static ResourceMetadata createFromJson(String json) {
        ResourceMetadata rmd = new ResourceMetadata();
        setFieldsFromJson(rmd, json);
        rmd.updateEntryFields();
        return rmd;
    }

    private static void setFieldsFromJson(ResourceMetadata rmd, String json) {
        JSONObject o = new JSONObject(json);

        // @formatter:off
        if (o.has(APPLICATION_NAME)) rmd.setApplication(o.getString(APPLICATION_NAME));
        if (o.has("dropBeforeRefresh")) rmd.setDropBeforeRefresh(o.getBoolean("dropBeforeRefresh"));
        if (o.has("editOnly")) rmd.setEditOnly(o.getBoolean("editOnly"));
        if (o.has("groupBy")) rmd.setGroupBy(o.getInt("groupBy"));
        if (o.has("listOnly")) rmd.setListOnly(o.getBoolean("listOnly"));
        if (o.has("listProperties"))
        {
            rmd.getListProperties().clear();
            o.getJSONArray("listProperties").toList().forEach(s -> rmd.getListProperties().add(s.toString()));
        }
        if (o.has("listPropertiesHtml"))
        {
            rmd.getListPropertiesHtml().clear();
            o.getJSONArray("listPropertiesHtml").toList().forEach(s -> rmd.getListPropertiesHtml().add(s.toString()));
        }
        if (o.has("listPropertiesTitle"))
        {
            rmd.getListPropertiesTitle().clear();
            o.getJSONArray("listPropertiesTitle").toList().forEach(s -> rmd.getListPropertiesTitle().add(s.toString()));
        }
        if (o.has("localStorageName")) rmd.setLocalStorageName(o.getString("localStorageName"));
        if (o.has("mergeBeforeUpdate")) rmd.setMergeBeforeUpdate(o.getBoolean("mergeBeforeUpdate"));
        if (o.has("needRole"))
        {
            rmd.getNeedRole().clear();
            o.getJSONArray("needRole").toList().forEach(s -> rmd.getNeedRole().add(s.toString()));
        }
        if (o.has(OPTIONS_SOURCE)) rmd.setOptionsSource(o.get(OPTIONS_SOURCE).toString());
        if (o.has("order")) rmd.setOrder(o.getInt("order"));
        if (o.has("pagingStrategy")) rmd.setPagingStrategy(o.getString("pagingStrategy"));
        if (o.has("primaryKey")) rmd.setPrimaryKey(o.getString("primaryKey"));
        if (o.has("primaryKeyType")) rmd.setPrimaryKey(o.getString("primaryKeyType"));
        if (o.has("remoteWritable")) rmd.setRemoteWritable(o.getBoolean("remoteWritable"));
        if (o.has(RESOURCE_NAME))
        {
            String rn = o.getString(RESOURCE_NAME);
            if (rmd.getResourceName().isEmpty() || rn.equals(rmd.getResourceName()))
            {
                rmd.setResourceName(rn);
            } else
            {
                LOGGER.warn("[{}] ignore different resource name during setFieldsFromJson '{}' --> field can be removed from json file", rmd.getResourceName(), rn);
            }
        }
        if (o.has("resourcePath")) rmd.setResourcePath(o.getString("resourcePath"));
        if (o.has(SCHEMA_SOURCE)) rmd.setSchemaSource(o.get(SCHEMA_SOURCE).toString());
        if (o.has("sortBy")) rmd.setSortBy(o.getInt("sortBy"));
        if (o.has("title")) rmd.setTitle(o.getString("title"));
        if (o.has("tooltip")) rmd.setTooltip(o.getString("tooltip"));
        if (o.has("urlTarget")) rmd.setUrlTarget(o.getString("urlTarget"));
        if (o.has(VIEW_SOURCE)) rmd.setViewSource(o.get(VIEW_SOURCE).toString());
        // @formatter:on
    }

    /**
     * Return true if the resource is allowed to write data to the remote connection,
     * this means the resource has an active connection and the flag is set to true.
     *
     * @return a boolean value
     */
    public boolean isRemoteWritable() {
        return remoteWritable && (null != connector);
    }

    public void setConnector(Connector connector) {
        this.connector = connector;

        if (this.connector != null) {
            // precheck existing paging parameter
            if (!pagingStrategy.isEmpty()) {
                ResponseStrategy rps = ResponseStrategyDictionary.findResponsePagingStrategy(pagingStrategy);
                if (null != rps) {
                    rps.checkParameter(this, pagingStrategy.split(","));
                } else {
                    LOGGER.error("paging strategy is '{}' but no implementation exists", pagingStrategy);
                }
            }
        }
    }

    public List<String> getConversionList() {
        Map<String, Conversion> m = ConversionDictionary.resourceEntries(getResourceName());

        return m
                .entrySet()
                .stream()
                .map(e -> String.format("%s:: %s", e.getKey(), e.getValue().toString()))
                .collect(Collectors.toList());
    }

    public void updateEntryFields() {
        if (!jsonReference.isEmpty()) {
            InputStream is = CommonHelper.getInputStreamFromName(jsonReference, true, true);
            if (null != is) {
                LOGGER.info("[{}] read resource from file '{}'", resourceName, jsonReference);
                String json = new BufferedReader(new InputStreamReader(is))
                        .lines()
                        .collect(Collectors.joining("\n"));

                setFieldsFromJson(this, json);
            } else {
                LOGGER.error("[{}] can't find referenced json resource '{}'", resourceName, jsonReference);
            }

            // do this action only once
            jsonReference = "";
        }

        if (null == listProperties) {
            listProperties = Collections.emptyList();
        }

        if (!listPropertiesTitle.isEmpty() && listPropertiesTitle.size() != listProperties.size()) {
            LOGGER.warn("listPropertiesTitle is set but has different length to listProperties -- use listProperties instead");
            listPropertiesTitle.clear();
        }

        if (listPropertiesTitle.isEmpty()) {
            listPropertiesTitle.addAll(listProperties);
        }

        // set default content merger if needed
        if (mergeBeforeUpdate && null == contentMerger) {
            contentMerger = new MergerJsonDefault();
        }

        if (!primaryKeyType.isEmpty() && !"numeric".equals(primaryKeyType)) {
            LOGGER.error("[{}] primaryKeyType must be 'numeric' but is '{}' --> unset value", resourceName,
                    primaryKeyType);
            primaryKeyType = "";
        }

        // set cache_name if not preset
        if (localStorageName.isEmpty()) {
            localStorageName = resourceName;
            if (!application.isEmpty()) {
                localStorageName = application + "_" + localStorageName;
            }
            localStorageName = localStorageName.replace("-", "_").toLowerCase();
            String regexp = "[a-zA-Z][_a-zA-Z]*";
            boolean b = Pattern.matches(regexp, localStorageName);

            if (!b) {
                LOGGER.error("[{}] set dataManager name = '{}' doesn't match the RE /{}/", resourceName, localStorageName,
                        regexp);
            }
        }

        // read possible external resources
        String resjson = resourceName + ".json";
        schemaSource = setFileResource(schemaSource, "schema", "schema/" + resjson);
        optionsSource = setFileResource(optionsSource, "alpaca-option", "option/" + resjson);
        viewSource = setFileResource(viewSource, "alpaca-view", "view/" + resjson);

        updateOptionsJson();

        hasDetailsView = !primaryKey.isEmpty() && !schemaSource.isEmpty() && !optionsSource.isEmpty();
    }

    /**
     * remove the outer brackets, option will be embedded and replace macros,
     * Attention: after this the optionsSource can't be parsed as JSON from now it is javascript !!
     */
    private void updateOptionsJson() {
        if (!optionsSource.isEmpty()) {
            try {
                JSONObject jo = new JSONObject(optionsSource);
                JsonObjectHelper.updateOptionsJsonLookup(jo);
                optionsSource = jo.toString();

                Matcher matcher = lookupReplacePattern.matcher(optionsSource);
                StringBuffer sb = new StringBuffer();
                while (matcher.find()) {
                    String f = String.format("function (callback) { callback( getLookupData(%s) ) }",
                            matcher.group(1));
                    matcher.appendReplacement(sb, f);
                }
                matcher.appendTail(sb);
                optionsSource = sb.toString();
            } catch (JSONException ex) {
                LOGGER.error("[{}] options not a valid json document: {}", resourceName, ex.getMessage());
                optionsSource = "";
            }
        }

        // remove the enclosing brackets
        if (!optionsSource.isEmpty()) {
            String opts = optionsSource.trim();
            if (opts.startsWith("{")) {
                optionsSource = opts.substring(1, opts.length() - 1);
            }
        }
    }

    private String setFileResource(String src, String type, String fn) {
        if (src.isEmpty()) {
            String resLoc = Metadata.getConfigurationFolder() + (Metadata.getConfigurationFolder().endsWith("/") ?
                    "" :
                    "/") + fn;
            InputStream is = CommonHelper.getInputStreamFromName(resLoc, true, true);
            if (is != null) {
                LOGGER.info("[{}.{}] found source '{}'", resourceName, type, resLoc);
                return new BufferedReader(new InputStreamReader(is)).lines().collect(Collectors.joining("\n"));
            }
            LOGGER.debug("[{}.{}] source NOT exists '{}'.", resourceName, type, resLoc);
            return "";
        }

        int l = src.length();
        LOGGER.debug("[{}.{}] is set '{}{}'", resourceName, type, src.substring(0, Math.min(l, 50)),
                l < 53 ? src.substring(50, l) : " ...");

        return src;
    }

    public String getListPropertiesAsString() {
        return listProperties.stream().map(s -> s.replace(".", ">")).collect(Collectors.joining(","));
    }

    /**
     * create a full qualified name for the resource, based on the information from the resource
     * (application-name | 'common')[.fixture-name].resource-name
     *
     * @return string with the FQN for the resource
     */
    public String getFqn() {
        String fs = connector != null ? connector.getFixtureName() : "";
        return String.format("%s%s.%s", application.isEmpty() ? "common" : application, fs.isEmpty() ? "" : "." + fs,
                resourceName);
    }

    /**
     * A resource is locally managed if there is no connector assigned or the resource can't be written (i.e.
     * remoteWritable is false)
     *
     * @return true if manged locally only
     */
    public boolean isLocallyManagedOnly() {
        return null == connector || !remoteWritable;
    }

    public String toJson() {
        List<String> sb = new ArrayList<>();
        String nullStr = "#null";

        sb.add("name");
        sb.add(resourceName);
        sb.add("storage-name");
        sb.add(localStorageName);
        if (!needRole.isEmpty()) {
            sb.add("access-roles");
            sb.add("[");
            sb.addAll(needRole);
            sb.add("]");
        }
        sb.add("connector");
        if (connector != null) {
            sb.add("{");
            sb.add("name");
            sb.add(connector.getClass().getSimpleName());
            sb.add("}");
        } else {
            sb.add(nullStr);
        }

        sb.add("merger");
        if (contentMerger != null) {
            sb.add("{");
            sb.add("name");
            sb.add(contentMerger.getClass().getSimpleName());
            sb.add("notifaction");
            sb.add(contentMerger.getNotification() == null ?
                    nullStr :
                    contentMerger.getNotification().getClass().getSimpleName());
            sb.add("}");
        } else {
            sb.add(nullStr);
        }

        return JsonHelper.createJsonFromList(sb);
    }

    @Override
    public int compareTo(ResourceMetadata o) {
        if (this == o) {
            return 0;
        }

        if (!this.getOrder().equals(o.getOrder())) {
            return this.getOrder() - o.getOrder();
        }

        return this.getTitle().compareTo(o.getTitle());
    }
}
