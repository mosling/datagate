package me.mosling.datagate.connector;

import me.mosling.datagate.core.DataManager;
import me.mosling.datagate.core.ResourceMetadata;
import me.mosling.datagate.fixture.Fixture;
import me.mosling.httpfixture.common.StringHelper;

public interface Connector {
    String getName();

    Fixture getFixture();

    DataManager getDataManager();

    void fetchList(ResourceMetadata rmd);

    String fetchEntry(ResourceMetadata rmd, String identifier);

    void deleteEntry(ResourceMetadata rmd, String identifier);

    String createEntry(ResourceMetadata rmd, String identifier, String body);

    default String getFixtureName() {
        return getFixture() != null ? StringHelper.nonNullStr(getFixture().getName()) : "";
    }

}