package me.mosling.datagate.connector;

import lombok.Getter;
import me.mosling.datagate.conversion.Conversion;
import me.mosling.datagate.conversion.ConversionDictionary;
import me.mosling.datagate.core.DataManager;
import me.mosling.datagate.core.Metadata;
import me.mosling.datagate.core.ResourceMetadata;
import me.mosling.datagate.fixture.Fixture;
import me.mosling.datagate.merger.MergerJsonDefault;
import me.mosling.datagate.notification.NotificationLogger;
import me.mosling.datagate.paging.ResponseStrategy;
import me.mosling.datagate.paging.ResponseStrategyDictionary;
import me.mosling.httpfixture.common.StringHelper;
import me.mosling.httpfixture.fixture.ResponseData;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;

public class ConnectorRest
        implements Connector {
    private static final Logger LOGGER = LogManager.getLogger(ConnectorRest.class);

    @Getter
    private final Fixture fixture;

    @Getter
    private final DataManager dataManager;

    public ConnectorRest(DataManager dataManager, Fixture fixture) {
        this.fixture = fixture;
        this.dataManager = dataManager;

        ResponseStrategyDictionary.addSystemPagingStrategies();

        LOGGER.info("using fixture {} .. ready", fixture.getClass().getSimpleName());
    }

    @Override
    public String getName() {
        return "REST";
    }

    @Override
    public String getFixtureName() {
        return fixture != null ? fixture.getName() : "";
    }

    @Override
    public String fetchEntry(ResourceMetadata rmd, String identifier) {
        // if we reach this position dataManager is empty
        if (!rmd.getResourcePath().isEmpty() && fixture.isConnected()) {
            LOGGER.info("request entry {} for {}", identifier, rmd.getResourceName());
            ResponseData rd = fixture.doGetRequest(rmd.getResourceName(),
                    constructUrlPath(rmd.getResourcePath(), identifier));
            if (rd.getStatus() == 200) {
                dataManager.addOrUpdateData(rmd, getName(), new JSONObject(rd.getResponseContent()));
                return rd.getResponseContent();
            }
        }

        return "";
    }

    @Override
    public void deleteEntry(ResourceMetadata rmd, String identifier) {
        if (rmd.isRemoteWritable() && !rmd.getResourcePath().isEmpty() && fixture.isConnected()) {
            LOGGER.info("DELETE remote '{}'({})", rmd.getResourceName(), identifier);

            String a = constructUrlPath(rmd.getResourcePath(), identifier);
            ResponseData rd = fixture.doWriteRequest(rmd.getResourceName(), "DELETE", a, "");
            if (rd.getStatus() != 204) {
                LOGGER.error("Error DELETE entry '{}' ", a);
                rd.showResponse(true, Level.ERROR);
            }
        }
    }

    @Override
    public String createEntry(ResourceMetadata rmd, String identifier, String body) {
        if (rmd.isRemoteWritable() && !rmd.getResourcePath().isEmpty() && fixture.isConnected()) {
            boolean isCreate = identifier.isEmpty();
            String outgoingData = body;
            Conversion c = ConversionDictionary.find(rmd.getResourceName(), getName());

            if (c != null) {
                outgoingData = c.unmap(new JSONObject(body)).toString();
            }

            LOGGER.info("{} remote '{}'({})", isCreate ? "CREATE" : "UPDATE", rmd.getResourceName(), identifier);

            @SuppressWarnings("squid:S1075") String a = constructUrlPath(rmd.getResourcePath(), identifier);
            ResponseData rd = fixture.doWriteRequest(rmd.getResourceName(), isCreate ? "POST" : "PUT", a,
                    outgoingData);

            if ((!isCreate && rd.getStatus() == 200) || (isCreate && rd.getStatus() == 201)) {
                MergerJsonDefault m = new MergerJsonDefault();
                if (Metadata.isDevelopmentMode()) {
                    m.setNotification(new NotificationLogger());
                }
                // merge the existing data (can contains additional meta data)  with the result from the server
                JSONObject src = new JSONObject(rd.getResponseContent());
                JSONObject trg = new JSONObject(body);
                m.mergeJson(rmd, src, trg);
                // and update the data
                dataManager.addOrUpdateData(rmd, getName(), trg);
            } else {
                rd.showResponse(true, Level.ERROR);
                return rd.getResponseContent();
            }
        }

        return "{}";
    }

    @Override
    public void fetchList(ResourceMetadata rmd) {
        if (rmd.getResourcePath().isEmpty()) {
            LOGGER.warn("fetch list for {} need a resource path", rmd.getResourceName());
            return;
        }

        if (StringHelper.nonNullStr(rmd.getPagingStrategy()).isEmpty()) {
            LOGGER.warn("fetch list for {} not possible without paging strategy ", rmd.getResourceName());
            return;
        }

        String[] parameter = rmd.getPagingStrategy().split(",", -1);

        ResponseStrategy rps = ResponseStrategyDictionary.findResponsePagingStrategy(parameter[0]);

        if (null == rps) {
            LOGGER.error("no registered response paging strategy for {}", rmd.getPagingStrategy());
            return;
        }

        if (rps.checkParameter(rmd, parameter)) {
            rps.fetchData(this, rmd, parameter);
        }
    }

    /**
     * Construct a regular URL from this two strings.
     *
     * @param x the base path from the resource, must be a regular url path
     * @param y the resource identifier, this must be url encoded
     *
     * @return the resulting url path
     */
    private String constructUrlPath(String x, String y) {
        if (y == null || y.isEmpty()) {
            return x;
        }

        return String.format("%s/%s", x, StringHelper.encodeUrl(y));
    }

}
