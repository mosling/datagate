package me.mosling.datagate.parser;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

@Accessors(fluent = true)
public class QueryFilterEntry {
    private static final Logger LOGGER = LogManager.getLogger(QueryFilterEntry.class);

    @Getter
    @Setter
    boolean negate = false;
    @Getter
    @Setter
    Boolean stringInfo = null;
    @Getter
    @Setter
    String lhs = "";
    @Getter
    @Setter
    String operator = "";
    @Getter
    @Setter
    String rhs = "";
    @Getter
    @Setter
    String rhs2 = "";

    public boolean isOperator() {
        return lhs.isEmpty() && rhs.isEmpty();
    }

    @Override
    @SuppressWarnings("squid:S3358")
    public String toString() {
        if (lhs.isEmpty() && rhs.isEmpty()) {
            return operator;
        }
        return String.format("%s%s %s %s%s%s", negate ? "not " : "", lhs, operator, rhs,
                rhs2.isEmpty() ? "" : " .. " + rhs2,
                (null != stringInfo ? (stringInfo ? " as string" : " as number") : ""));
    }
}
