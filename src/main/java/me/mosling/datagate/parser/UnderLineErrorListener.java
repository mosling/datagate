package me.mosling.datagate.parser;

import lombok.Getter;
import me.mosling.datagate.grammar.UrlQueryFilterLexer;
import me.mosling.datagate.grammar.UrlQueryFilterParser;
import org.antlr.v4.runtime.*;

import java.util.ArrayList;
import java.util.List;

public class UnderLineErrorListener
        extends BaseErrorListener {
    @Getter
    private final List<ErrorEntry> errors = new ArrayList<>();

    @Override
    public void syntaxError(Recognizer<?, ?> recognizer, Object offendingSymbol, int line, int charPositionInLine,
                            String msg, RecognitionException e) {
        ErrorEntry ee = new ErrorEntry();
        errors.add(ee);
        ee.setMsg("line " + line + ":" + charPositionInLine + " " + msg);
        underLineError(ee, recognizer, (Token) offendingSymbol, line);
    }

    private void underLineError(ErrorEntry ee, Recognizer recognizer, Token offendingToken, int line) {
        if (recognizer instanceof UrlQueryFilterParser) {
            UrlQueryFilterParser p = (UrlQueryFilterParser) recognizer;
            CommonTokenStream tokens = (CommonTokenStream) p.getInputStream();
            String input = tokens.getTokenSource().getInputStream().toString();
            String[] lines = input.split("\n");
            String erroLine = lines[line - 1];

            ee.setSource("parser");
            ee.setLine(erroLine);
            ee.setStart(offendingToken.getStartIndex());
            ee.setStop(offendingToken.getStopIndex());
        } else if (recognizer instanceof UrlQueryFilterLexer) {
            UrlQueryFilterLexer l = (UrlQueryFilterLexer) recognizer;
            CodePointCharStream cs = (CodePointCharStream) l.getInputStream();

            ee.setSource("lexer");
            ee.setLine(cs.toString());
            ee.setStart(-1);
            ee.setStop(-1);
        }
    }

}
