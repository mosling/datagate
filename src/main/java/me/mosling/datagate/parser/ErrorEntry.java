package me.mosling.datagate.parser;

import lombok.Getter;
import lombok.Setter;

public class ErrorEntry {
    @Getter
    @Setter
    String msg;
    @Getter
    @Setter
    String source;
    @Getter
    @Setter
    String line;
    @Getter
    @Setter
    int start;
    @Getter
    @Setter
    int stop;

    public String errorMarker() {
        String ret = "";

        if (start >= 0 && stop >= 0) {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < start; ++i)
                sb.append(" ");

            for (int i = start; i <= stop; ++i)
                sb.append("^");

            ret = sb.toString();
            if (sb.indexOf("^") == -1) {
                // EOL error, adding a marker
                ret += "^";
            }
        }

        return ret;
    }

    public String inlineMarker(String begin, String end) {
        if (start >= 0 && stop >= 0) {
            StringBuilder sb = new StringBuilder();
            sb.append(line, 0, start);
            sb.append(begin);
            if (start < stop) {
                sb.append(line, start, stop + 1);
                sb.append(end);
                sb.append(line.substring(stop + 1));
            } else {
                sb.append("EOL");
                sb.append(end);
            }

            return sb.toString();
        }

        return line;
    }
}
