package me.mosling.datagate.parser;

import lombok.Getter;
import me.mosling.datagate.grammar.UrlQueryFilterBaseListener;
import me.mosling.datagate.grammar.UrlQueryFilterParser;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;
import java.util.List;

public class UrlQueryFilterViewer
        extends UrlQueryFilterBaseListener {
    @Getter
    private final List<QueryFilterEntry> query = new ArrayList<>();

    private QueryFilterEntry clause;
    private final Deque<String> junctionStack = new ArrayDeque<>();

    @Override
    public void enterJunction_operator(UrlQueryFilterParser.Junction_operatorContext ctx) {
        junctionStack.push(ctx.getText());
        super.enterJunction_operator(ctx);
    }

    @Override
    public void enterComparison_separator(UrlQueryFilterParser.Comparison_separatorContext ctx) {
        addOperationClause(junctionStack.isEmpty() ? "and" : junctionStack.peek());
        super.enterComparison_separator(ctx);
    }

    @Override
    public void enterString(UrlQueryFilterParser.StringContext ctx) {
        clause.stringInfo(true);
        super.enterString(ctx);
    }

    @Override
    public void enterComparison(UrlQueryFilterParser.ComparisonContext ctx) {
        clause.rhs(ctx.comp_rhs.getText().trim());
        super.enterComparison(ctx);
    }

    @Override
    public void enterString_pair(UrlQueryFilterParser.String_pairContext ctx) {
        clause.stringInfo(true);
        clause.rhs(ctx.from.getText());
        clause.rhs2(ctx.to.getText());
        super.enterString_pair(ctx);
    }

    @Override
    public void enterNumber_pair(UrlQueryFilterParser.Number_pairContext ctx) {
        clause.stringInfo(false);
        clause.rhs(ctx.from.getText());
        clause.rhs2(ctx.to.getText());
        super.enterNumber_pair(ctx);
    }

    @Override
    public void enterMatchcondition(UrlQueryFilterParser.MatchconditionContext ctx) {
        clause.operator(ctx.match_op.getText());
        clause.rhs(ctx.match_pattern.getText());
        clause.stringInfo(true);
        super.enterMatchcondition(ctx);
    }

    @Override
    public void enterLikecondition(UrlQueryFilterParser.LikeconditionContext ctx) {
        clause.operator(ctx.like_op.getText());
        clause.rhs(ctx.like_pattern.getText());
        clause.stringInfo(true);
        super.enterLikecondition(ctx);
    }

    @Override
    public void enterBetween(UrlQueryFilterParser.BetweenContext ctx) {
        clause.operator(ctx.between_op.getText());
        super.enterBetween(ctx);
    }

    @Override
    public void enterDate_pair(UrlQueryFilterParser.Date_pairContext ctx) {
        clause.stringInfo(true);
        clause.rhs(ctx.from.getText());
        clause.rhs2(ctx.to.getText());
        super.enterDate_pair(ctx);
    }

    @Override
    public void enterJunction(UrlQueryFilterParser.JunctionContext ctx) {
        addOperationClause("(");
        super.enterJunction(ctx);
    }

    @Override
    public void exitSequence(UrlQueryFilterParser.SequenceContext ctx) {
        super.exitSequence(ctx);
    }

    @Override
    public void exitJunction(UrlQueryFilterParser.JunctionContext ctx) {
        addOperationClause(")");
        if (!junctionStack.isEmpty()) {
            junctionStack.pop();
        }

        super.exitJunction(ctx);
    }

    @Override
    public void enterNumericList(UrlQueryFilterParser.NumericListContext ctx) {
        clause.stringInfo(false);
        super.enterNumericList(ctx);
    }

    @Override
    public void enterStringList(UrlQueryFilterParser.StringListContext ctx) {
        clause.stringInfo(true);
        super.enterStringList(ctx);
    }

    @Override
    public void enterListcondition(UrlQueryFilterParser.ListconditionContext ctx) {
        clause.operator(ctx.list_op.getText());
        clause.rhs(ctx.list_rhs.getText());
        super.enterListcondition(ctx);
    }

    @Override
    public void enterSelection(UrlQueryFilterParser.SelectionContext ctx) {
        clause = new QueryFilterEntry();
        clause.lhs(ctx.lhs.getText());
        super.enterSelection(ctx);
    }

    @Override
    public void exitSelection(UrlQueryFilterParser.SelectionContext ctx) {
        query.add(clause);
        super.exitSelection(ctx);
    }

    @Override
    public void enterNotOperator(UrlQueryFilterParser.NotOperatorContext ctx) {
        clause.negate(true);
        super.enterNotOperator(ctx);
    }

    @Override
    public void enterOperator(UrlQueryFilterParser.OperatorContext ctx) {
        clause.operator(ctx.comp_op.getText());
        super.enterOperator(ctx);
    }

    private void addOperationClause(String o) {
        QueryFilterEntry c = new QueryFilterEntry();
        c.operator(o);
        query.add(c);
    }

}
