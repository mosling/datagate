package me.mosling.datagate.parser;

import lombok.Getter;
import me.mosling.httpfixture.common.StringHelper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 *
 */
public class QueryParameter {

    private static final Logger LOGGER = LogManager.getLogger( QueryParameter.class );

    @Getter
    private String originalFilter = "";
    @Getter
    private List<QueryFilterEntry> filterList = Collections.emptyList();
    @Getter
    private final List<String> orderList = new ArrayList<>();
    @Getter
    private final List<String> selectList = new ArrayList<>();
    @Getter
    private int limit = -1;

    @Getter
    private List<ErrorEntry> errorEntries = Collections.emptyList();

    private QueryParameter() {
        // used by builder only
    }

    public QueryParameter(String filter, String select, String order, String limit) {
        select = StringHelper.nonNullStr(select);
        order = StringHelper.nonNullStr(order);

        originalFilter = filter;

        QueryFilterParser p = new QueryFilterParser();
        filterList = p.createFilterListFromHttpQuery(filter);
        errorEntries = p.getErrorListener().getErrors();

        if (null != limit && !limit.isEmpty()) {
            try {
                this.limit = Integer.parseInt(limit);
            } catch (NumberFormatException ex) {
                LOGGER.error("limit '{}' not a number {}", limit, ex.getMessage(), ex);
            }
        }

        if (!order.isEmpty()) {
            orderList.addAll(Arrays.asList(order.split(",")));
        }

        addSelection(select);
    }

    public String filterToString() {
        if (filterList.isEmpty()) {
            return "";
        }

        StringBuilder sb = new StringBuilder();

        for (QueryFilterEntry fe : filterList) {
            sb.append(String.format(" %s %s %s %s", fe.lhs().isEmpty() ? "" : fe.lhs(), fe.operator().toUpperCase(),
                    fe.rhs().isEmpty() ? "" : "'" + fe.rhs() + "'",
                    fe.rhs2().isEmpty() ? "" : "and '" + fe.rhs2() + "'"));
        }

        return sb.toString().replaceAll("  *", " ").trim();
    }

    private void addSelection(String select) {
        if (!select.isEmpty()) {
            selectList.addAll(Arrays.asList(select.split(",")));
        }
    }

    /**
     * Creates a new object with one QueryFilterEntry entry to check the equality of the column
     * with the value.
     *
     * @param columnName the column name with the same convention as for http parameter,
     *                   underscore starts a native column and the &gt; character separates
     *                   json path elements
     * @param value      a string value
     *
     * @return the created object
     */
    public static QueryParameter newEqual(String columnName, String value) {
        QueryParameter qp = new QueryParameter();
        String queryColumnName = columnName.replace(".", ">");

        qp.filterList = new ArrayList<>();
        qp.filterList.add(new QueryFilterEntry().lhs(queryColumnName).operator("EQ").rhs(value));
        qp.originalFilter = String.format("%s.eq.%s", queryColumnName, value);

        return qp;
    }

    public static QueryParameter newSelection(String selectCommaList) {
        QueryParameter qp = new QueryParameter();

        qp.addSelection(selectCommaList);

        return qp;
    }
}
