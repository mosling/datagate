grammar UrlQueryFilter;

query
    : junction EOF
    | sequence EOF
    ;

junction
    : junction_operator '(' junction_component (comparison_separator junction_component)* ')'
    ;

junction_operator
    : AND
    | OR
    ;

junction_component
    : sequence
    | junction
    ;

sequence
    : selection (comparison_separator selection)*
    ;

comparison_separator
    : ','
    ;

selection
    : lhs=attribute SEP condition
    ;

condition
    : comparison
    | listcondition
    | likecondition
    | matchcondition
    | between
    ;

comparison
    : operator '.' comp_rhs=comparison_rhs
    ;

comparison_rhs
    : attribute      #rhs_attribute
    | number         #rhs_number
    | string         #rhs_string
    | ISODATETIME    #rhs_date
    ;

listcondition
    : not list_op=listoperator SEP '(' list_rhs=list ')'
    ;

listoperator
    : IN
    | ANY
    | ALL
    ;

likecondition
    : not like_op=likeoperator SEP like_pattern=likepattern
    ;

likeoperator
    : LIKE
    | ILIKE
    ;

matchcondition
    : not match_op=MATCH SEP match_pattern=matchpattern
    ;

between
    : not between_op=BTW SEP '(' betweenpair ')'
    ;

betweenpair
    : string_pair
    | date_pair
    | number_pair
    ;

date_pair
    : from=datetime ',' to=datetime
    ;

string_pair
    : from=string ',' to=string
    ;

number_pair
    : from=number ',' to=number
    ;

operator
    : negate=not comp_op=bioperator
    ;
not
    : NOT SEP #NotOperator
    |         #NotEmpty
    ;

bioperator
    : EQ
    | GT
    | GTE
    | LT
    | LTE
    | IS
    | HAS
    ;

list
    : number (',' number )*         #NumericList
    | string (',' string )*         #StringList
    ;

attribute
    : fieldname (FIELDSEP fieldname)*
    ;

likepattern
    : (string | '_' | '*')+
    ;

matchpattern
    : (( patternstring | patterngroup | patternclass ) patternrepetition?)*
    ;

patternstring
    : string
    | number
    | patternquote
    ;

patternquote
    : '\\(' | '\\)' | '\\[' | '\\]' | '\\{' | '\\}' | '\\*' | '\\+' | '\\?'
    ;

patterngroup
    : '('  ( patternstring ) ( '|' ( patternstring ))* ')'
    ;

// syntactical characters only, we not check if the expression is correct
// adding '0' and '-' because this characters allone not matched by number or string
patternclass
    : '[' patternclassstring ']'  #match_in_class
    | '[^' patternclassstring ']' #match_not_in_class
    ;

patternclassstring
    : '-'? ('-' | '0' | number | string )+
    ;

patternrepetition
    : '*'                        #rep_zero_many
    | '+'                        #rep_one_many
    | '?'                        #rep_zero_one
    | '{' number '}'             #rep_n
    | '{' number ',' '}'         #rep_n_many
    | '{' number ',' number '}'  #rep_n_m
    ;

datetime
    : ISODATETIME
    ;

number
    : NUMBER
    ;

fieldname
    : FIELDNAME
    ;

string
    : QSTRING
    | ( FIELDNAME | ADDCHAR )+
    ;

WS         : (' '|'\t'|'\n'|'\r')+ ->skip;

SEP        : '.';
FIELDSEP   : '>';
UNDERLINE  : '_';
DASH       : '-';
OR         : ( 'or' | 'OR' );
AND        : ( 'and' | 'AND' );
IN         : ( 'in' | 'IN' );
ANY        : ( 'any' | 'ANY' );
ALL        : ( 'all' | 'ALL' );
LIKE       : ( 'like' | 'LIKE' );
ILIKE      : ( 'ilike' | 'ILIKE' );
MATCH      : ( 'match' | 'MATCH' );
BTW        : ( 'btw' | 'BTW' | 'between' | 'BETWEEN' );
NOT        : ( 'not' | 'NOT' );
EQ         : ( 'eq' | 'EQ' );
GT         : ( 'gt' | 'GT' );
GTE        : ( 'gte' | 'GTE' );
LT         : ( 'lt' | 'LT' );
LTE        : ( 'lte' | 'LTE' );
IS         : ( 'is' | 'IS' );
HAS        : ( 'has' | 'HAS' );

QSTRING    : '\'' ( ~('\\'|'\'') | ('\'' '\'' ) )* '\'';

ISODATETIME: ( ISODATE | ISODATE ISOTIME );
NUMBER     : ('1'..'9') DIGIT*;
FIELDNAME  : ('_' | CHAR) ( '_' | '-' | CHAR | DIGIT )*;

ADDCHAR    : ( PUNCTUATION | UMLAUTE )+;

fragment ISODATE    : DIGIT DIGIT DIGIT DIGIT '-' DIGIT DIGIT '-' DIGIT DIGIT;
fragment ISOTIME    : 'T' DIGIT DIGIT ':' DIGIT DIGIT ':' DIGIT DIGIT;
fragment PUNCTUATION: ('=' | '!' | '#' | '$' | '%' | '&' | '/' | ':' | ';' | '@' | '^' | '~' );
fragment CHAR       : ( LL | UL );
fragment DIGIT      : '0'..'9';
fragment LL         : 'a'..'z';
fragment UL         : 'A'..'Z';
fragment UMLAUTE    : ( '\u00c4' | '\u00e4' | '\u00d6' | '\u00f6' | '\u00dc' | '\u00fc' | '\u00df' );

