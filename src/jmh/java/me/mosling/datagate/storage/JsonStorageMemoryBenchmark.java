package me.mosling.datagate.storage;

import java.util.UUID;
import java.util.concurrent.TimeUnit;

import com.github.javafaker.Faker;
import me.mosling.datagate.core.Metadata;
import me.mosling.datagate.core.ResourceMetadata;
import me.mosling.datagate.parser.QueryParameter;
import org.json.JSONObject;
import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.BenchmarkMode;
import org.openjdk.jmh.annotations.Fork;
import org.openjdk.jmh.annotations.Group;
import org.openjdk.jmh.annotations.Measurement;
import org.openjdk.jmh.annotations.Mode;
import org.openjdk.jmh.annotations.OutputTimeUnit;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.Setup;
import org.openjdk.jmh.annotations.State;
import org.openjdk.jmh.annotations.Warmup;

@State( Scope.Group )
@Warmup( iterations = 2, time = 2, timeUnit = TimeUnit.SECONDS )
@Measurement( iterations = 5, time = 2, timeUnit = TimeUnit.SECONDS )
@Fork( 2 )
@BenchmarkMode( Mode.All )
@OutputTimeUnit( TimeUnit.MILLISECONDS )
public class JsonStorageMemoryBenchmark
{
    private static final String            tableName = "user";
    private static final JsonStorageMemory memdb     = new JsonStorageMemory();
    private static final Faker             f         = new Faker();
    private static final ResourceMetadata  rmd       = new ResourceMetadata();
    private static       String            name;
    private static       String            uuid;
    private static       long              sw;

    @Setup
    static public void createLargeDataSet()
    {
        rmd.setLocalStorageName( tableName );
        rmd.setPrimaryKey( "id" );
        Metadata.getEntryMapping().put( tableName, rmd );

        for ( int i = 0; i < 50000; ++i )
        {
            memdb.insert( tableName, entry( i == 1111 ).toString() );
        }
    }

    @Benchmark
    @Group( "memdbAccess" )
    public void searchByName()
    {
        memdb.select( tableName, QueryParameter.newEqual( "name", name ) );
    }

    @Benchmark
    @Group( "memdbAccess" )
    public void searchByUuid()
    {
        memdb.select( tableName, QueryParameter.newEqual( "id", uuid ) );
    }

    static private JSONObject entry( boolean store )
    {
        JSONObject o = new JSONObject();

        String u = UUID.randomUUID().toString();
        String n = f.name().fullName();

        if ( store )
        {
            uuid = u;
            name = n;
        }

        o.put( "id", u );
        o.put( "name", n );
        o.put( "street", f.address().streetAddress() );
        o.put( "phone", f.phoneNumber().cellPhone() );
        return o;
    }
}
